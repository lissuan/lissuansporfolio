package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * @author <a href="mailto:lissuan26@gmail.com">Lissuan Fadraga Artiles </a>
 */
@Entity(name = "CONTACTO")
public class Contacto extends SuperEntity implements Serializable, UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final long PASSWORD_EXPIRATION_PERIOD = 60 * 60 * 24 * 90;

	private String usuarioCorreo;
	private String celular;
	private String telefono;
	private String idNextel;
	private String nombreContacto;
	private Set<Correo> listaCorreos;
	private Set<Factura> listaFacturas;

	/**
	 * Datos para logueo
	 * */
	private Rol rol = null;
	private Timestamp fechaActualizacionPassword = null;
	private Timestamp fechaExpiracionCuenta = null;
	private String contrasena;
	private String nombreCuenta;
	private boolean activo = false;
	private boolean bloqueado = false;
	private Set<Cliente> clientes;

	public Contacto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Contacto(String descripcion) {
		super(descripcion);
		// TODO Auto-generated constructor stub
	}

	public Contacto(String usuarioCorreo, String celular, String telefono,
			String idNextel, String nombreContacto) {
		super();
		this.usuarioCorreo = usuarioCorreo;
		this.celular = celular;
		this.telefono = telefono;
		this.idNextel = idNextel;
		this.nombreContacto = nombreContacto;
	}

	@Column(name = "USUARIOCORREO", nullable = false)
	public String getUsuarioCorreo() {
		return usuarioCorreo;
	}

	public void setUsuarioCorreo(String usuarioCorreo) {
		this.usuarioCorreo = usuarioCorreo;
	}

	@Column(name = "CELULAR", nullable = true)
	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	@Column(name = "TELEFONO", nullable = true)
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name = "IDNEXTEL", nullable = true)
	public String getIdNextel() {
		return idNextel;
	}

	public void setIdNextel(String idNextel) {
		this.idNextel = idNextel;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CLIENTE_CORREO", joinColumns = { @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_CORREO", referencedColumnName = "ID") })
	public Set<Correo> getListaCorreos() {
		return listaCorreos;
	}

	public void setListaCorreos(Set<Correo> listaCorreos) {
		this.listaCorreos = listaCorreos;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CLIENTE_FACTURA", joinColumns = { @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_FACTURA", referencedColumnName = "ID") })
	public Set<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(Set<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Contacto)) {
			return false;
		}
		final Contacto o = (Contacto) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getUsername()
	 */
	@Transient
	public String getUsername() {
		String username = this.nombreCuenta == null ? "" : this.nombreCuenta;
		return username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getPassword()
	 */
	@Transient
	public String getPassword() {
		String password = this.contrasena == null ? "" : this.contrasena;
		return password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getAuthorities
	 * ()
	 */
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return rol.getFuncionalidades();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired
	 * ()
	 */
	@Transient
	public boolean isAccountNonExpired() {
		java.util.Date date = new java.util.Date();
		return fechaExpiracionCuenta == null
				|| fechaExpiracionCuenta.getTime() > date.getTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked
	 * ()
	 */
	@Transient
	public boolean isAccountNonLocked() {
		return !bloqueado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isCredentialsNonExpired()
	 */
	@Transient
	public boolean isCredentialsNonExpired() {
		return this.fechaActualizacionPassword == null
				|| this.fechaActualizacionPassword.getTime() - 1 > PASSWORD_EXPIRATION_PERIOD;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	@Transient
	public boolean isEnabled() {
		return this.isActivo() && this.rol.isActivo();
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para {@link Rol}
	 * 
	 * @return {@link Rol}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_ROL", referencedColumnName = "ID")
	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	/**
	 * Obtiene la ultima fecha de actualizacion de contraseña
	 * 
	 * @return {@link Timestamp}
	 */
	@Column(name = "FECHA_ACTUALIZACION_PASSWORD")
	public Timestamp getFechaActualizacionPassword() {
		return this.fechaActualizacionPassword;
	}

	public void setFechaActualizacionPassword(
			Timestamp fechaActualizacionPassword) {
		this.fechaActualizacionPassword = fechaActualizacionPassword;
	}

	/**
	 * Obtiene la fecha de expiración de la cuenta
	 * 
	 * @return
	 */
	@Column(name = "FECHA_EXPIRACION_CUENTA")
	public Timestamp getFechaExpiracionCuenta() {
		return fechaExpiracionCuenta;
	}

	public void setFechaExpiracionCuenta(Timestamp fechaExpiracionCuenta) {
		this.fechaExpiracionCuenta = fechaExpiracionCuenta;
	}

	@Column(name = "CONTRASENA")
	public String getContrasena() {
		return this.contrasena == null ? "" : this.contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	@Column(name = "NOMBRECUENTA", length = 200, nullable = false)
	public String getNombreCuenta() {
		return this.nombreCuenta;
	}

	public void setNombreCuenta(String nombreCuenta) {
		this.nombreCuenta = nombreCuenta;
	}

	/**
	 * ¿Está Activo?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "ACTIVO", nullable = false)
	public boolean isActivo() {
		return this.activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * ¿Esta Bloqueado?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "BLOQUEADO", nullable = false)
	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	@ManyToMany(mappedBy = "contactos")
	public Set<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(Set<Cliente> clientes) {
		this.clientes = clientes;
	}

}
