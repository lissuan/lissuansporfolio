package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Component("abstractClienteValidator")
public class AbstractClienteValidator extends AbstractValidator {

	@Autowired
	private ClienteCorreoValidator clienteCorreoValidator;
	@Autowired
	private NombreContactoValidator nombreContactoValidator;
	@Autowired
	private CuentaBancariaValidator cuentaBancariaValidator;

	@Override
	public void validate(ValidationContext ctx) {
		clienteCorreoValidator.validate(ctx);
		nombreContactoValidator.validate(ctx);
		cuentaBancariaValidator.validate(ctx);
		
	}

}
