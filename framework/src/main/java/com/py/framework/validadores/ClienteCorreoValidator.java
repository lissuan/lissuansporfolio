package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.entidades.Empresa;
import com.py.framework.servicios.domain.ClienteServ;

@Component("clienteCorreoValidator")
public class ClienteCorreoValidator extends ConfigurableValidator {

	@Autowired
	private ClienteServ clienteServ;

	public ClienteCorreoValidator() {
		super();
		setInvalidMessage("Correo no valido");
		setMessageKey("edCorreoCliente");
	}

	@Override
	public void validate(ValidationContext ctx) {
		String descripcion = null;
		String descripcionOld = (String) ctx.getBindContext().getValidatorArg(
				"correoClienteOld");
		boolean nuevo = (Boolean) ctx.getBindContext()
				.getValidatorArg("editar");
		Empresa empresa = (Empresa) ctx.getBindContext().getValidatorArg(
				"empresa");
		boolean validationResult = true;

		descripcion = (String) ctx.getBindContext().getValidatorArg(
				"correoCliente");
		if (!nuevo) {
			if (descripcion.equals("")) {
				validationResult = false;
				this.addInvalidMessage(ctx, getMessageKey(),
						getInvalidMessage());
				return;
			}
			if (descripcion.equals(descripcionOld)) {
				validationResult = true;
				return;
			}
		} else {
			validationResult = doValidate(descripcion, empresa);
		}
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}
	}

	public boolean doValidate(String descripcionEmpresa, Empresa empresa) {
		if (descripcionEmpresa.isEmpty()
				|| (!descripcionEmpresa.contains("@") && !descripcionEmpresa
						.contains("."))) {
			return false;
		}
		if (clienteServ.findByCorreoContacto(descripcionEmpresa) != null) {
			return false;
		}
		return true;
	}
}
