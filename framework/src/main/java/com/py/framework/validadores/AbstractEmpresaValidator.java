package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;


@Component("abstractEmpresaValidator")
public class AbstractEmpresaValidator extends AbstractValidator {

	@Autowired
	private DescripcionEmpresaValidator descripcionEmpresaValidator;
	@Autowired
	private RucValidator rucValidator;
	
	@Override
	public void validate(ValidationContext ctx) {
		
        descripcionEmpresaValidator.validate(ctx);
        rucValidator.validate(ctx); 
	}

	public DescripcionEmpresaValidator getDescripcionEmpresaValidator() {
		return descripcionEmpresaValidator;
	}

	public void setDescripcionEmpresaValidator(
			DescripcionEmpresaValidator descripcionEmpresaValidator) {
		this.descripcionEmpresaValidator = descripcionEmpresaValidator;
	}

	public RucValidator getRucValidator() {
		return rucValidator;
	}

	public void setRucValidator(RucValidator rucValidator) {
		this.rucValidator = rucValidator;
	}

}
