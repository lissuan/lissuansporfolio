package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Component("userFormValidator")
public class FormUsuarioValidator extends AbstractValidator {

	@Autowired
	private
	PasswordMatchValidator passwordMatchValidator;
	
	@Autowired
	private
	NombreCuentaAvailabilityValidator nombreCuentaAvailabilityValidator; 
	
	@Override
	public void validate(ValidationContext ctx) {
		passwordMatchValidator.validate(ctx);
		nombreCuentaAvailabilityValidator.validate(ctx);
	}

}
