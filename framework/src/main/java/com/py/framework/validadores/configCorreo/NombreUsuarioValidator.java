package com.py.framework.validadores.configCorreo;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.py.framework.servicios.domain.ContactoServ;
import com.py.framework.validadores.ConfigurableValidator;

@Component("nombreUsuarioValidator")
public class NombreUsuarioValidator extends ConfigurableValidator {

	

	public NombreUsuarioValidator() {
		super();
		setMessageKey("edNombreUsuario");
		setInvalidMessage("Usuario no valido");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String edNombreUsuario = (String) ctx.getBindContext().getValidatorArg(
				"edNombreUsuario");

		boolean validationResult = true;

		validationResult = doValidate(edNombreUsuario);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		if (!edCantidadNiveles.contains("@")) {
			return false;
		}
//		if (contactoServ.findByUsuarioCorreo(edCantidadNiveles) != null) {
//			return false;
//		}
		return true;
	}

}
