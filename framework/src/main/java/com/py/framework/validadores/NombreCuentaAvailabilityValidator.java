package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.entidades.Empresa;
import com.py.framework.servicios.domain.UsuarioServ;

@Component("nombreCuentaAvailabilityValidator")
public class NombreCuentaAvailabilityValidator extends ConfigurableValidator {

	
	@Autowired
	private UsuarioServ userServ;
	
	
	public NombreCuentaAvailabilityValidator(){
		super();
		setMessageKey("edNombreCuenta");
		setInvalidMessage("Este nombre usuario no está disponible.");
	}
	
	@Override
	public void validate(ValidationContext ctx) {
		/**
		 * Campos contextuales, estos son campos relacionados que intervienen en la validacion
		 * estos campos siempre son pasados como parametros
		 */
		String oldNombreCuenta = (String)ctx.getBindContext().getValidatorArg("oldNombreCuenta");
		Empresa empresa = (Empresa)ctx.getBindContext().getValidatorArg("empresa");

		/**
		 * Campo Principal, de no estar enviado como parametro se obtiene de la propiedad principal
		 */
		String nombreCuenta;
		if (ctx.getBindContext().getValidatorArg("nombreCuenta")==null){
			nombreCuenta = (String)ctx.getProperty().getValue();
		} else {
			nombreCuenta = (String)ctx.getBindContext().getValidatorArg("nombreCuenta");
		}
		
		/**
		 * Validacion
		 */
		boolean validationResult = doValidate(oldNombreCuenta,nombreCuenta,empresa);
        
		/**
		 * poner mensaje si falla
		 */
		if (!validationResult){
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}
	}
	
	public boolean doValidate(String oldNombreCuenta,String nombreCuenta,Empresa empresa){
		if (nombreCuenta.isEmpty()){
			return false;
		}
		if (oldNombreCuenta!=null && oldNombreCuenta.equals(nombreCuenta)) {
			return true;
		}
		if (userServ.findUserByEmpresaAndNombreCuenta(empresa, nombreCuenta)==null){
			return true;
		} else {
			return false;
		}
	}

	

}
