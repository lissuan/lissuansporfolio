package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.entidades.Empresa;
import com.py.framework.servicios.domain.SucursalServ;

@Component("sucursalValidator")
public class SucursalValidator extends ConfigurableValidator {

	@Autowired
	private SucursalServ sucursalServ;

	public SucursalValidator() {
		super();
		setMessageKey("edDescripcionSucursal");
		setInvalidMessage("Esta descripción no es válida");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String descripcionSucursal = null;
		String descripcionsucursalOld = (String) ctx.getBindContext()
				.getValidatorArg("oldDescripcionSucursal");
		boolean nuevo = (Boolean) ctx.getBindContext()
				.getValidatorArg("editar");
		boolean validationResult = true;
		Empresa empresa = (Empresa) ctx.getBindContext()
				.getValidatorArg("empresa");

		if (ctx.getBindContext().getValidatorArg("descripcionSucursal") == null) {
			descripcionSucursal = (String) ctx.getProperty().getValue();
		} else {
			descripcionSucursal = (String) ctx.getBindContext()
					.getValidatorArg("descripcionSucursal");
		}
	 
		if (!nuevo) {
			if (descripcionSucursal != null && !descripcionSucursal.equals("") && descripcionsucursalOld.equals(descripcionSucursal)) {
				validationResult = true;
			} else
				validationResult = false;
		}else{
			validationResult =(descripcionSucursal != null && !descripcionSucursal.equals("") && doValidate(descripcionSucursal,empresa));
		}
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}
	}

	private boolean doValidate(String descripcionSucursal,Empresa empresa) {
		if (descripcionSucursal.isEmpty()) {
			return false;
		}
		if (getSucursalServ().findByDescripcionAndEmpresa(descripcionSucursal,empresa) == null) {
			return true;
		} else {
			return false;
		}
	}

	public SucursalServ getSucursalServ() {
		return sucursalServ;
	}

	public void setSucursalServ(SucursalServ sucursalServ) {
		this.sucursalServ = sucursalServ;
	}

}
