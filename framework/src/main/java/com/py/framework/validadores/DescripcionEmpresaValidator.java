package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.servicios.domain.EmpresaServ;

@Component("descripcionEmpresaValidator")
public class DescripcionEmpresaValidator extends ConfigurableValidator {

	@Autowired
	private EmpresaServ empresaServ;

	public DescripcionEmpresaValidator() {
		super();
		setMessageKey("edDescripcionEmpresa");
		setInvalidMessage("Esta descripción no es válida");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String descripcionEmpresa = null;
		String descripcionEmpresaOld = (String) ctx.getBindContext()
				.getValidatorArg("oldDescripcionEmpresa");
		boolean nuevo = (Boolean) ctx.getBindContext()
				.getValidatorArg("editar");
		String carpeta = (String) ctx.getBindContext()
				.getValidatorArg("carpeta");
		boolean validationResult = true;

		if (ctx.getBindContext().getValidatorArg("descripcionEmpresa") == null) {
			descripcionEmpresa = (String) ctx.getProperty().getValue();
		} else {
			descripcionEmpresa = (String) ctx.getBindContext().getValidatorArg(
					"descripcionEmpresa");
		}

		if (!nuevo) {
			if (descripcionEmpresa.equals(descripcionEmpresaOld)) {
				validationResult = true;
				return;
			} else {
				validationResult = doValidate(descripcionEmpresa);
			}
			if (carpeta==null || carpeta.equals("") || empresaServ.findByCarpeta(carpeta)!=null) {
				validationResult = false;
				this.addInvalidMessage(ctx, "edCarpetaNoValida", "La carpeta no es válida");
				return;
			}
		} else {
			validationResult = (descripcionEmpresaOld != null && descripcionEmpresaOld
					.equals(descripcionEmpresa)) ? false
					: doValidate(descripcionEmpresa);
			if ((ctx != null) && (!validationResult)) {
				this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
			}
			if (carpeta==null || carpeta.equals("") || empresaServ.findByCarpeta(carpeta)!=null) {
				validationResult = false;
				this.addInvalidMessage(ctx, "edCarpetaNoValida", "La carpeta no es válida");
				return;
			}
		}
		
	}
   
	public boolean doValidate(String descripcionEmpresa) {
		if (descripcionEmpresa.isEmpty()) {
			return false;
		}
		if (empresaServ.findByDescripcion(descripcionEmpresa) == null) {
			return true;
		} else {
			return false;
		}
	}

}
