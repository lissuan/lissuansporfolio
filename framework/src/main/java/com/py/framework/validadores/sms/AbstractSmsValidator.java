package com.py.framework.validadores.sms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Component("abstractSmsValidator")
public class AbstractSmsValidator extends AbstractValidator {

	@Autowired
	private MensajeValidator mensajeValidator;

	@Override
	public void validate(ValidationContext arg0) {
		mensajeValidator.validate(arg0);
	}

}
