package com.py.framework.validadores.contacto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.servicios.domain.ContactoServ;
import com.py.framework.validadores.ConfigurableValidator;

@Component("usuarioCorreo")
public class UsuarioCorreo extends ConfigurableValidator {
	@Autowired
	private ContactoServ contactoServ;

	public UsuarioCorreo() {
		setMessageKey("usuarioCorreo");
		setInvalidMessage("Usuario no valido");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String usuarioCorreo = (String) ctx.getBindContext().getValidatorArg(
				"usuarioCorreo");

		boolean validationResult = true;

		validationResult = doValidate(usuarioCorreo);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		if (!edCantidadNiveles.contains("@")) {
			return false;
		}
		if (contactoServ.findByUsuarioCorreo(edCantidadNiveles) != null) {
			return false;
		}
		return true;
	}

}
