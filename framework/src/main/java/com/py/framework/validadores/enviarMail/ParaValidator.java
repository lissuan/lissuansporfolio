package com.py.framework.validadores.enviarMail;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Component("paraValidator")
public class ParaValidator extends ConfigurableValidator {

	public ParaValidator() {
		super();
		setMessageKey("edPara");
		setInvalidMessage("Usuario no valido");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String edPara = (String) ctx.getBindContext().getValidatorArg(
				"edPara");

		boolean validationResult = true;

		validationResult = doValidate(edPara);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		return true;
	}
}
