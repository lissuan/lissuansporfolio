package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

public class FormEmpresaValidator extends AbstractValidator{

	@Autowired
	private DescripcionEmpresaValidator descripcionEmpresaValidator;
	
	@Autowired
	private RucValidator rucValidator;
	
	@Override
	public void validate(ValidationContext ctx) {
	   	descripcionEmpresaValidator.validate(ctx);
	   	rucValidator.validate(ctx);	 	
	}
	
	

}
