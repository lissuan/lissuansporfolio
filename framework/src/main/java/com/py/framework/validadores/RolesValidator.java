package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.entidades.Empresa;
import com.py.framework.servicios.domain.RolServ;

@Component("rolesValidator")
public class RolesValidator extends ConfigurableValidator {

	@Autowired
	private RolServ rolServ;
	
	public RolesValidator() {
		super();
		setMessageKey("edrolDescripcion");
		setInvalidMessage("Rol no válida");
	}


	@Override
	public void validate(ValidationContext ctx) {
		String rolDescripcion = null;
		String rolDescripcionOld = (String) ctx.getBindContext()
				.getValidatorArg("rolDescripcionOld");
		boolean validationResult = true;
		boolean nuevo = (Boolean) ctx.getBindContext()
				.getValidatorArg("editar");
		Empresa empresa = (Empresa) ctx.getBindContext().getValidatorArg(
				"empresa");

		if (ctx.getBindContext().getValidatorArg("rolDescripcion") == null) {
			rolDescripcion = (String) ctx.getProperty().getValue();
		} else {
			rolDescripcion = (String) ctx.getBindContext().getValidatorArg(
					"rolDescripcion");
		}

		if (!nuevo) {
			if (rolDescripcionOld == null || rolDescripcionOld.equals("")) {
				validationResult = false;
				return;
			}
			if (!rolDescripcionOld.equals(rolDescripcion)) {
				validationResult = doValidate(rolDescripcion, empresa);
			}
		} else {
			validationResult = (rolDescripcionOld != null
					&& rolDescripcionOld.equals(rolDescripcion) && rolDescripcion != "") ? false
					: doValidate(rolDescripcion, empresa);
		}
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	private boolean doValidate(String rolDescripcion, Empresa empresa) {
		if (rolDescripcion.isEmpty()) {
			return false;
		}
		if (getRolServ().findByEmpresaAndDescripcion(empresa,rolDescripcion) == null) {
			return true;
		} else {
			return false;
		}
	}


	public RolServ getRolServ() {
		return rolServ;
	}


	public void setRolServ(RolServ rolServ) {
		this.rolServ = rolServ;
	}

	

}
