package com.py.framework.validadores;

import org.zkoss.bind.validator.AbstractValidator;

public abstract class ConfigurableValidator extends AbstractValidator {

	private String _messageKey = "";
	private String _invalidMessage = "";

	public String getMessageKey() {
		return _messageKey;
	}

	public void setMessageKey(String MessageKey) {
		this._messageKey = MessageKey;
	}

	public String getInvalidMessage() {
		return _invalidMessage;
	}

	public void setInvalidMessage(String invalidMessage) {
		this._invalidMessage = invalidMessage;
	}

}
