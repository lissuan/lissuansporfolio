package com.py.framework.validadores.enviarMail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Component("abstractEnviarMailValidator")
public class AbstractEnviarMailValidator extends AbstractValidator {

	@Autowired
	private ParaValidator paraValidator;

	
	@Override
	public void validate(ValidationContext ctx) {

		paraValidator.validate(ctx);

	}

}
