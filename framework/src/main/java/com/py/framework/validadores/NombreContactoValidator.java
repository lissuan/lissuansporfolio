package com.py.framework.validadores;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

@Component("nombreContactoValidator")
public class NombreContactoValidator extends ConfigurableValidator {

	public NombreContactoValidator() {
		super();
		setInvalidMessage("Nombre contacto no valido no valido");
		setMessageKey("edNombreContacto");
	}

	@Override
	public void validate(ValidationContext ctx) {
		String nombreContacto = null;
		boolean validationResult = true;

		nombreContacto = (String) ctx.getBindContext().getValidatorArg(
				"edNombreContacto");

		validationResult = doValidate(nombreContacto);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}
	}

	public boolean doValidate(String descripcionEmpresa) {
		if (descripcionEmpresa == null || descripcionEmpresa.isEmpty()) {
			return false;
		}
		return true;
	}

}
