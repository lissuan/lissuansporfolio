package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

import com.py.framework.validadores.contacto.Celular;

@Component("abstractCambiarValidator")
public class AbstractCambiarValidator extends AbstractValidator {

	@Autowired
	private Celular celular;

	@Autowired
	private PasswordMatchValidator2 passwordMatchValidator2;

	@Override
	public void validate(ValidationContext ctx) {
		celular.validate(ctx);
		passwordMatchValidator2.validate(ctx);
	}
}
