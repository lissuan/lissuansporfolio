package com.py.framework.validadores.sms;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Component("mensajeValidator")
public class MensajeValidator extends ConfigurableValidator {

	public MensajeValidator() {
		super();
		setMessageKey("edMensaje");
		setInvalidMessage("Mensaje vacio");
	}

	@Override
	public void validate(ValidationContext arg0) {
		String edMensaje = (String) arg0.getBindContext().getValidatorArg(
				"edMensaje");

		boolean validationResult = true;

		validationResult = doValidate(edMensaje);
		if ((arg0 != null) && (!validationResult)) {
			this.addInvalidMessage(arg0, getMessageKey(), getInvalidMessage());
		}

	}

	private boolean doValidate(String edMensaje) {
		if (edMensaje == null || edMensaje.equals("")) {
			return false;
		}
		return true;
	}

}
