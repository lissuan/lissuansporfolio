package com.py.framework.validadores.contacto;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Component("nombreContacto")
public class NombreContacto extends ConfigurableValidator {

	public NombreContacto() {
		super();
		setMessageKey("nombreContacto");
		setInvalidMessage("Usuario no valido");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String nombreContacto = (String) ctx.getBindContext().getValidatorArg(
				"nombreContacto");

		boolean validationResult = true;

		validationResult = doValidate(nombreContacto);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		return true;
	}

}
