package com.py.framework.validadores.contacto;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Component("celular")
public class Celular extends ConfigurableValidator {

	public Celular() {
		super();
		setMessageKey("celular");
		setInvalidMessage("Respuesta no valida");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String celular = (String) ctx.getBindContext().getValidatorArg(
				"celular");

		boolean validationResult = true;

		validationResult = doValidate(celular);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles == null) {
			return false;
		}
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		return true;
	}

}
