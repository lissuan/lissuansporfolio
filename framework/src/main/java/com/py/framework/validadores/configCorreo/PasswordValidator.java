package com.py.framework.validadores.configCorreo;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Component("passwordValidator")
public class PasswordValidator extends ConfigurableValidator {

	public PasswordValidator() {
		setMessageKey("edPassword");
		setInvalidMessage("Respuesta no valida");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String edPassword = (String) ctx.getBindContext()
				.getValidatorArg("edPassword");

		boolean validationResult = true;

		validationResult = doValidate(edPassword);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		return true;
	}

}
