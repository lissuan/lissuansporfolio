package com.py.framework.validadores;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

@Component("promocionFechaFinValidator")
public class PromocionFechaFinValidator extends ConfigurableValidator {

	public PromocionFechaFinValidator() {
		super();
		setMessageKey("edFechaFin");
		setInvalidMessage("Esta fecha no es válida");
	}
	@Override
	public void validate(ValidationContext ctx) {
		/**
		 * Campos contextuales
		 */
		Date horaFin = (Date) ctx.getBindContext().getValidatorArg(
				"fechaFin");

		/**
		 * Campo Principal
		 */
		Date horaInicio = (Date) ctx.getBindContext().getValidatorArg(
				"fechaInicio");

		/**
		 * Validar
		 */
		boolean validationResult = doValidate(horaInicio, horaFin);

		/**
		 * Resultado de la validacion
		 */
		if (!validationResult) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(Date horaInicio, Date horaFin) {
		if (horaInicio == null) {
			return false;
		}
		if (horaFin == null) {
			return false;
		}
		if (horaInicio.after(horaFin)) {
			return false;
		}
		return true;
	}

}
