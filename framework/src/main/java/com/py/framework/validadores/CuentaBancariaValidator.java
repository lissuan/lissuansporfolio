package com.py.framework.validadores;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

@Component("cuentaBancariaValidator")
public class CuentaBancariaValidator extends ConfigurableValidator {

	public CuentaBancariaValidator() {
		super();
		setInvalidMessage("Nombre cuenta no valido no valido");
		setMessageKey("cuentaBancaria");
	}

	@Override
	public void validate(ValidationContext ctx) {
		String nombreContacto = null;
		boolean validationResult = true;
		nombreContacto = (String) ctx.getBindContext().getValidatorArg(
				"cuentaBancaria");
		validationResult = doValidate(nombreContacto);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}
	}

	public boolean doValidate(String descripcionEmpresa) {
		if (descripcionEmpresa == null || descripcionEmpresa.isEmpty()) {
			return false;
		}
		return true;
	}

}
