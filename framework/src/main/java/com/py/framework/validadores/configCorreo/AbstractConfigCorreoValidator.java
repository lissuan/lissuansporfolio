package com.py.framework.validadores.configCorreo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Service("abstractConfigCorreoValidator")
public class AbstractConfigCorreoValidator extends AbstractValidator {

	@Autowired
	private NombreUsuarioValidator nombreUsuarioValidator;
	@Autowired
	private PasswordValidator passwordValidator;
	@Autowired
	private HostValidator hostValidator;
	@Autowired
	private PortValidator portValidator;

	@Override
	public void validate(ValidationContext ctx) {
		nombreUsuarioValidator.validate(ctx);
	}

}
