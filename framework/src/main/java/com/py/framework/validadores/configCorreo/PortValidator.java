package com.py.framework.validadores.configCorreo;

import org.springframework.stereotype.Service;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Service("portValidator")
public class PortValidator extends ConfigurableValidator {

	public PortValidator() {
		super();
		setMessageKey("edPort");
		setInvalidMessage("Respuesta no valida");
	}

	@Override
	public void validate(ValidationContext ctx) {

		Integer edPort = (Integer) ctx.getBindContext().getValidatorArg(
				"edPort");

		boolean validationResult = true;

		validationResult = doValidate(edPort);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(int edCantidadNiveles) {
		if (edCantidadNiveles <= 0) {
			return false;
		}
		return true;
	}

}
