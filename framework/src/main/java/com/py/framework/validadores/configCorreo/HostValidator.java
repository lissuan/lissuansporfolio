package com.py.framework.validadores.configCorreo;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

import com.py.framework.validadores.ConfigurableValidator;

@Component("hostValidator")
public class HostValidator extends ConfigurableValidator {

	public HostValidator() {
		super();
		setMessageKey("edHost");
		setInvalidMessage("Respuesta no valida");
	}

	@Override
	public void validate(ValidationContext ctx) {

		String edHost = (String) ctx.getBindContext()
				.getValidatorArg("edHost");

		boolean validationResult = true;

		validationResult = doValidate(edHost);
		if ((ctx != null) && (!validationResult)) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}

	public boolean doValidate(String edCantidadNiveles) {
		if (edCantidadNiveles.isEmpty()) {
			return false;
		}
		return true;
	}

}
