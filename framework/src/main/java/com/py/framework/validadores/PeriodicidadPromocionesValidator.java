package com.py.framework.validadores;

import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;

@Component("periodicidadPromocionesValidator")
public class PeriodicidadPromocionesValidator extends ConfigurableValidator{

	public PeriodicidadPromocionesValidator() {
		super();
		setMessageKey("edPeriodicidad");
		setInvalidMessage("Esta descripción no es válida");
	}
	
	@Override
	public void validate(ValidationContext ctx) {
		/**
		 * Campo Principal
		 */
		Integer intervalo = null;
		if (ctx.getBindContext().getValidatorArg("periodicidad")==null){
			intervalo = (Integer)ctx.getProperty().getValue();
		} else {
			intervalo = (Integer)ctx.getBindContext().getValidatorArg("periodicidad");
		}
		
		/**
		 * Validar
		 */
		boolean validationResult = doValidate(intervalo);
		
		/**
		 * Resultado de la validacion
		 */
		if (!validationResult) {
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}

	}
	
	public boolean doValidate(Integer intervalo){
		if (intervalo == null || intervalo <= 0) {
			return false;
		}
		return true;
	}
}
