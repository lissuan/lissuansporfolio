package com.py.framework.validadores.contacto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Component("abstractContactosValidator")
public class AbstractContactosValidator extends AbstractValidator {

	@Autowired
	private UsuarioCorreo usuarioCorreo;
	@Autowired
	private Celular celular;
	@Autowired
	private NombreContacto nombreContacto;

	@Override
	public void validate(ValidationContext ctx) {
		usuarioCorreo.validate(ctx);
		celular.validate(ctx);
		nombreContacto.validate(ctx);
	}

}
