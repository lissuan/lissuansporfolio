package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

@Component("abstractRolesValidator")
public class AbstractRolesValidator extends AbstractValidator  {
	
	@Autowired
	private RolesValidator rolesValidator;
	
	@Override
	public void validate(ValidationContext ctx) {
		rolesValidator.validate(ctx);		
	}
	
}
