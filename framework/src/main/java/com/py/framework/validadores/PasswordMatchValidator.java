package com.py.framework.validadores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.py.framework.servicios.domain.UsuarioServ;

@Component("passwordMatchValidator")
public class PasswordMatchValidator extends ConfigurableValidator {
	
	@Autowired
	private BCryptPasswordEncoder bcryp;
	
	@WireVariable
	private UsuarioServ usuarioServ;
	
	public PasswordMatchValidator(){
		super();
		setMessageKey("edPassword");
		setInvalidMessage("Las contraseñas no coinciden.");
	}
	
	@Override
	public void validate(ValidationContext ctx) {
        String retype = null;
		String password = null;
		boolean nuevo = (Boolean)ctx.getBindContext().getValidatorArg("editar");
		boolean validationResult = true;
		
		if (ctx.getBindContext().getValidatorArg("retypedPassword")==null){
			retype = (String)ctx.getProperty().getValue();
		} else {
			retype = (String)ctx.getBindContext().getValidatorArg("retypedPassword");
		}
		if (ctx.getBindContext().getValidatorArg("password")==null){
			password = (String)ctx.getProperty().getValue();
		} else {
			password = (String)ctx.getBindContext().getValidatorArg("password");
		}
		if (!nuevo) {
			if (password.equals("") && retype.equals("")) {
				validationResult =true;
				return;
			}
		}
		
		validationResult= doValidate(password, retype);
        
		if ((ctx!=null) && (!validationResult)){
			this.addInvalidMessage(ctx, getMessageKey(), getInvalidMessage());
		}
	}
	
	public boolean doValidate(String password, String retype){
        if(password == null || retype == null || (!password.equals(retype)) || password.equals("") || retype.equals("") ) {
            return false;
        } else {
        	return true;
        }
	}

}
