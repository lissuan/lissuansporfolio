package com.py.framework.servicios.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Factura;
import com.py.framework.repositorios.FacturaRepository;

@Service("facturaServ")
public class FacturaServ {

	@Autowired
	private FacturaRepository facturaDao;

	@Transactional
	public Factura saveAndFlush(Factura ifactura) {

		return facturaDao.saveAndFlush(ifactura);
	}
}
