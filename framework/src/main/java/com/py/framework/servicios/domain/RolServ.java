package com.py.framework.servicios.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Funcionalidad;
import com.py.framework.entidades.Rol;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.repositorios.RolRepository;

@Service("rolServ")
public class RolServ {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private RolRepository rolDAO;
	@Autowired
	private EmpresaServ empresaServ;
	@Autowired
	private FuncionalidadServ funcionalidadServ;
	@Autowired
	private UsuarioServ usuarioServ;

	@Transactional(readOnly = true)
	public List<Rol> findAll() {
		return rolDAO.findAll();
	}

	@Transactional(readOnly = true)
	public List<Rol> findByEmpresaOrderByDescripcion(Empresa empresa) {
		return rolDAO.findByEmpresa(empresa, new Sort(Direction.ASC,
				"descripcion"));
	}

	@Transactional(readOnly = true)
	public List<Rol> findByEmpresaAndActivos(Empresa empresa) {
		return rolDAO.findByEmpresaAndActivo(empresa, true);
	}

	@Transactional
	public Rol expandFuncionalidades(Rol entity) {
		PersistenceUnitUtil uu = getEm().getEntityManagerFactory()
				.getPersistenceUnitUtil();
		if (!uu.isLoaded(entity, "funcionalidades")) {
			Rol tmpEntity = rolDAO.findOne(entity.getId());
			tmpEntity.getFuncionalidades().iterator().hasNext();
			entity.setFuncionalidades(tmpEntity.getFuncionalidades());
		}
		return entity;
	}

	@Transactional
	public Rol save(Rol entity) throws ValorNotFoundException {
		if (entity.getEmpresa() != null) {

			Empresa empresa = empresaServ.findById(entity.getEmpresa().getId());

			if (empresa != null) {
				if (!entity.getFuncionalidades().isEmpty()) {
					for (Funcionalidad iterable_funcionalidad : entity
							.getFuncionalidades()) {
						if (funcionalidadServ.findOne(iterable_funcionalidad
								.getId()) == null) {
							new ValorNotFoundException(
									"La funcionalidad a que hace referencia no se encuentra");
						}
					}
				}
				return rolDAO.save(entity);
			} else
				throw new ValorNotFoundException(
						"La empresa a que hace referencia no se encuentra");
		} else
			throw new ValorNotFoundException(
					"No se ha especificado una empresa para crear un rol.");
	}

	@Transactional
	public Rol saveAndFlush(Rol entity) throws ValorNotFoundException {

		if (entity.getEmpresa() != null) {

			Empresa empresa = empresaServ.findById(entity.getEmpresa().getId());

			if (empresa != null) {
				if (!entity.getFuncionalidades().isEmpty()) {
					for (Funcionalidad iterable_funcionalidad : entity
							.getFuncionalidades()) {
						if (funcionalidadServ.findOne(iterable_funcionalidad
								.getId()) == null) {
							new ValorNotFoundException(
									"La funcionalidad a que hace referencia no se encuentra");
						}
					}

				}
				return rolDAO.saveAndFlush(entity);
			} else
				throw new ValorNotFoundException(
						"La empresa a que hace referencia no se encuentra");
		} else
			throw new ValorNotFoundException(
					"No se ha especificado una empresa para crear un rol.");

	}

	@Transactional
	public void delete(Rol entity) throws ValorNotFoundException {
		if (entity.getFuncionalidades().isEmpty()
				|| entity.getUsuarios().isEmpty()) {
			rolDAO.delete(entity);
		} else {
			throw new ValorNotFoundException(
					"No se puede eliminar el Rol indicado,");
		}
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	public Rol findByEmpresaAndDescripcion(Empresa empresa, String descripcion) {
		return rolDAO.findByEmpresaAndDescripcion(empresa, descripcion);
	}

	@Transactional
	public boolean validarInactividadRol(Rol rol) {
		if (rol.getUsuarios().isEmpty()) {
			return true;
		} else
			return false;
	}

	@Transactional(readOnly = true)
	public Rol findOne(Long id) {

		return rolDAO.findOne(id);
	}

	@Transactional(readOnly = true)
	public Rol getRolClienteUsuario(Empresa empresa) {
		return rolDAO.findByEmpresaAndDescripcion(empresa, "Cliente usuario");
	}
}
