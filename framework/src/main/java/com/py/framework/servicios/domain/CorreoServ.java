package com.py.framework.servicios.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Correo;
import com.py.framework.repositorios.CorreoResository;

@Service("correoServ")
public class CorreoServ {

	@Autowired
	private CorreoResository correoDao;

	@Transactional
	public Correo saveAndFlush(Correo correoAEnviar) {
		return correoDao.saveAndFlush(correoAEnviar);
	}

	@Transactional(readOnly = true)
	public List<Correo> findByCliente(Cliente cliente) {
		return correoDao.findByCliente(cliente);
	}

	@Transactional(readOnly = true)
	public List<Correo> findByClienteAndRemitenteContaining(
			Cliente edSelectedCliente, String flSearchTerm) {
		// TODO Auto-generated method stub
		return correoDao.findByClienteAndRemitenteContaining(edSelectedCliente,
				flSearchTerm);
	}

}
