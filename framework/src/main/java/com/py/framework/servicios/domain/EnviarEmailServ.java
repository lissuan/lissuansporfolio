package com.py.framework.servicios.domain;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Correo;
import com.py.framework.entidades.Factura;
import com.py.framework.entidades.Usuario;

@Service("enviarEMailServ")
public class EnviarEmailServ {

	@Autowired
	private JavaMailSenderImpl mailSender;
	@Autowired
	private SimpleMailMessage simpleMailMessage;

	public boolean sendMail(Usuario usuarioAdmin, Cliente cliente,
			String textoMail) {
		return true;
	}

	public void sendMail(Correo correoAEnviar, String[] to) {

		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom("no-reply@softwaresnail.com");
			helper.setTo(to);
			helper.setSubject(correoAEnviar.getAsunto());
			helper.setText(correoAEnviar.getTextoCorreo());
			List<File> listaArchivos = new ArrayList<File>();
			for (Factura iFactura : correoAEnviar.getFacturas()) {
				File fileatach = devolverPDF(iFactura.getArchivoPdf(),
						iFactura.getDescripcion());
				helper.addAttachment(fileatach.getName(), fileatach);
//				fileatach.delete();
				listaArchivos.add(fileatach);
			}
			mailSender.send(message);
			eliminarArchivos(listaArchivos);
		} catch (javax.mail.MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void eliminarArchivos(List<File> listaArchivos) {
		for (File file : listaArchivos) {
			file.delete();
		}
	}

	public File devolverPDF(byte[] arregloBytes, String Name) {
		File fileDevolver = null;
		try {
			FileOutputStream fos = new FileOutputStream(Name);
			fos.write(arregloBytes);
			fos.close();
			fileDevolver = new File(Name);
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		return fileDevolver;
	}

	public void eliminarArchivoServidor(String nombre) {
		File auxFile = new File(nombre);
		auxFile.delete();
	}
}
