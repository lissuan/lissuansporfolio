package com.py.framework.servicios.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.CuentaBancaria;
import com.py.framework.entidades.CuentaBancaria.TIPOCUENTA;
import com.py.framework.repositorios.CuentaBancariaRepository;

@Service("cuentaServ")
public class CuentaBancariaServ {

	@Autowired
	private CuentaBancariaRepository cuentaDao;

	@Transactional(readOnly = true)
	public CuentaBancaria saveAndFlush(CuentaBancaria cuentaSalvar) {
		return cuentaDao.saveAndFlush(cuentaSalvar);
	}

	public TIPOCUENTA getTipoCuentaXString(String string) {
		if (string.equals("BANAMEX")) {
			return TIPOCUENTA.BANAMEX;
		}
		if (string.equals("BANCOMER")) {
			return TIPOCUENTA.BANCOMER;
		}
		if (string.equals("BANORTE")) {
			return TIPOCUENTA.BANORTE;
		}
		if (string.equals("IVERLAT")) {
			return TIPOCUENTA.IVERLAT;
		}
		if (string.equals("SANTANDER_HSBC")) {
			return TIPOCUENTA.SANTANDER_HSBC;
		}
		if (string.equals("SERFIN")) {
			return TIPOCUENTA.SERFIN;
		}
		return null;
	}

}
