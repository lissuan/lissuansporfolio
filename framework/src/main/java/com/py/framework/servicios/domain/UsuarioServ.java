package com.py.framework.servicios.domain;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Rol;
import com.py.framework.entidades.Sucursal;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.repositorios.UsuarioRepository;

@Service("usuarioServ")
public class UsuarioServ {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UsuarioRepository usuarioDAO;

	@Autowired
	private EmpresaServ empresaServ;

	@Autowired
	private RolServ rolServ;

	@Autowired
	private SucursalServ sucursalServ;

	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return usuarioDAO.findAll();
	}

	@Transactional
	public void delete(Usuario entity) throws ValorNotFoundException {
		if (entity.getRol() == null) {
			usuarioDAO.delete(entity);
		} else {
			entity.setSucursales(null);
			entity = usuarioDAO.saveAndFlush(entity);
			usuarioDAO.delete(entity);
		}

	}

	@Transactional(readOnly = true)
	public Usuario findUserByEmpresaAndNombreCuenta(Empresa empresa,
			String nombreCuenta) {
		return usuarioDAO
				.findOneByEmpresaAndNombreCuenta(empresa, nombreCuenta);
	}

	@Transactional(readOnly = true)
	public Usuario fetchLoginUser(Long id, String nombreCuenta) {
		Usuario result = usuarioDAO.findOneByEmpresa_IdAndNombreCuenta(id,
				nombreCuenta);
		return result == null ? null : result.getRol().getFuncionalidades()
				.size() > 0 ? result : null;
	}

	@Transactional(readOnly = true)
	public List<Usuario> searchUser(Empresa empresa, String searchTerm,
			Rol rol, Integer activo) {
		return usuarioDAO.searchUser(empresa, "%" + searchTerm + "%", rol,
				activo);
	}

	@Transactional
	public Usuario save(Usuario entity) throws ValorNotFoundException {
		if (entity.getEmpresa() != null) {
			Empresa empresaVerificar = empresaServ.findById(entity.getEmpresa()
					.getId());
			if (empresaVerificar == null) {
				throw new ValorNotFoundException(
						"La empresa asociada no se encuentra almacenada.");
			}
		}
		if (entity.getRol() != null) {
			Rol rolVerificar = rolServ.findOne(entity.getRol().getId());
			if (rolVerificar == null)
				throw new ValorNotFoundException(
						"La empresa asociada no se encuentra almacenada.");
		}
		if (entity.getSucursales().isEmpty()) {
			Set<Sucursal> sucursales = entity.getSucursales();
			for (Sucursal sucursal : sucursales) {
				if (sucursalServ.findOne(sucursal.getId()) == null)
					throw new ValorNotFoundException(
							"La sucursal asociada no se encuentra almacenada.");
			}
		}
		return usuarioDAO.save(entity);
	}

	@Transactional
	public Usuario saveAndFlush(Usuario entity) throws ValorNotFoundException {

		if (entity.getEmpresa() != null) {
			Empresa empresaVerificar = empresaServ.findById(entity.getEmpresa()
					.getId());
			if (empresaVerificar == null) {
				throw new ValorNotFoundException(
						"La empresa asociada no se encuentra almacenada.");
			}
		}
		if (entity.getRol() != null) {
			Rol rolVerificar = rolServ.findOne(entity.getRol().getId());
			if (rolVerificar == null)
				throw new ValorNotFoundException(
						"La empresa asociada no se encuentra almacenada.");
		}
		if (entity.getSucursales().isEmpty()) {
			Set<Sucursal> sucursales = entity.getSucursales();
			for (Sucursal sucursal : sucursales) {
				if (sucursalServ.findOne(sucursal.getId()) == null)
					throw new ValorNotFoundException(
							"La sucursal asociada no se encuentra almacenada.");
			}
		}
		return usuarioDAO.saveAndFlush(entity);
	}

	@Transactional(readOnly = true)
	public Usuario expandeSucursales(Usuario entity) {
		PersistenceUnitUtil uu = em.getEntityManagerFactory()
				.getPersistenceUnitUtil();
		if (!uu.isLoaded(entity, "sucursales")) {
			Usuario tmpEntity = usuarioDAO.findOne(entity.getId());
			tmpEntity.getSucursales().iterator().hasNext();
			entity.setSucursales(tmpEntity.getSucursales());
		}
		return entity;
	}

	@Transactional(readOnly = true)
	public List<Usuario> findByRolAndEmpresa(Rol item, Empresa empresa) {
		return usuarioDAO.findByRolAndEmpresa(item, empresa);
	}

	@Transactional(readOnly = true)
	public Usuario findOne(Long id) {
		return usuarioDAO.findOne(id);
	}
}
