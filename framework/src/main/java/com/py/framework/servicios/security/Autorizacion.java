package com.py.framework.servicios.security;

public final class Autorizacion {
	static private Autorizacion singleton = null;

	static final public String adminCearLobatoApp = "adminCearLobatoApp";

	/**
	 * Seccion de administración: Roles y Usuarios
	 */
	static final public String adminAdministracion = "adminAdministracion";

	// agregadas para roles...
	static final public String adminRoles = "adminRoles";
	static final public String adminRolesAlta = "adminRolesAlta";
	static final public String adminRolesBaja = "adminRolesBaja";
	static final public String adminRolesModificar = "adminRolesModificar";
	static final public String adminRolesPermisos = "adminRolesPermisos";
	// agregadas para usuarios...
	static final public String adminUsuarios = "adminUsuarios";
	static final public String adminUsuariosAlta = "adminUsuariosAlta";
	static final public String adminUsuariosBaja = "adminUsuariosBaja";
	static final public String adminUsuariosModificar = "adminUsuariosModificar";
	static final public String adminUsuariosPermisos = "adminUsuariosPermisos";

	// promociones
	static final public String usuario = "usuario";
	// clientes
	static final public String adminSmsSender = "adminSmsSender";

	private Autorizacion() {
	}

	static public Autorizacion getSingleton() {
		if (singleton == null)
			singleton = new Autorizacion();
		return singleton;
	}
}
