package com.py.framework.servicios.domain;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Sucursal;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.repositorios.SucursalRepository;

@Service("sucursalServ")
public class SucursalServ {
	@PersistenceContext
	private EntityManager em;

	@Autowired
	private SucursalRepository sucursalDAO;

	@Autowired
	private EmpresaServ empresaServ;

	@Transactional
	public Sucursal saveAndFlush(Sucursal entity) throws ValorNotFoundException {

		if (entity != null) {
			if (entity.getEmpresa() != null) {
				Empresa empresaAInsertar = empresaServ.findById(entity
						.getEmpresa().getId());
				if (empresaAInsertar != null) {
					return sucursalDAO.saveAndFlush(entity);
				} else
					throw new ValorNotFoundException(
							"La empresa a que se refiere no se encuentra en la base de datos");
			} else
				throw new ValorNotFoundException(
						"Error creando la sucursal. No se especificó la empresa asociada.");
		} else {
			throw new ValorNotFoundException("La empresa no puede ser nula");
		}
	}

	public void delete(Sucursal selectedSucursal) throws ValorNotFoundException {
		if (selectedSucursal.getUsuarios().isEmpty()) {
			sucursalDAO.delete(selectedSucursal);
		} else
			throw new ValorNotFoundException("La empresa no puede ser nula");

	}

	@Transactional(readOnly = true)
	public List<Sucursal> findAll() {
		return sucursalDAO.findAll(new Sort(Direction.ASC, "descripcion"));
	}

	@Transactional(readOnly = true)
	public List<Sucursal> findByEmpresa(Empresa empresa) {
		return sucursalDAO.findByEmpresa(empresa);
	}

	@Transactional(readOnly = true)
	public List<Sucursal> findByUsuarioAndActiva(Usuario usr, boolean activa) {
		return sucursalDAO.findByActivaAndUsuarios_Id(activa, usr.getId());
	}

	@Transactional(readOnly = true)
	public List<Sucursal> findByUsuarioAndActivaAndEmpresa(Usuario usr,
			boolean activa, Empresa empresa) {
		return sucursalDAO.findByActivaAndEmpresaAndUsuarios_Id(activa,
				empresa, usr.getId());
	}

	@Transactional(readOnly = true)
	public Sucursal expandeUsuarios(Sucursal entity) {
		PersistenceUnitUtil uu = em.getEntityManagerFactory()
				.getPersistenceUnitUtil();
		if (!uu.isLoaded(entity, "usuarios")) {
			Sucursal tmpEntity = sucursalDAO.findOne(entity.getId());
			tmpEntity.getUsuarios().iterator().hasNext();
			entity.setUsuarios(tmpEntity.getUsuarios());
		}
		return entity;
	}

	@Transactional(readOnly = true)
	public Sucursal findByDescripcionAndEmpresa(String descripcion,
			Empresa empresa) {
		return sucursalDAO.findByDescripcionAndEmpresa(descripcion, empresa);
	}

	public HashMap<String, String> llenarTokens(Sucursal entidad,
			HashMap<String, String> diccionario) {
		String llave = "%Sucursal.Nombre%";
		String value = entidad.getDescripcion();
		diccionario.put(llave, value);
		return diccionario;
	}

	@Transactional(readOnly = true)
	public Sucursal getSucursalSistema(Empresa selectedEmpresa) {
		Sucursal findByDescripcionAndEmpresa = sucursalDAO
				.findByDescripcionAndEmpresa(selectedEmpresa.getDescripcion(),
						selectedEmpresa);
		if (findByDescripcionAndEmpresa == null) {
			findByDescripcionAndEmpresa = sucursalDAO.findByEmpresaAndActiva(
					selectedEmpresa, true).get(0);
		}
		return findByDescripcionAndEmpresa;
	}
	@Transactional(readOnly = true)
	public Sucursal findOne(Long id) {
		return sucursalDAO.findOne(id);
	}

}
