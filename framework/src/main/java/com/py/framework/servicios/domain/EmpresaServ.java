package com.py.framework.servicios.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Rol;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.repositorios.EmpresaRepository;

@Service("empresaServ")
public class EmpresaServ {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private EmpresaRepository empresaDAO;

	@Autowired
	private RolServ rolServ;

	@Transactional(readOnly = true)
	public List<Empresa> findAll() {
		List<Empresa> l = empresaDAO.findAll(new Sort(Direction.ASC,
				"descripcion"));
		return l;
	}

	@Transactional(readOnly = true)
	public Empresa findByActivaAndCarpeta(boolean activa, String carpeta) {
		return empresaDAO.findOneByActivaAndCarpeta(activa, carpeta);
	}

	@Transactional(readOnly = true)
	public Empresa findByActivaAndId(boolean activa, Long id) {
		Empresa e = empresaDAO.findOneByActivaAndId(activa, id);
		return e != null ? e : empresaDAO.findOne(1L);
	}

	@Transactional(readOnly = true)
	public Empresa findById(Long id) {
		return empresaDAO.findOne(id);
	}

	@Transactional(readOnly = true)
	public List<Empresa> findByActiva(boolean activa) {
		List<Empresa> l = empresaDAO.findByActiva(activa, new Sort(
				Direction.ASC, "descripcion"));
		return l == null ? new ArrayList<Empresa>(0) : l;
	}

	@Transactional(readOnly = true)
	public List<Empresa> searchEmpresa(String searchTerm, Integer activa) {
		return empresaDAO.searchEmpresa("%" + searchTerm + "%", activa);
	}

	@Transactional
	public Empresa saveAndFlush(Empresa entity) throws ValorNotFoundException {

		if (entity.getCarpeta() == null || entity.getCarpeta().equals("")) {
			throw new ValorNotFoundException(
					"Existen errores al llenar el formulario de la empresa");
		} else
			return empresaDAO.saveAndFlush(entity);
	}

	@Transactional
	public void delete(Empresa entity) throws ValorNotFoundException {
		List<Rol> listaRoles = rolServ.findByEmpresaAndActivos(entity);

		if (!entity.isTieneSucursales() 
				|| listaRoles != null ) {
			throw new ValorNotFoundException(
					"La empresa tiene registros asociados y no se puede eliminar.");
		} else
			empresaDAO.delete(entity);
	}

	@Transactional(readOnly = true)
	public Empresa findByDescripcion(String descripcion) {
		return empresaDAO.findByDescripcion(descripcion);
	}

	@Transactional(readOnly = true)
	public Empresa findByRuc(String ruc) {
		return empresaDAO.findByRuc(ruc);
	}

	public HashMap<String, String> llenarTokens(Empresa entidad,
			HashMap<String, String> diccionario) {
		String llave = "%Empresa.Nombre%";
		String value = entidad.getDescripcion();
		diccionario.put(llave, value);
		return diccionario;
	}

	@Transactional(readOnly = true)
	public List<Empresa> findByDescripcionContainingAndActiva(
			String descripcion, Boolean activa) {

		return empresaDAO.findByDescripcionContainingAndActiva(descripcion,
				activa);
	}

	@Transactional(readOnly = true)
	public Empresa findByCarpeta(String carpeta) {

		return empresaDAO.findByCarpeta(carpeta);
	}
}
