package com.py.framework.servicios.domain;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service("mensageMailSimpleServ")
public class MensageMailSimpleServ extends SimpleMailMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setFrom(String from) {
		// TODO Auto-generated method stub
		super.setFrom(from);
	}

	@Override
	public String getFrom() {
		// TODO Auto-generated method stub
		return super.getFrom();
	}

	@Override
	public void setTo(String to) {
		// TODO Auto-generated method stub
		super.setTo(to);
	}

	@Override
	public void setTo(String[] to) {
		// TODO Auto-generated method stub
		super.setTo(to);
	}

	@Override
	public String[] getTo() {
		// TODO Auto-generated method stub
		return super.getTo();
	}

	@Override
	public void setCc(String cc) {
		// TODO Auto-generated method stub
		super.setCc(cc);
	}

	@Override
	public String[] getCc() {
		// TODO Auto-generated method stub
		return super.getCc();
	}

	@Override
	public void setSubject(String subject) {
		// TODO Auto-generated method stub
		super.setSubject(subject);
	}

	@Override
	public String getSubject() {
		// TODO Auto-generated method stub
		return super.getSubject();
	}

	@Override
	public void setText(String text) {
		// TODO Auto-generated method stub
		super.setText(text);
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return super.getText();
	}

}
