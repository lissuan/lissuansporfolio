package com.py.framework.servicios.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.py.framework.servicios.domain.ContactoServ;
import com.py.framework.servicios.domain.UsuarioServ;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioServ userServ;
	@Autowired
	private ContactoServ contactoServ;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		String idStr = username.substring(0, username.indexOf(':'));
		String userStr = username.substring(username.indexOf(':') + 1);
		UserDetails ud = userServ.fetchLoginUser(Long.decode(idStr), userStr);
		if (ud == null) {
			ud = contactoServ.fetchLoginUser(Long.decode(idStr), userStr);
			if (ud == null) {
				throw new UsernameNotFoundException("El usuario no existe!");
			}
		}
		return ud;
	}
}
