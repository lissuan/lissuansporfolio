package com.py.framework.servicios.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Funcionalidad;
import com.py.framework.entidades.Rol;
import com.py.framework.repositorios.FuncionalidadRepository;

@Service("funcionalidadServ")
public class FuncionalidadServ {

	@Autowired
	private FuncionalidadRepository functionalityDAO;

	@Transactional(readOnly = true)
	public List<Funcionalidad> findAll() {
		return functionalityDAO.findAll();
	}

	@Transactional
	public Funcionalidad saveAndFlush(Funcionalidad entity) {
		return functionalityDAO.saveAndFlush(entity);
	}

	@Transactional(readOnly = true)
	public List<Funcionalidad> findByRol(Rol rol) {
		return functionalityDAO.findByRoles(rol);
	}

	@Transactional(readOnly = true)
	public Funcionalidad findOne(Long id) {
		return functionalityDAO.findOne(id);
	}
}
