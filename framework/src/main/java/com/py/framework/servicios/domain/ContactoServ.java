package com.py.framework.servicios.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Empresa;
import com.py.framework.repositorios.ContactoRepository;

@Service("contactoServ")
public class ContactoServ {

	@Autowired
	private ContactoRepository contactoDao;

	@Transactional(readOnly = true)
	public List<Contacto> findDescripcionContainigH(
			String edNombreContactoTerminoBusqueda) {
		return contactoDao
				.findByNombreContactoContaining(edNombreContactoTerminoBusqueda);
	}

	@Transactional(readOnly = true)
	public List<Contacto> findAll() {
		return contactoDao.findAll();
	}

	@Transactional(readOnly = true)
	public Contacto findByUsuarioCorreo(String edCantidadNiveles) {
		return contactoDao.findByUsuarioCorreo(edCantidadNiveles);
	}

	@Transactional
	public UserDetails fetchLoginUser(Long decode, String userStr) {
		Contacto result = contactoDao.findOneByNombreCuenta(userStr);
		return result == null ? null : result.getRol().getFuncionalidades()
				.size() > 0 ? result : null;
	}

	@Transactional(readOnly = true)
	public List<Contacto> FindByEmpresaAndActivaAndDescripcionContaining(
			Empresa empresa, boolean b, String flSearchTerm) {
		// TODO Auto-generated method stub
		return contactoDao.findByDescripcionContaining(b, flSearchTerm);
	}

	@Transactional
	public Contacto saveAndFlush(Contacto clienteSalvar) {
		return contactoDao.saveAndFlush(clienteSalvar);
	}

	@Transactional(readOnly = true)
	public List<Contacto> findByEmpresaAndDescripcionContaining(
			Empresa empresa, String flSearchTerm) {
		return contactoDao.findByDescripcionContaining(flSearchTerm);
	}

	@Transactional(readOnly = true)
	public Contacto findByCorreoContacto(String string) {
		return contactoDao.findByUsuarioCorreo(string);
	}

	@Transactional
	public void delete(Contacto edSelectedCliente) {
		contactoDao.delete(edSelectedCliente);
	}

	@Transactional(readOnly = true)
	public Contacto findUserByEmpresaAndNombreCuenta(Empresa empresa,
			String string) {
		return contactoDao.findUserByNombreCuenta(string);
	}

	@Transactional(readOnly = true)
	public Contacto findByNombreCuenta(String nombreCuenta) {
		return contactoDao.findOneByNombreCuenta(nombreCuenta);
	}

}
