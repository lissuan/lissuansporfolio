package com.py.framework.servicios.domain;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.py.framework.entidades.Carpeta;
import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Funcionalidad;
import com.py.framework.entidades.Rol;
import com.py.framework.entidades.Sucursal;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.servicios.security.Autorizacion;

@Service("installInitDataServ")
public class InstallInitDataServ {

	@Autowired
	private UsuarioServ usuarioServ;

	@Autowired
	private FuncionalidadServ funcionalidadServ;

	@Autowired
	private CarpetaServ carpetaServ;

	@Autowired
	private RolServ rolServ;

	@Autowired
	private EmpresaServ empresaServ;

	@Autowired
	private SucursalServ sucursalServ;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	@Autowired
	private ExceptionsManager exceptionsManager;
	@Autowired
	private ContactoServ contactoServ;

	public InstallInitDataServ() {
	}

	@SuppressWarnings("unused")
	public void Execute() {

		/**
		 * Empresa
		 */
		Empresa prosur = null;

		try {
			prosur = empresaServ.saveAndFlush(new Empresa("CEAR Lobato",
					"80079268-8", "sistema", true, true));
		} catch (ValorNotFoundException e1) {
			exceptionsManager.reportarExcepcion(e1);
			return;
		}

		/**
		 * - Administracion
		 */
		Funcionalidad administracion = funcionalidadServ
				.saveAndFlush(new Funcionalidad(null, "Administración",
						Autorizacion.adminAdministracion));

		// agregadas para los roles
		Funcionalidad administrarRoles = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administracion,
						"Administrar Roles", Autorizacion.adminRoles));
		Funcionalidad administrarRolesAlta = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarRoles,
						"Crear nuevos Roles", Autorizacion.adminRolesAlta));
		Funcionalidad administrarRolesBaja = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarRoles,
						"Eliminar Roles", Autorizacion.adminRolesBaja));
		Funcionalidad administrarRolesModificar = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarRoles,
						"Modificar Roles", Autorizacion.adminRolesModificar));
		Funcionalidad administrarRolesPermisos = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarRoles,
						"Permisos para Roles", Autorizacion.adminRolesPermisos));

		// agregadas para los roles
		Funcionalidad administrarUsuarios = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administracion,
						"Administrar Usuarios", Autorizacion.adminUsuarios));
		Funcionalidad administrarUsuariosAlta = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarUsuarios,
						"Crear nuevos Usuarios", Autorizacion.adminUsuariosAlta));
		Funcionalidad administrarUsuariosBaja = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarUsuarios,
						"Eliminiar Usuarios", Autorizacion.adminUsuariosBaja));
		Funcionalidad administrarUsuariosModificar = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarUsuarios,
						"Modificar Usuarios",
						Autorizacion.adminUsuariosModificar));
		Funcionalidad administrarUsuariosPermisos = funcionalidadServ
				.saveAndFlush(new Funcionalidad(administrarUsuarios,
						"Permisos para Usuarios",
						Autorizacion.adminUsuariosPermisos));
		// agregadas para la activacion de promociones
		// para los usuarios
		Funcionalidad usuarios = funcionalidadServ
				.saveAndFlush(new Funcionalidad(null, "Usuarios",
						Autorizacion.usuario));
		Funcionalidad adminSms = funcionalidadServ
				.saveAndFlush(new Funcionalidad(null, "Sender",
						Autorizacion.adminSmsSender));

		/**
		 * Carpeta - Administración
		 */
		Carpeta capetaPermisosApp = carpetaServ.saveAndFlush(new Carpeta(null,
				"Cear Lobato", Autorizacion.adminCearLobatoApp,
				new HashSet<Funcionalidad>(Arrays.asList(usuarios))));

		Carpeta capetaUsuario = carpetaServ.saveAndFlush(new Carpeta(
				capetaPermisosApp, "Clientes", Autorizacion.usuario,
				new HashSet<Funcionalidad>(Arrays.asList(usuarios))));

		Carpeta capetaAdministracion = carpetaServ.saveAndFlush(new Carpeta(
				null, "Administración", Autorizacion.adminAdministracion,
				new HashSet<Funcionalidad>(Arrays.asList(administracion,
						administrarRoles, administrarUsuarios))));
		// agregadas para administrar los roles
		Carpeta carpetaAdministrarRoles = carpetaServ.saveAndFlush(new Carpeta(
				capetaAdministracion, "Administrar Roles",
				Autorizacion.adminRoles, new HashSet<Funcionalidad>(Arrays
						.asList(administrarRolesAlta, administrarRolesBaja,
								administrarRolesModificar,
								administrarRolesPermisos))));

		// agregadas para administrar los usuarios
		Carpeta carpetaAdministrarUsuarios = carpetaServ
				.saveAndFlush(new Carpeta(capetaAdministracion,
						"Administrar Usuarios", Autorizacion.adminUsuarios,
						new HashSet<Funcionalidad>(Arrays.asList(
								administrarUsuariosAlta,
								administrarUsuariosBaja,
								administrarUsuariosModificar,
								administrarUsuariosPermisos))));
		Carpeta carpetaAdminSms = carpetaServ.saveAndFlush(new Carpeta(null,
				"Sms", Autorizacion.adminSmsSender, new HashSet<Funcionalidad>(
						Arrays.asList(adminSms))));
		// Carpeta carpetaAdminFacturacion = carpetaServ.saveAndFlush(new
		// Carpeta(
		// capetaAdministracion, "Administrar facturacion",
		// Autorizacion.adminClientesFacturacion,
		// new HashSet<Funcionalidad>(Arrays.asList(
		// adminClientesFacturacionAlta,
		// adminClientesFacturacionBaja,
		// adminClientesFacturacionModificar))));
		Rol clienteUsuario = new Rol("Cliente usuario", true, prosur,
				new HashSet<Funcionalidad>(Arrays.asList(usuarios)));
		try {
			rolServ.saveAndFlush(clienteUsuario);
		} catch (ValorNotFoundException e2) {
			exceptionsManager.reportarExcepcion(e2);
			return;
		}
		/**
		 * Rol
		 */
		Rol administradores = null;
		try {
			administradores = rolServ.saveAndFlush(new Rol(
					"Administradores del sistema", true, prosur,
					new HashSet<Funcionalidad>(Arrays.asList(administracion,
							administrarRoles, administrarRolesAlta,
							administrarRolesBaja, administrarRolesModificar,
							administrarRolesPermisos, administrarUsuarios,
							administrarUsuariosAlta, administrarUsuariosBaja,
							administrarUsuariosModificar,
							administrarUsuariosPermisos))));
		} catch (ValorNotFoundException e1) {
			exceptionsManager.reportarExcepcion(e1);
			return;
		}

		/**
		 * Sucursal
		 */
		Sucursal systema = null;
		try {
			systema = sucursalServ.saveAndFlush(new Sucursal(prosur
					.getDescripcion(), true, prosur));
		} catch (ValorNotFoundException e) {
			exceptionsManager.reportarExcepcion(e);
			return;
		}

		/**
		 * Usuario
		 */
		String contrasenaCodificada = bcryptEncoder.encode("admin");

		try {
			Usuario admin = usuarioServ.saveAndFlush(new Usuario("admin", // **
																			// nombreCuenta
																			// **/
					contrasenaCodificada, // ** contrasena **/
					"Administrador", // ** nombres **/
					"", // ** apellidos **/
					true, // ** activo **/
					false, // ** bloqueado **/
					true, // ** global **/
					null, // ** fechaActualizacionPassword **/
					null, // ** fechaExpiracionCuenta **/
					administradores, // ** rol **/
					prosur, // ** empresa **/
					new HashSet<Sucursal>(Arrays.asList(systema)) // **
																	// sucursales
																	// **/
					));
			Contacto contactoAdmin = new Contacto();
			contactoAdmin.setActivo(true);
			contactoAdmin.setUsuarioCorreo("admin@correo.com");
			contactoAdmin.setBloqueado(false);
			contactoAdmin.setCelular("12345678");
			contactoAdmin.setNombreContacto("Admincontacto");
			contactoAdmin.setNombreCuenta("admin@correo.com");
			contactoAdmin.setContrasena(bcryptEncoder.encode("admin"));
			contactoAdmin = contactoServ.saveAndFlush(contactoAdmin);
			admin.setContacto(contactoAdmin);
			admin = usuarioServ.saveAndFlush(admin);
		} catch (ValorNotFoundException e) {
			exceptionsManager.reportarExcepcion(e);
			return;
		}

	}
}
