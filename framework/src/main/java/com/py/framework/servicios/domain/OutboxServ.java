package com.py.framework.servicios.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.py.framework.entidades.Outbox;
import com.py.framework.entidades.Outbox.Coding;
import com.py.framework.repositorios.OutboxRepository;

@Service("outbouxServ")
public class OutboxServ {

	@Autowired
	private OutboxRepository outboxDao;

	public void persistList(List<String> listaPhones, String mensaje) {
		for (String numero : listaPhones) {
			outboxDao.saveAndFlush(factoriaOutBox(numero, mensaje));
		}
	}

	private Outbox factoriaOutBox(String numero, String mensaje) {
		Outbox outBoxRetornar = new Outbox();
		outBoxRetornar.setDestinationNumber(numero);
		outBoxRetornar.setTextDecoded(mensaje);
		outBoxRetornar.setSenderID("MTigo1");
		outBoxRetornar.setCreatorId("MTigo1");
		outBoxRetornar.setCoding(Coding.Unicode_No_Compression);
		outBoxRetornar.setSendAfter(new Timestamp(new Date().getTime()));
		outBoxRetornar.setSendBefore(new Timestamp(new Date().getTime()));
		return outBoxRetornar;
	}

}
