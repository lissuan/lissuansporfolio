package com.py.framework.servicios.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Empresa;
import com.py.framework.repositorios.ClienteRepository;

@Service("clienteServ")
public class ClienteServ {

	@Autowired
	ClienteRepository clServ;

	@Transactional(readOnly = true)
	public List<Cliente> FindByEmpresaAndActivaAndDescripcionContaining(
			Empresa empresa, boolean b, String flSearchTerm) {
		// TODO Auto-generated method stub
		return clServ.findByEmpresaAndDescripcionContaining(empresa, b,
				flSearchTerm);
	}

	@Transactional
	public Cliente saveAndFlus(Cliente clienteSalvar) {
		return clServ.saveAndFlush(clienteSalvar);
	}

	@Transactional
	public void delete(Cliente edSelectedCliente) {
		clServ.delete(edSelectedCliente);
	}

	@Transactional(readOnly = true)
	public List<Cliente> findByEmpresaAndDescripcionContaining(Empresa empresa,
			String flSearchTerm) {
		return clServ.findByEmpresaAndDescripcionContaining(empresa,
				flSearchTerm);
	}

	@Transactional(readOnly = true)
	public Cliente findByCorreoContacto(String string) {
		return clServ.findByCorreoContacto(string);
	}

	@Transactional(readOnly = true)
	public Cliente finByEmpresaAndUsuario(Empresa empresa, Contacto principal) {
		return clServ.findByEmpresaAndUsuario(empresa, principal);
	}

	@Transactional(readOnly = true)
	public Cliente finByEmpresaAndUsuario(Contacto principalContacto) {
		return clServ.findByUsuario(principalContacto);
	}

	@Transactional(readOnly = true)
	public List<Cliente> findAll() {
		return clServ.findAll();
	}

}
