package com.py.framework.servicios.domain;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.zkoss.bind.BindContext;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.ListModelList;

import au.com.bytecode.opencsv.CSVReader;

@Service("componenteUpLoadArchivo")
public class ComponenteUpLoadArchivo {

	private ListModelList<String> listaEncabezado1 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado2 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado3 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado4 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado5 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado6 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado7 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado8 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado9 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado10 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado11 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado12 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado13 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado14 = new ListModelList<String>();
	private ListModelList<String> listaEncabezado15 = new ListModelList<String>();
	private CSVReader reader;
	private BufferedReader bufferReader;
	private boolean mostrarVentanaEleccion = false;
	private boolean txt = false;
	private boolean csv = false;
	private String direccionDocumento = "";
	private List<String> listaPhones = new ArrayList<>();

	public void InicializarComponente(BindContext ctx) throws IOException {
		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		String dir = Executions.getCurrent().getDesktop().getWebApp()
				.getRealPath("/");
		Media media = upEvent.getMedia();
		String format = media.getFormat();
		dir = dir + media.getName();
		setDireccionDocumento(dir);
		if (format.equals("csv") || format.equals("xls")) {
			setCsv(true);
			setTxt(false);
			cargarCsv(media, dir);
		}
		if (format.equals("txt")) {
			setTxt(true);
			setCsv(false);
			cargarTxt(media, dir);
		}
		mostrarVentanaEleccion = true;
	}

	private void cargarTxt(Media media, String dir) throws IOException {
		String stringData = media.getStringData();
		if (stringData != null) {
			String[] split = stringData.split("\n");
			// cargarStringsEnListas(split);
			addPhoneALista(split);
			Writer writer = new FileWriter(dir);
			BufferedWriter bwriter = new BufferedWriter(writer);
			bwriter.append(stringData);
			bwriter.close();
			FileReader readerFile = new FileReader(dir);
			bufferReader = new BufferedReader(readerFile);
		}
	}

	private void cargarCsv(Media media, String dir) throws IOException {
		File file = new File(dir);
		file.createNewFile();
		if (media.isBinary()) {
			Files.copy(file, media.getStreamData());
			setReader(new CSVReader(new FileReader(dir)));
			// cargarStringsEnListas(getReader().readNext());
			addPhoneALista(getReader());
		} else {
			String stringData = media.getStringData();
			if (stringData != null) {
				String[] split = stringData.split("\n");
				// cargarStringsEnListas(split);
				addPhoneALista(split);
				Writer writer = new FileWriter(dir);
				BufferedWriter bwriter = new BufferedWriter(writer);
				bwriter.append(stringData);
				bwriter.close();
				FileReader readerFile = new FileReader(dir);
				setReader(new CSVReader(readerFile));
			}
		}
	}

	private void addPhoneALista(CSVReader reader2) {
		try {
			List<String[]> readAll = reader2.readAll();
			for (String[] strings : readAll) {
				addPhoneALista(strings);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addPhoneALista(String[] phoneRows) {
		String[] nextLine = phoneRows;
		String[] split = nextLine[0].split(";");
		for (int i = 0; i < split.length; i++) {
			listaPhones.add("0"+split[i]);
		}
	}

	@SuppressWarnings("unused")
	private void cargarStringsEnListas(String[] strings) {
		String[] nextLine = strings;
		String[] split = nextLine[0].split(";");
		for (int i = 0; i < split.length; i++) {
			listaEncabezado1.add(split[i]);
			listaEncabezado2.add(split[i]);
			listaEncabezado3.add(split[i]);
			listaEncabezado4.add(split[i]);
			listaEncabezado5.add(split[i]);
			listaEncabezado6.add(split[i]);
			listaEncabezado7.add(split[i]);
			listaEncabezado8.add(split[i]);
			listaEncabezado9.add(split[i]);
			listaEncabezado10.add(split[i]);
			listaEncabezado11.add(split[i]);
			listaEncabezado12.add(split[i]);
			listaEncabezado13.add(split[i]);
			listaEncabezado14.add(split[i]);
			listaEncabezado15.add(split[i]);
		}
	}

	public void finalizarComponente() {
		listaEncabezado1.clear();
		listaEncabezado2.clear();
		listaEncabezado3.clear();
		listaEncabezado4.clear();
		listaEncabezado5.clear();
		listaEncabezado6.clear();
		listaEncabezado7.clear();
		listaEncabezado8.clear();
		listaEncabezado9.clear();
		listaEncabezado10.clear();
		listaEncabezado11.clear();
		listaEncabezado12.clear();
		listaEncabezado13.clear();
		listaEncabezado14.clear();
		listaEncabezado15.clear();
	}

	public ListModelList<String> getListaEncabezado1() {
		return listaEncabezado1;
	}

	public void setListaEncabezado1(ListModelList<String> listaEncabezado1) {
		this.listaEncabezado1 = listaEncabezado1;
	}

	public ListModelList<String> getListaEncabezado2() {
		return listaEncabezado2;
	}

	public void setListaEncabezado2(ListModelList<String> listaEncabezado2) {
		this.listaEncabezado2 = listaEncabezado2;
	}

	public ListModelList<String> getListaEncabezado3() {
		return listaEncabezado3;
	}

	public void setListaEncabezado3(ListModelList<String> listaEncabezado3) {
		this.listaEncabezado3 = listaEncabezado3;
	}

	public ListModelList<String> getListaEncabezado4() {
		return listaEncabezado4;
	}

	public void setListaEncabezado4(ListModelList<String> listaEncabezado4) {
		this.listaEncabezado4 = listaEncabezado4;
	}

	public ListModelList<String> getListaEncabezado5() {
		return listaEncabezado5;
	}

	public void setListaEncabezado5(ListModelList<String> listaEncabezado5) {
		this.listaEncabezado5 = listaEncabezado5;
	}

	public ListModelList<String> getListaEncabezado6() {
		return listaEncabezado6;
	}

	public void setListaEncabezado6(ListModelList<String> listaEncabezado6) {
		this.listaEncabezado6 = listaEncabezado6;
	}

	public ListModelList<String> getListaEncabezado7() {
		return listaEncabezado7;
	}

	public void setListaEncabezado7(ListModelList<String> listaEncabezado7) {
		this.listaEncabezado7 = listaEncabezado7;
	}

	public ListModelList<String> getListaEncabezado8() {
		return listaEncabezado8;
	}

	public void setListaEncabezado8(ListModelList<String> listaEncabezado8) {
		this.listaEncabezado8 = listaEncabezado8;
	}

	public ListModelList<String> getListaEncabezado9() {
		return listaEncabezado9;
	}

	public void setListaEncabezado9(ListModelList<String> listaEncabezado9) {
		this.listaEncabezado9 = listaEncabezado9;
	}

	public ListModelList<String> getListaEncabezado10() {
		return listaEncabezado10;
	}

	public void setListaEncabezado10(ListModelList<String> listaEncabezado10) {
		this.listaEncabezado10 = listaEncabezado10;
	}

	public ListModelList<String> getListaEncabezado11() {
		return listaEncabezado11;
	}

	public void setListaEncabezado11(ListModelList<String> listaEncabezado11) {
		this.listaEncabezado11 = listaEncabezado11;
	}

	public ListModelList<String> getListaEncabezado12() {
		return listaEncabezado12;
	}

	public void setListaEncabezado12(ListModelList<String> listaEncabezado12) {
		this.listaEncabezado12 = listaEncabezado12;
	}

	public ListModelList<String> getListaEncabezado13() {
		return listaEncabezado13;
	}

	public void setListaEncabezado13(ListModelList<String> listaEncabezado13) {
		this.listaEncabezado13 = listaEncabezado13;
	}

	public ListModelList<String> getListaEncabezado14() {
		return listaEncabezado14;
	}

	public void setListaEncabezado14(ListModelList<String> listaEncabezado14) {
		this.listaEncabezado14 = listaEncabezado14;
	}

	public ListModelList<String> getListaEncabezado15() {
		return listaEncabezado15;
	}

	public void setListaEncabezado15(ListModelList<String> listaEncabezado15) {
		this.listaEncabezado15 = listaEncabezado15;
	}

	/**
	 * @return the bufferReader
	 */
	public BufferedReader getBufferReader() {
		return bufferReader;
	}

	/**
	 * @param bufferReader
	 *            the bufferReader to set
	 */
	public void setBufferReader(BufferedReader bufferReader) {
		this.bufferReader = bufferReader;
	}

	/**
	 * @return the reader
	 */
	public CSVReader getReader() {
		return reader;
	}

	/**
	 * @param reader
	 *            the reader to set
	 */
	public void setReader(CSVReader reader) {
		this.reader = reader;
	}

	/**
	 * @return the mostrarVentanaEleccion
	 */
	public boolean isMostrarVentanaEleccion() {
		return mostrarVentanaEleccion;
	}

	/**
	 * @param mostrarVentanaEleccion
	 *            the mostrarVentanaEleccion to set
	 */
	public void setMostrarVentanaEleccion(boolean mostrarVentanaEleccion) {
		this.mostrarVentanaEleccion = mostrarVentanaEleccion;
	}

	/**
	 * @return the csv
	 */
	public boolean isCsv() {
		return csv;
	}

	/**
	 * @param csv
	 *            the csv to set
	 */
	public void setCsv(boolean csv) {
		this.csv = csv;
	}

	/**
	 * @return the txt
	 */
	public boolean isTxt() {
		return txt;
	}

	/**
	 * @param txt
	 *            the txt to set
	 */
	public void setTxt(boolean txt) {
		this.txt = txt;
	}

	/**
	 * @return the direccionDocumento
	 */
	public String getDireccionDocumento() {
		return direccionDocumento;
	}

	/**
	 * @param direccionDocumento
	 *            the direccionDocumento to set
	 */
	public void setDireccionDocumento(String direccionDocumento) {
		this.direccionDocumento = direccionDocumento;
	}

	public List<String> getListaPhones() {
		return listaPhones;
	}

	public void setListaPhones(List<String> listaPhones) {
		this.listaPhones = listaPhones;
	}

}
