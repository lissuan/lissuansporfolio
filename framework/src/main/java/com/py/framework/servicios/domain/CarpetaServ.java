package com.py.framework.servicios.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.py.framework.entidades.Carpeta;
import com.py.framework.repositorios.CarpetaRepository;

@Service("carpetaServ")
public class CarpetaServ {
	@Autowired
	private CarpetaRepository carpetaDAO;

	@Transactional(readOnly = true)
	public List<Carpeta> findAll() {
		List<Carpeta> l = carpetaDAO.findAll(
				new Sort(Direction.ASC, "llave"));
		/**
		 * Cargar las funcionalidades por LazyLoading
		 */
		for (Carpeta f : l) {
			boolean t = f.getFuncionalidades().size() >= 0;
			if (!t) {
			}
		}
		return l;
	}

	@Transactional
	public Carpeta saveAndFlush(Carpeta entity) {
		return carpetaDAO.saveAndFlush(entity);
	}
}
