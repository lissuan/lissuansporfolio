package com.py.framework.servicios.domain;

import java.util.Properties;

import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

@Scope("prototype")
@Service("mailImplServ")
public class MailImplServ extends JavaMailSenderImpl {

	private String host;
	private int port;
	private String username;
	private Properties javaMailProperties;
	private String password;

	@Override
	public void setPort(int port) {
		// TODO Auto-generated method stub
		super.setPort(port);
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return this.port;
	}

	@Override
	public Properties getJavaMailProperties() {
		// TODO Auto-generated method stub
		return this.javaMailProperties;
	}

	@Override
	public void setHost(String host) {
		// TODO Auto-generated method stub
		this.host = host;
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return this.host;
	}

	@Override
	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username = username;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public void setPassword(String password) {
		// TODO Auto-generated method stub
		this.password =password;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

}
