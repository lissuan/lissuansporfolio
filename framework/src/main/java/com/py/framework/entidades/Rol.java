package com.py.framework.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Clase Entidad para el manejo de <b>ROLES</b>.
 * 
 * @see Funcionalidad
 * @see Usuario
 * 
 * @author <a href="mailto:moyacuba@gmail.com">Iosvany Moya Cruz </a>
 */
@Entity
@Table(name = "ROL")
public class Rol extends SuperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean activo = false;
	private Empresa empresa;
	private Set<Funcionalidad> funcionalidades = new HashSet<Funcionalidad>();
	private Set<Usuario> usuarios = new HashSet<Usuario>();

	/**
	 * Constructor por defecto para {@link Rol}
	 */
	public Rol() {
		super();
	}

	/**
	 * Constructor con datos basicos para {@link Rol}
	 */
	public Rol(String descripcion, boolean activo, Empresa empresa) {
		super(descripcion);
		this.activo = activo;
		this.empresa = empresa;
	}

	/**
	 * Constructor completo para {@link Rol}
	 */
	public Rol(String descripcion, boolean activo, Empresa empresa,
			Set<Funcionalidad> funcionalidades) {
		this(descripcion, activo, empresa);
		for (Funcionalidad f : funcionalidades)
			this.funcionalidades.add(f.addRol(this));
	}

	/**
	 * Está Activo?
	 * 
	 * @return {@link Boolean}
	 */
	@Column(name = "ACTIVO", nullable = false)
	public boolean isActivo() {
		return this.activo;
	}

	/**
	 * Actualiza Activo
	 * 
	 * @param activo
	 *            {@link Boolean}
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para {@link Empresa}
	 * 
	 * @return {@link Empresa}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID")
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para {@link Empresa}
	 * 
	 * @param empresa
	 *            {@link Empresa}
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a muchos para
	 * {@link Funcionalidad}
	 * 
	 * @return {@link Set}<{@link Funcionalidad}>
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "ROLES_FUNCIONALIDADES", joinColumns = { @JoinColumn(name = "ID_ROL", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_FUNCIONALIDAD", referencedColumnName = "ID") })
	public Set<Funcionalidad> getFuncionalidades() {
		return this.funcionalidades;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a muchos para
	 * {@link Funcionalidad}
	 * 
	 * @param funcionalidades
	 *            {@link Set}<{@link Funcionalidad}>
	 */
	public void setFuncionalidades(Set<Funcionalidad> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	/**
	 * Obtiene la asociacion bidirecional uno a muchos para {@link Usuario}
	 * 
	 * @return {@link Set}<{@link Usuario}>
	 */
	@OneToMany(mappedBy = "rol")
	public Set<Usuario> getUsuarios() {
		return this.usuarios;
	}

	/**
	 * Actualiza la asociacion bidirecional uno a muchos para {@link Usuario}
	 * 
	 * @param usuarios
	 *            {@link Set}<{@link Usuario}>
	 */
	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Rol)) {
			return false;
		}
		final Rol o = (Rol) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Transient
	public Rol addUsuario(Usuario usuario) {
		this.usuarios.add(usuario);
		return this;
	}

	@Transient
	public Rol addUsuarioAll(Collection<Usuario> usuarios) {
		this.usuarios.addAll(usuarios);
		return this;
	}

	@Transient
	public Rol addFuncionalidad(Funcionalidad funcionalidad) {
		this.funcionalidades.add(funcionalidad);
		return this;
	}

	@Transient
	public Rol addFuncionalidadAll(Collection<Funcionalidad> funcionalidades) {
		this.funcionalidades.addAll(funcionalidades);
		return this;
	}

	@Transient
	public boolean isGranted(String funcLlave) {
		for (Funcionalidad f : this.funcionalidades) {
			if (f.getLlave().equals(funcLlave))
				return true;
		}
		return false;
	}
}