package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Clase Entidad para el manejo de <b>USUARIOS</b>.
 * 
 * @see Rol
 * @see Empresa
 * @see Sucursal
 * @see UserDetails
 * 
 * @author <a href="mailto:moyacuba@gmail.com">Iosvany Moya Cruz </a>
 */
@Entity
@Table(name = "USUARIO")
@NamedQueries({ @NamedQuery(name = "Usuario.findByNombreCuenta", query = "select u from Usuario u where u.nombreCuenta = ?1") })
public class Usuario extends SuperEntity implements Serializable, UserDetails {
	private static final long serialVersionUID = 1L;

	private static final long PASSWORD_EXPIRATION_PERIOD = 60 * 60 * 24 * 90;

	private String nombreCuenta;
	private String apellidos;
	private String contrasena;
	private boolean activo = false;
	private boolean bloqueado = false;
	private boolean global = false;
	private Timestamp fechaActualizacionPassword = null;
	private Timestamp fechaExpiracionCuenta = null;
	private Rol rol = null;
	private Empresa empresa = null;
	private Set<Sucursal> sucursales = new HashSet<Sucursal>();
	private Contacto contacto = null;

	/**
	 * Constructor por defecto para {@link Usuario}
	 */
	public Usuario() {
		super();
	}

	/**
	 * Constructor con descripcion para {@link Usuario}
	 */
	public Usuario(String nombres) {
		super(nombres);
	}

	/**
	 * Constructor con datos basicos para {@link Usuario}
	 */
	public Usuario(String nombreCuenta, String contrasena, String nombres,
			String apellidos, boolean activo, boolean bloqueado,
			boolean global, Timestamp fechaActualizacionPassword,
			Timestamp fechaExpiracionCuenta, Empresa empresa) {
		this(nombres);
		if (nombreCuenta == null)
			throw new NullPointerException("nombreCuenta");
		this.nombreCuenta = nombreCuenta;
		this.contrasena = contrasena;
		this.apellidos = apellidos;
		this.activo = activo;
		this.bloqueado = bloqueado;
		this.global = global;
		this.fechaActualizacionPassword = fechaActualizacionPassword;
		this.fechaExpiracionCuenta = fechaExpiracionCuenta;
		this.empresa = empresa;
	}

	/**
	 * Constructor completo para {@link Usuario}
	 */
	public Usuario(String nombreCuenta, String contrasena, String nombres,
			String apellidos, boolean activo, boolean bloqueado,
			boolean global, Timestamp fechaActualizacionPassword,
			Timestamp fechaExpiracionCuenta, Rol rol, Empresa empresa,
			Set<Sucursal> sucursales) {
		this(nombreCuenta, contrasena, nombres, apellidos, activo, bloqueado,
				global, fechaActualizacionPassword, fechaExpiracionCuenta,
				empresa);

		this.rol = rol.addUsuario(this);

		for (Sucursal s : sucursales)
			this.sucursales.add(s.addUsuario(this));
	}

	/**
	 * Obtiene el nombreCuenta usado para la autentificación de usuario No puede
	 * retornar <code>null</code>.
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#getUsername()
	 * @return the nombreCuenta (never <code>null</code>)
	 */
	@Column(name = "NOMBRECUENTA", length = 20, nullable = false)
	public String getNombreCuenta() {
		return this.nombreCuenta;
	}

	/**
	 * Actualiza el {@link String} <b>nombreCuenta</b> usado para la
	 * autentificación de usuario. No acepta <code>null</code>.
	 * 
	 * @param nombreCuenta
	 *            {@link String} nunca <b>null</b>!
	 * @throws NullPointerException
	 */
	public void setNombreCuenta(String nombreCuenta) {
		if (nombreCuenta == null)
			throw new NullPointerException("nombreCuenta");
		this.nombreCuenta = nombreCuenta;
	}

	/**
	 * ¿Está Activo?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "ACTIVO", nullable = false)
	public boolean isActivo() {
		return this.activo;
	}

	/**
	 * Actualiza Activo
	 * 
	 * @param activo
	 *            {@link boolean}
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * ¿Esta Bloqueado?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "BLOQUEADO", nullable = false)
	public boolean isBloqueado() {
		return bloqueado;
	}

	/**
	 * Actualiza la marca de Bloqueado
	 * 
	 * @param bloqueado
	 *            {@link boolean}
	 */
	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	/**
	 * ¿Es Global?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "GLOBAL", nullable = false)
	public boolean isGlobal() {
		return global;
	}

	/**
	 * Actualiza la marca de Global
	 * 
	 * @param global
	 *            {@link boolean}
	 */
	public void setGlobal(boolean global) {
		this.global = global;
	}

	/**
	 * Obtiene la ultima fecha de actualizacion de contraseña
	 * 
	 * @return {@link Timestamp}
	 */
	@Column(name = "FECHA_ACTUALIZACION_PASSWORD")
	public Timestamp getFechaActualizacionPassword() {
		return this.fechaActualizacionPassword;
	}

	/**
	 * Actualiza la fecha de actualizacion de contraseña
	 * 
	 * @param fechaActualizacionPassword
	 *            {@link Timestamp}
	 */
	public void setFechaActualizacionPassword(
			Timestamp fechaActualizacionPassword) {
		this.fechaActualizacionPassword = fechaActualizacionPassword;
	}

	/**
	 * Obtiene la fecha de expiración de la cuenta
	 * 
	 * @return
	 */
	@Column(name = "FECHA_EXPIRACION_CUENTA")
	public Timestamp getFechaExpiracionCuenta() {
		return fechaExpiracionCuenta;
	}

	/**
	 * Actualiza la fecha de expiración de la cuenta usando {@link Timestamp}
	 * 
	 * @param fechaExpiracionCuenta
	 */
	public void setFechaExpiracionCuenta(Timestamp fechaExpiracionCuenta) {
		this.fechaExpiracionCuenta = fechaExpiracionCuenta;
	}

	/**
	 * Obtiene los Nombres
	 * 
	 * @return {@link String}
	 */
	@Transient
	public String getNombres() {
		return this.getDescripcion();
	}

	/**
	 * Actualiza los Nombres
	 * 
	 * @param nombres
	 *            {@link String}
	 */
	public void setNombres(String nombres) {
		setDescripcion(nombres);
	}

	/**
	 * Obtiene los Apellidos
	 * 
	 * @return {@link String}
	 */
	@Column(name = "APELLIDOS")
	public String getApellidos() {
		return this.apellidos;
	}

	/**
	 * Actualiza los apellidos
	 * 
	 * @param apellidos
	 *            {@link String}
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/**
	 * Obtiene la contraseña para autentificar el usuario. No puede devolver
	 * <code>null</code>.
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
	 * @return {@link String} the password (never <code>null</code>)
	 */
	@Column(name = "CONTRASENA")
	public String getContrasena() {
		return this.contrasena == null ? "" : this.contrasena;
	}

	/**
	 * Actualiza la contraseña para autentificar el usuario. No acepta
	 * <b>null</b>.
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
	 * @param contrasena
	 *            {@link String}
	 */
	public void setContrasena(String contrasena) {
		if (contrasena == null)
			throw new NullPointerException("La contraseña no puede ser NULL");
		this.contrasena = contrasena;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para {@link Rol}
	 * 
	 * @return {@link Rol}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_ROL", referencedColumnName = "ID")
	public Rol getRol() {
		return this.rol;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para {@link Rol}
	 * 
	 * @param rol
	 *            {@link Rol}
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para {@link Empresa}
	 * 
	 * @return {@link Empresa}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID")
	public Empresa getEmpresa() {
		return this.empresa;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para {@link Empresa}
	 * 
	 * @param empresa
	 *            {@link Empresa}
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a muchos para {@link Rol}
	 * 
	 * @return {@link Set}<{@link Rol}>
	 */
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USUARIOS_SUCURSALES", joinColumns = { @JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_SUCURSAL", referencedColumnName = "ID") })
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a muchos para
	 * {@link Sucursal}
	 * 
	 * @param sucursales
	 *            {@link Set}<{@link Sucursal}>
	 */
	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getUsername()
	 */
	@Transient
	public String getUsername() {
		String username = this.nombreCuenta == null ? "" : this.nombreCuenta;
		return username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getPassword()
	 */
	@Transient
	public String getPassword() {
		String password = this.contrasena == null ? "" : this.contrasena;
		return password;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getAuthorities
	 * ()
	 */
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return rol.getFuncionalidades();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired
	 * ()
	 */
	@Transient
	public boolean isAccountNonExpired() {
		java.util.Date date = new java.util.Date();
		return fechaExpiracionCuenta == null
				|| fechaExpiracionCuenta.getTime() > date.getTime();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked
	 * ()
	 */
	@Transient
	public boolean isAccountNonLocked() {
		return !bloqueado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isCredentialsNonExpired()
	 */
	@Transient
	public boolean isCredentialsNonExpired() {
		return this.fechaActualizacionPassword == null
				|| this.fechaActualizacionPassword.getTime() - 1 > PASSWORD_EXPIRATION_PERIOD;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	@Transient
	public boolean isEnabled() {
		return this.isActivo() && this.rol.isActivo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Usuario)) {
			return false;
		}
		final Usuario o = (Usuario) obj;
		if (o.getId() == null) {
			return false;
		}
		if (this.getId() == null) {
			return false;
		}
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}

	@ManyToOne
	@JoinColumn(name = "ID_CONTACTO", referencedColumnName = "ID")
	public Contacto getContacto() {
		return contacto;
	}

	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}
}