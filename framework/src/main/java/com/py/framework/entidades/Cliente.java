package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Clase Entidad para el manejo de <b>FUNCIONALIDADES</b>.
 * 
 * @see Carpeta
 * @see Rol
 * 
 * @author <a href="mailto:lissuan26@gmail.com">Lissuan Fadraga Artiles</a>
 */
@Entity
@Table(name = "CLIENTE")
public class Cliente extends SuperEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private Empresa empresa;
	private String rasonSocial;
	private String nombreResponsableDirecto;
	private String domocilioFiscal;
	private String col;
	private String municipioDelegacion;
	private String telefono;
	private String codigoPostal;
	private String registroFederalContribuyente;
	private String cuentaBancaria;
	private String nombreCliente;
	private String correoContacto;
	private String telefonoID;
	private String destinatarioFactura;
	private String domicionOrigenServicio;
	private String domicionEntregaServicio;
	private String lineaTransportista;
	private String rutasFrecuente;
	private String costosRutas;
	private String comisionClientes;
	private String personaAutorizadaRecivirServicio;
	private String personasALasQueNotificar;
	private String especificacionesServio;
	private String folioServio;
	private String folioFactura;
	private String instruccionesCliente;
	private String plazoCredito;
	private String domicilioLineaTransportistas;
	private String correoPersonasNotificar;
	private Timestamp horarioInicio;
	private boolean activo;
	private byte[] archivoInformes;
	private Timestamp horarioEntregaServicio;
	private Set<Correo> listaCorreos = new HashSet<Correo>();
	private Set<Factura> listaFacturas = new HashSet<Factura>();
	private Contacto usuario;
	private Set<Contacto> contactos = new HashSet<Contacto>();
//	private CuentaBancaria cuentasBancarias;

	public Cliente(Empresa empresa, boolean activo, String rasonSocial,
			String nombreResponsableDirecto, String domocilioFiscal,
			String col, String municipioDelegacion, String telefono,
			String codigoPostal, String registroFederalContribuyente,
			String cuentaBancaria, String nombreCliente, String correoContacto,
			String telefonoID, String destinatarioFactura,
			String domicionOrigenServicio, String domicionEntregaServicio,
			String lineaTransportista, String rutasFrecuente,
			String costosRutas, String comisionClientes, String plazoCredito,
			String domicilioLineaTransportistas, Timestamp horarioInicio,
			Timestamp horarioEntregaServicio,
			String personaAutorizadaRecivirServicio,
			String personasALasQueNotificar, String especificacionesServio,
			String folioServio, String folioFatura, byte[] archivoInformes,
			String instruccionesCliente) {
		super();
		this.empresa = empresa;
		this.activo = activo;
		this.rasonSocial = rasonSocial;
		this.nombreResponsableDirecto = nombreResponsableDirecto;
		this.domocilioFiscal = domocilioFiscal;
		this.col = col;
		this.municipioDelegacion = municipioDelegacion;
		this.telefono = telefono;
		this.codigoPostal = codigoPostal;
		this.registroFederalContribuyente = registroFederalContribuyente;
		this.cuentaBancaria = cuentaBancaria;
		this.nombreCliente = nombreCliente;
		this.correoContacto = correoContacto;
		this.telefonoID = telefonoID;
		this.destinatarioFactura = destinatarioFactura;
		this.domicionOrigenServicio = domicionOrigenServicio;
		this.domicionEntregaServicio = domicionEntregaServicio;
		this.lineaTransportista = lineaTransportista;
		this.rutasFrecuente = rutasFrecuente;
		this.costosRutas = costosRutas;
		this.comisionClientes = comisionClientes;
		this.plazoCredito = plazoCredito;
		this.domicilioLineaTransportistas = domicilioLineaTransportistas;
		this.horarioInicio = horarioInicio;
		this.horarioEntregaServicio = horarioEntregaServicio;
		this.personaAutorizadaRecivirServicio = personaAutorizadaRecivirServicio;
		this.personasALasQueNotificar = personasALasQueNotificar;
		this.especificacionesServio = especificacionesServio;
		this.folioServio = folioServio;
		this.folioFactura = folioFatura;
		this.archivoInformes = archivoInformes;
		this.instruccionesCliente = instruccionesCliente;
	}

	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cliente(String descripcion) {
		super(descripcion);
		// TODO Auto-generated constructor stub
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Column(name = "ACTIVO", nullable = true)
	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Column(name = "RAZONSOCIAL", nullable = true)
	public String getRasonSocial() {
		return rasonSocial;
	}

	public void setRasonSocial(String rasonSocial) {
		this.rasonSocial = rasonSocial;
	}

	@Column(name = "NOMBRERESPONSABLEDIRECTO", nullable = true)
	public String getNombreResponsableDirecto() {
		return nombreResponsableDirecto;
	}

	public void setNombreResponsableDirecto(String nombreResponsableDirecto) {
		this.nombreResponsableDirecto = nombreResponsableDirecto;
	}

	@Column(name = "DOMICILIOFISCAL", nullable = true)
	public String getDomocilioFiscal() {
		return domocilioFiscal;
	}

	public void setDomocilioFiscal(String domocilioFiscal) {
		this.domocilioFiscal = domocilioFiscal;
	}

	@Column(name = "COL", nullable = true)
	public String getCol() {
		return col;
	}

	public void setCol(String col) {
		this.col = col;
	}

	@Column(name = "MUNICIPIOODELEGACION", nullable = true)
	public String getMunicipioDelegacion() {
		return municipioDelegacion;
	}

	public void setMunicipioDelegacion(String municipioDelegacion) {
		this.municipioDelegacion = municipioDelegacion;
	}

	@Column(name = "TELEFONO", nullable = true)
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column(name = "CODIGOPOSTAL", nullable = true)
	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Column(name = "REGISTROFEDERALCONTRIBUYENTE", nullable = true)
	public String getRegistroFederalContribuyente() {
		return registroFederalContribuyente;
	}

	public void setRegistroFederalContribuyente(
			String registroFederalContribuyente) {
		this.registroFederalContribuyente = registroFederalContribuyente;
	}

	@Column(name = "CUENTABANCARIA", nullable = true)
	public String getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	@Column(name = "NOMBRECLIENNTE", nullable = true)
	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	@Column(name = "CORREOCONTACTO", nullable = true)
	public String getCorreoContacto() {
		return correoContacto;
	}

	public void setCorreoContacto(String correoContacto) {
		this.correoContacto = correoContacto;
	}

	@Column(name = "TELEFONOID", nullable = true)
	public String getTelefonoID() {
		return telefonoID;
	}

	public void setTelefonoID(String telefonoID) {
		this.telefonoID = telefonoID;
	}

	@Column(name = "DESTINATARIOFACTURA", nullable = true)
	public String getDestinatarioFactura() {
		return destinatarioFactura;
	}

	public void setDestinatarioFactura(String destinatarioFactura) {
		this.destinatarioFactura = destinatarioFactura;
	}

	@Column(name = "DOMICILIOORIGENSERVICIO", nullable = true)
	public String getDomicionOrigenServicio() {
		return domicionOrigenServicio;
	}

	public void setDomicionOrigenServicio(String domicionOrigenServicio) {
		this.domicionOrigenServicio = domicionOrigenServicio;
	}

	@Column(name = "DOMICIONENTREGASERVICIO", nullable = true)
	public String getDomicionEntregaServicio() {
		return domicionEntregaServicio;
	}

	public void setDomicionEntregaServicio(String domicionEntregaServicio) {
		this.domicionEntregaServicio = domicionEntregaServicio;
	}

	@Column(name = "LINEATRANSPORTISTA", nullable = true)
	public String getLineaTransportista() {
		return lineaTransportista;
	}

	public void setLineaTransportista(String lineaTransportista) {
		this.lineaTransportista = lineaTransportista;
	}

	@Column(name = "RUTASFRECUENTES", nullable = true)
	public String getRutasFrecuente() {
		return rutasFrecuente;
	}

	public void setRutasFrecuente(String rutasFrecuente) {
		this.rutasFrecuente = rutasFrecuente;
	}

	@Column(name = "PLAZOCREDITO", nullable = true)
	public String getPlazoCredito() {
		return plazoCredito;
	}

	public void setPlazoCredito(String plazoCredito) {
		this.plazoCredito = plazoCredito;
	}

	@Column(name = "FOLIOSERVICIO", nullable = true)
	public String getFolioServio() {
		return folioServio;
	}

	public void setFolioServio(String folioServio) {
		this.folioServio = folioServio;
	}

	@Column(name = "FOLIOFACTURA", nullable = true)
	public String getFolioFactura() {
		return folioFactura;
	}

	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}

	@Column(name = "DOMICILIOLINEATRANSPORTISTA", nullable = true)
	public String getDomicilioLineaTransportistas() {
		return domicilioLineaTransportistas;
	}

	public void setDomicilioLineaTransportistas(
			String domicilioLineaTransportistas) {
		this.domicilioLineaTransportistas = domicilioLineaTransportistas;
	}

	@Column(name = "HORAINICIO", nullable = true)
	public Timestamp getHorarioInicio() {
		return horarioInicio;
	}

	public void setHorarioInicio(Timestamp horarioInicio) {
		this.horarioInicio = horarioInicio;
	}

	@Column(name = "HORARIOENTREGASERVICIO", nullable = true)
	public Timestamp getHorarioEntregaServicio() {
		return horarioEntregaServicio;
	}

	public void setHorarioEntregaServicio(Timestamp horarioEntregaServicio) {
		this.horarioEntregaServicio = horarioEntregaServicio;
	}

	@Column(name = "PERSONAAUTORIZADARECIVIRSERVICIO", nullable = true)
	public String getPersonaAutorizadaRecivirServicio() {
		return personaAutorizadaRecivirServicio;
	}

	public void setPersonaAutorizadaRecivirServicio(
			String personaAutorizadaRecivirServicio) {
		this.personaAutorizadaRecivirServicio = personaAutorizadaRecivirServicio;
	}

	@Column(name = "PERSONASANOTIFICAR", nullable = true)
	public String getPersonasALasQueNotificar() {
		return personasALasQueNotificar;
	}

	public void setPersonasALasQueNotificar(String personasALasQueNotificar) {
		this.personasALasQueNotificar = personasALasQueNotificar;
	}

	@Column(name = "ESPECIFICACIONESSERVICIO", nullable = true)
	public String getEspecificacionesServio() {
		return especificacionesServio;
	}

	public void setEspecificacionesServio(String especificacionesServio) {
		this.especificacionesServio = especificacionesServio;
	}

	@Column(name = "ARCHIVOINFORMES", nullable = true, length = 5000)
	public byte[] getArchivoInformes() {
		return archivoInformes;
	}

	public void setArchivoInformes(byte[] archivoInformes) {
		this.archivoInformes = archivoInformes;
	}

	@Column(name = "INSTRUCCIONESCLIENTE", nullable = true)
	public String getInstruccionesCliente() {
		return instruccionesCliente;
	}

	public void setInstruccionesCliente(String instruccionesCliente) {
		this.instruccionesCliente = instruccionesCliente;
	}

	@Column(name = "COSTOSRUTAS", nullable = true)
	public String getCostosRutas() {
		return costosRutas;
	}

	public void setCostosRutas(String costosRutas) {
		this.costosRutas = costosRutas;
	}

	@Column(name = "COMISIONCLIENTES", nullable = true)
	public String getComisionClientes() {
		return comisionClientes;
	}

	public void setComisionClientes(String comisionClientes) {
		this.comisionClientes = comisionClientes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Cliente)) {
			return false;
		}
		final Cliente o = (Cliente) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CLIENTE_CORREO", joinColumns = { @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_CORREO", referencedColumnName = "ID") })
	public Set<Correo> getListaCorreos() {
		return listaCorreos;
	}

	public void setListaCorreos(Set<Correo> listaCorreos) {
		this.listaCorreos = listaCorreos;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CLIENTE_FACTURA", joinColumns = { @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_FACTURA", referencedColumnName = "ID") })
	public Set<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(Set<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	@ManyToOne
	@JoinColumn(name = "ID_CONTACTO", referencedColumnName = "ID")
	public Contacto getUsuario() {
		return usuario;
	}

	public void setUsuario(Contacto usuario) {
		this.usuario = usuario;
	}

	@Column(name = "CORREOPERSONASNOTIFICAR", nullable = true)
	public String getCorreoPersonasNotificar() {
		return correoPersonasNotificar;
	}

	public void setCorreoPersonasNotificar(String correoPersonasNotificar) {
		this.correoPersonasNotificar = correoPersonasNotificar;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "CLIENTE_CONTACTO", joinColumns = { @JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_CONTACTO", referencedColumnName = "ID") })
	public Set<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(Set<Contacto> contactos) {
		this.contactos = contactos;
	}

//	@OneToOne
//	@JoinColumn(name = "ID_CUENTABANCARIA", referencedColumnName = "ID")
//	public CuentaBancaria getCuentasBancarias() {
//		return cuentasBancarias;
//	}
//
//	public void setCuentasBancarias(CuentaBancaria cuentasBancarias) {
//		this.cuentasBancarias = cuentasBancarias;
//	}


}
