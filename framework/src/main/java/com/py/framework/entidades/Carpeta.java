package com.py.framework.entidades;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The Entity class for the FOLDER database table.
 * 
 * @see Funcionalidad
 * 
 * @author <a href="mailto:moyacuba@gmail.com">Iosvany Moya Cruz </a>
 */
@Entity
@Table(name = "CARPETA")
public class Carpeta extends SuperEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String llave;
	private Carpeta padre;
	private boolean global = false;
	private Set<Carpeta> hijos = new HashSet<Carpeta>();
	private Set<Funcionalidad> funcionalidades = new HashSet<Funcionalidad>();

	/**
	 * Constructor por defecto para {@link Carpeta}
	 */
	public Carpeta() {
		super();
	}

	/**
	 * Constructor con descripcion para {@link Carpeta}
	 */
	public Carpeta(String descripcion) {
		super(descripcion);
	}

	/**
	 * Constructor con datos basicos para {@link Carpeta}
	 */
	public Carpeta(Carpeta padre, String descripcion, String llave) {
		this(descripcion);
		if (padre != null)
			this.padre = padre.addHijo(this);
		this.llave = llave;
	}

	/**
	 * Constructor completo para {@link Carpeta}
	 */
	public Carpeta(Carpeta padre, String descripcion, String llave,
			Set<Funcionalidad> funcionalidades) {
		this(padre, descripcion, llave);
		for (Funcionalidad f : funcionalidades)
			this.funcionalidades.add(f.addCarpeta(this));
	}

	/**
	 * Obtiene la Llave
	 * 
	 * @return {@link String}
	 */
	@Column(name = "LLAVE")
	public String getLlave() {
		return this.llave;
	}

	/**
	 * Actualiza la Llave
	 * 
	 * @param key
	 *            {@link String}
	 */
	public void setLlave(String llave) {
		this.llave = llave;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para padre
	 * {@link Carpeta}
	 * 
	 * @return {@link Carpeta}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_PADRE")
	public Carpeta getPadre() {
		return this.padre;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para padre
	 * {@link Carpeta}
	 * 
	 * @param parent
	 *            {@link Carpeta}
	 */
	public void setPadre(Carpeta padre) {
		this.padre = padre;
	}

	/**
	 * ¿Es Global?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "GLOBAL", nullable = false, columnDefinition="bit default 0")
	public boolean isGlobal() {
		return global;
	}

	/**
	 * Actualiza la marca de Global
	 * 
	 * @param global
	 *            {@link boolean}
	 */
	public void setGlobal(boolean global) {
		this.global = global;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para hijos {@link Set}<
	 * {@link Carpeta}>
	 * 
	 * @return {@link Set}<{@link Carpeta}>
	 */
	@OneToMany(mappedBy = "padre", fetch = FetchType.EAGER)
	public Set<Carpeta> getHijos() {
		return this.hijos;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para hijos
	 * {@link Carpeta}
	 * 
	 * @param children
	 *            {@link Set}<{@link Carpeta}>
	 */
	public void setHijos(Set<Carpeta> hijos) {
		this.hijos = hijos;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a muchos para
	 * {@link Funcionalidad}
	 * 
	 * @return {@link Set}<{@link Funcionalidad}>
	 */
	@ManyToMany
	@JoinTable(name = "CARPETAS_FUNCIONALIDADES", joinColumns = { @JoinColumn(name = "ID_FUNCIONALIDAD", referencedColumnName = "ID") }, inverseJoinColumns = { @JoinColumn(name = "ID_CARPETA", referencedColumnName = "ID") })
	public Set<Funcionalidad> getFuncionalidades() {
		return this.funcionalidades;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a muchos para
	 * {@link Funcionalidad}
	 * 
	 * @param functionalities
	 *            {@link Set}<{@link Funcionalidad}>
	 */
	public void setFuncionalidades(Set<Funcionalidad> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Carpeta)) {
			return false;
		}
		final Carpeta o = (Carpeta) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Transient
	public Carpeta addHijo(Carpeta hijo) {
		this.hijos.add(hijo);
		return this;
	}

	@Transient
	public Carpeta addFuncionalidad(Funcionalidad funcionalidad) {
		this.funcionalidades.add(funcionalidad);
		return this;
	}

	@Transient
	public Carpeta addFuncionalidadAll(Collection<Funcionalidad> funcionalidades) {
		this.funcionalidades.addAll(funcionalidades);
		return this;
	}

}