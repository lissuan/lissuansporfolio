package com.py.framework.entidades;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Clase Entidad para el manejo de <b>SUCURSALES</b>.
 * 
 * @see Empresa
 * @see Usuario
 * 
 * @author <a href="mailto:moyacuba@gmail.com">Iosvany Moya Cruz </a>
 */
@Entity
@Table(name = "SUCURSAL")
public class Sucursal extends SuperEntity {
	private static final long serialVersionUID = 1L;

	private boolean activa = false;
	private Empresa empresa = null;
	private Set<Usuario> usuarios = new HashSet<Usuario>();

	/**
	 * Constructor por defecto para {@link Sucursal}
	 */
	public Sucursal() {
		super();
	}

	/**
	 * Constructor con descripcion para {@link Sucursal}
	 */
	public Sucursal(String descripcion) {
		super(descripcion);
	}

	/**
	 * Constructor con datos basicos para {@link Sucursal}
	 */
	public Sucursal(String descripcion, boolean activa) {
		this(descripcion);
		this.activa = activa;
	}

	/**
	 * Constructor con da
	 */
	public Sucursal(String descripcion, boolean activa, Empresa empresa) {
		this(descripcion, activa);
		this.empresa = empresa;
	}

	/**
	 * Está Activa?
	 * 
	 * @return {@link Boolean}
	 */
	@Column(name = "ACTIVA", nullable = false)
	public boolean isActiva() {
		return activa;
	}

	/**
	 * Actualiza Activa
	 * 
	 * @param activa
	 *            {@link Boolean}
	 */
	public void setActiva(boolean activa) {
		this.activa = activa;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para {@link Empresa}
	 * 
	 * @return {@link Empresa}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID")
	public Empresa getEmpresa() {
		return this.empresa;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para {@link Empresa}
	 * 
	 * @param empresa
	 *            {@link Empresa}
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a muchos para {@link Usuario}
	 * 
	 * @return {@link Set}<{@link Usuario}>
	 */
	@ManyToMany(mappedBy = "sucursales")
	public Set<Usuario> getUsuarios() {
		return this.usuarios;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a muchos para {@link Usuario}
	 * 
	 * @param usuarios
	 *            {@link Set}<{@link Usuario}>
	 */
	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Sucursal)) {
			return false;
		}
		final Sucursal o = (Sucursal) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Transient
	public Sucursal addUsuario(Usuario usuario) {
		this.usuarios.add(usuario);
		return this;
	}

	@Transient
	public Sucursal addUsuarioAll(Collection<Usuario> usuarios) {
		this.usuarios.addAll(usuarios);
		return this;
	}
}