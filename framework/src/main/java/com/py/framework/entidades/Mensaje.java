package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Clase Entidad para el manejo de <b>Mensaje</b>.
 * 
 * @see Turno
 * @see Usuario
 * 
 * @author <a href="mailto:lissuan26@gmail.com">Lissuan Fadraga Artiles</a>
 */
@Entity
@Table(name = "MENSAJE")
public class Mensaje extends SuperEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum Estado {
		ENCOLA, CANCELADO
	}

	

	@Enumerated(EnumType.STRING)
	private Estado estado;
	private Usuario referenciaUsuarioEntrada;
	private Usuario referenciaUsuarioEstado;
	private Empresa empresa;
	private String mensaje;
	private Timestamp fechaEntrada;
	private Timestamp fechaEnvio;
	private Timestamp fechaEnviado;

	public Mensaje() {
		super();
	}

	public Mensaje(String descripcion) {
		super(descripcion);
	}

	public Mensaje(String mensaje, Timestamp fechaEntrada,
			Timestamp fechaEnvio, Timestamp fechaEnviado, String estado) {
		super();
		this.mensaje = mensaje;
		this.fechaEntrada = fechaEntrada;
		this.fechaEnvio = fechaEnvio;
		this.fechaEnviado = fechaEnviado;
	}

	@Column(name = "MENSAJE")
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Column(name = "FECHAENTRADA")
	public Timestamp getFechaEntrada() {
		return fechaEntrada;
	}

	public void setFechaEntrada(Timestamp fechaEntrada) {
		this.fechaEntrada = fechaEntrada;
	}

	@Column(name = "FECHAENVIO")
	public Timestamp getFechaEnvio() {
		return fechaEnvio;
	}

	public void setFechaEnvio(Timestamp fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	@Column(name = "FECHAENVIADO")
	public Timestamp getFechaEnviado() {
		return fechaEnviado;
	}

	public void setFechaEnviado(Timestamp fechaEnviado) {
		this.fechaEnviado = fechaEnviado;
	}

	/**
	 * Obtiene el Estado
	 * 
	 * @return {@link String}
	 */
	@Column(name = "ESTADO")
	public Estado getEstado() {
		return estado;
	}

	/**
	 * Actualiza el Estado
	 * 
	 * @param estado
	 *            {@link String}
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUARIOENTRADA", referencedColumnName = "ID")
	public Usuario getReferenciaUsuarioEntrada() {
		return referenciaUsuarioEntrada;
	}

	public void setReferenciaUsuarioEntrada(Usuario referenciaUsuarioEntrada) {
		this.referenciaUsuarioEntrada = referenciaUsuarioEntrada;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUARIOESTADO", referencedColumnName = "ID")
	public Usuario getReferenciaUsuarioEstado() {
		return referenciaUsuarioEstado;
	}

	public void setReferenciaUsuarioEstado(Usuario referenciaUsuarioEstado) {
		this.referenciaUsuarioEstado = referenciaUsuarioEstado;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPRESA", referencedColumnName = "ID")
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Mensaje)) {
			return false;
		}
		final Mensaje o = (Mensaje) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
