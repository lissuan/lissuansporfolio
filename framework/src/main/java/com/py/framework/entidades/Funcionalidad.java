package com.py.framework.entidades;

import java.io.Serializable;

import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Clase Entidad para el manejo de <b>FUNCIONALIDADES</b>.
 * 
 * @see Carpeta
 * @see Rol
 * 
 * @author <a href="mailto:moyacuba@gmail.com">Iosvany Moya Cruz </a>
 */
@Entity
@Table(name = "FUNCIONALIDAD")
public class Funcionalidad extends SuperEntity implements Serializable,
		GrantedAuthority {
	private static final long serialVersionUID = 1L;

	private String llave;
	private boolean global = false;
	private Funcionalidad padre = null;
	private Set<Funcionalidad> hijos = new HashSet<Funcionalidad>();
	private Set<Carpeta> carpetas = new HashSet<Carpeta>();
	private Set<Rol> roles = new HashSet<Rol>();

	/**
	 * Constructor por defecto para {@link Funcionalidad}
	 */
	public Funcionalidad() {
		super();
	}

	/**
	 * Constructor con descripcion para {@link Funcionalidad}
	 */
	public Funcionalidad(String descripcion) {
		super(descripcion);
	}

	/**
	 * Constructor con datos basicos para {@link Funcionalidad}
	 */
	public Funcionalidad(Funcionalidad padre, String descripcion, String llave) {
		this(descripcion);
		if (padre != null)
			this.padre = padre.addHijo(this);
		this.llave = llave;
	}

	/**
	 * Constructor con datos basicos para {@link Funcionalidad}
	 */
	public Funcionalidad(Funcionalidad padre, String descripcion, String llave,
			Set<Carpeta> carpetas) {
		this(padre, descripcion, llave);
		for (Carpeta c : carpetas)
			this.carpetas.add(c.addFuncionalidad(this));
	}

	/**
	 * Obtiene la Llave
	 * 
	 * @return {@link String}
	 */
	@Column(name = "LLAVE")
	public String getLlave() {
		return this.llave;
	}

	/**
	 * Actualiza la Llave
	 * 
	 * @param llave
	 */
	public void setLlave(String llave) {
		this.llave = llave;
	}

	/**
	 * ¿Es Global?
	 * 
	 * @return {@link boolean}
	 */
	@Column(name = "GLOBAL", nullable = false, columnDefinition="bit default 0")
	public boolean isGlobal() {
		return global;
	}

	/**
	 * Actualiza la marca de Global
	 * 
	 * @param global
	 *            {@link boolean}
	 */
	public void setGlobal(boolean global) {
		this.global = global;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para padre
	 * {@link Funcionalidad}
	 * 
	 * @return {@link Funcionalidad}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_PADRE")
	public Funcionalidad getPadre() {
		return this.padre;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para padre
	 * {@link Funcionalidad}
	 * 
	 * @param padre
	 *            {@link Funcionalidad}
	 */
	public void setPadre(Funcionalidad padre) {
		this.padre = padre;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a uno para hijos
	 * {@link Funcionalidad}
	 * 
	 * @return {@link Set}<{@link Funcionalidad}>
	 */
	@OneToMany(mappedBy = "padre")
	public Set<Funcionalidad> getHijos() {
		return this.hijos;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a uno para hijos
	 * {@link Funcionalidad}
	 * 
	 * @param hijos
	 *            {@link Set}<{@link Funcionalidad}>
	 */
	public void setHijos(Set<Funcionalidad> hijos) {
		this.hijos = hijos;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a muchos para {@link Carpeta}
	 * 
	 * @return {@link Set}<{@link Carpeta}>
	 */
	@ManyToMany(mappedBy = "funcionalidades")
	public Set<Carpeta> getCarpetas() {
		return this.carpetas;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a muchos para {@link Carpeta}
	 * 
	 * @param carpetas
	 *            {@link Set}<{@link Carpeta}>
	 */
	public void setCarpetas(Set<Carpeta> carpetas) {
		this.carpetas = carpetas;
	}

	/**
	 * Obtiene la asociacion bidirecional muchos a muchos para {@link Rol}
	 * 
	 * @return {@link Set}<{@link Rol}>
	 */
	@ManyToMany(mappedBy = "funcionalidades")
	public Set<Rol> getRoles() {
		return this.roles;
	}

	/**
	 * Actualiza la asociacion bidirecional muchos a muchos para {@link Rol}
	 * 
	 * @param roles
	 *            {@link Set}<{@link Rol}>
	 */
	public void setRoles(Set<Rol> roles) {
		this.roles = roles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.GrantedAuthority#getAuthority()
	 */
	@Transient
	public String getAuthority() {
		return llave;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Funcionalidad)) {
			return false;
		}
		final Funcionalidad o = (Funcionalidad) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Transient
	public Funcionalidad addHijo(Funcionalidad hijo) {
		this.hijos.add(hijo);
		return this;
	}

	@Transient
	public Funcionalidad addHijoAll(Collection<Funcionalidad> hijos) {
		this.hijos.addAll(hijos);
		return this;
	}

	@Transient
	public Funcionalidad addCarpeta(Carpeta carpeta) {
		this.carpetas.add(carpeta);
		return this;
	}

	@Transient
	public Funcionalidad addCarpetaAll(Collection<Carpeta> carpetas) {
		this.carpetas.addAll(carpetas);
		return this;
	}

	@Transient
	public Funcionalidad addRol(Rol rol) {
		this.roles.add(rol);
		return this;
	}

	@Transient
	public Funcionalidad addRolAll(Collection<Rol> roles) {
		this.roles.addAll(roles);
		return this;
	}

}