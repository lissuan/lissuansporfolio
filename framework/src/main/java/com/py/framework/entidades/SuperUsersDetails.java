package com.py.framework.entidades;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@MappedSuperclass
public class SuperUsersDetails implements Serializable, UserDetails {

	private static final long serialVersionUID = 1L;

	protected Long id = 0L;
	protected String descripcion = "";
	

	/**
	 * Constructor por defecto para {@link SuperEntity}
	 */
	public SuperUsersDetails() {
	}

	/**
	 * Constructor con {@link String} descripcion
	 */
	public SuperUsersDetails(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el Identificador
	 * 
	 * @return {@link Long}
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public Long getId() {
		return this.id;
	}

	/**
	 * Actualiza el Identificador
	 * 
	 * @param id
	 *            {@link Long}
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Obtiene la Descripcion
	 * 
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", nullable = false)
	public String getDescripcion() {
		return this.descripcion;
	}

	/**
	 * Actualiza la Descripcion
	 * 
	 * @param descripcion
	 *            {@link String}
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SuperUsersDetails)) {
			return false;
		}
		final SuperUsersDetails o = (SuperUsersDetails) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {  
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

}
