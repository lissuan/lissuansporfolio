package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.py.framework.entidades.Mensaje.Estado;

@Entity
@Table(name = "outbox")
public class Outbox extends SuperEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Timestamp updatedInDB;

	private Timestamp insertIntoDB;

	private Timestamp sendingDateTime;

	private Timestamp sendBefore;

	private Timestamp sendAfter;

	private String text;

	private String destinationNumber;

	public enum Coding {
		Default_No_Compression, Unicode_No_Compression, Default_Compression, Unicode_Compression
	}

	@Enumerated
	private Coding coding;

	private String udh;

	private Integer class1;

	private String textDecoded;

	private String multiPart;

	private Integer relativeValidity;

	private String senderID;

	private Timestamp sendingTimeOut;

	private String deliveryReport;
	private String creatorId;

	public Outbox() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Outbox(String descripcion) {
		super(descripcion);
		// TODO Auto-generated constructor stub
	}

	@Column(name = "UpdatedInDB")
	public Timestamp getUpdatedInDB() {
		return updatedInDB;
	}

	public void setUpdatedInDB(Timestamp updatedInDB) {
		this.updatedInDB = updatedInDB;
	}

	@Column(name = "CreatorId")
	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	@Column(name = "InsertIntoDB")
	public Timestamp getInsertIntoDB() {
		return insertIntoDB;
	}

	public void setInsertIntoDB(Timestamp insertIntoDB) {
		this.insertIntoDB = insertIntoDB;
	}

	@Column(name = "SendingDateTime")
	public Timestamp getSendingDateTime() {
		return sendingDateTime;
	}

	public void setSendingDateTime(Timestamp sendingDateTime) {
		this.sendingDateTime = sendingDateTime;
	}

	@Column(name = "SendBefore")
	public Timestamp getSendBefore() {
		return sendBefore;
	}

	public void setSendBefore(Timestamp sendBefore) {
		this.sendBefore = sendBefore;
	}

	@Column(name = "SendAfter")
	public Timestamp getSendAfter() {
		return sendAfter;
	}

	public void setSendAfter(Timestamp sendAfter) {
		this.sendAfter = sendAfter;
	}

	@Column(name = "Text", length = 65535)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Column(name = "DestinationNumber", nullable = false, length = 20)
	public String getDestinationNumber() {
		return destinationNumber;
	}

	public void setDestinationNumber(String destinationNumber) {
		this.destinationNumber = destinationNumber;
	}

	@Column(name = "Coding")
	public Coding getCoding() {
		return coding;
	}

	public void setCoding(Coding coding) {
		this.coding = coding;
	}

	@Column(name = "UDH", length = 65535)
	public String getUdh() {
		return udh;
	}

	public void setUdh(String udh) {
		this.udh = udh;
	}

	@Column(name = "Class")
	public Integer getClass1() {
		return class1;
	}

	public void setClass1(Integer class1) {
		this.class1 = class1;
	}

	@Column(name = "TextDecoded", nullable = false, length = 65535)
	public String getTextDecoded() {
		return textDecoded;
	}

	public void setTextDecoded(String textDecoded) {
		this.textDecoded = textDecoded;
	}

	@Column(name = "MultiPart", length = 6)
	public String getMultiPart() {
		return multiPart;
	}

	public void setMultiPart(String multiPart) {
		this.multiPart = multiPart;
	}

	@Column(name = "RelativeValidity")
	public Integer getRelativeValidity() {
		return relativeValidity;
	}

	public void setRelativeValidity(Integer relativeValidity) {
		this.relativeValidity = relativeValidity;
	}

	@Column(name = "SenderID", length = 255)
	public String getSenderID() {
		return senderID;
	}

	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	@Column(name = "SendingTimeOut")
	public Timestamp getSendingTimeOut() {
		return sendingTimeOut;
	}

	public void setSendingTimeOut(Timestamp sendingTimeOut) {
		this.sendingTimeOut = sendingTimeOut;
	}

	@Column(name = "DeliveryReport", length = 8)
	public String getDeliveryReport() {
		return deliveryReport;
	}

	public void setDeliveryReport(String deliveryReport) {
		this.deliveryReport = deliveryReport;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Outbox)) {
			return false;
		}
		final Outbox o = (Outbox) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

}
