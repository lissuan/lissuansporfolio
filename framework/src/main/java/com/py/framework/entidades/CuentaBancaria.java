package com.py.framework.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CUENTABANCARIA")
public class CuentaBancaria extends SuperEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroCuentaBancaria;

	public enum TIPOCUENTA {
		BANCOMER, BANAMEX, BANORTE, SERFIN, SANTANDER_HSBC, IVERLAT
	}

	@Enumerated(EnumType.STRING)
	private TIPOCUENTA tipoCuenta;

	@Column(name = "NUMEROCUENTABANCARIA")
	public String getNumeroCuentaBancaria() {
		return numeroCuentaBancaria;
	}

	public void setNumeroCuentaBancaria(String numeroCuentaBancaria) {
		this.numeroCuentaBancaria = numeroCuentaBancaria;
	}

	public CuentaBancaria() {
		super();
	}

	public CuentaBancaria(String descripcion) {
		super(descripcion);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CuentaBancaria)) {
			return false;
		}
		final CuentaBancaria o = (CuentaBancaria) obj;
		return this.id.equals(o.getId());
	}

	public CuentaBancaria(String numeroCuentaBancaria, TIPOCUENTA tipoCuenta) {
		super();
		this.numeroCuentaBancaria = numeroCuentaBancaria;
		this.tipoCuenta = tipoCuenta;
	}

	@Column(name = "TIPOCUENTA", nullable = true)
	public TIPOCUENTA getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(TIPOCUENTA tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	//
	// @OneToOne(mappedBy = "cuentasBancarias")
	// public Cliente getCliente() {
	// return cliente;
	// }
	//
	// public void setCliente(Cliente cliente) {
	// this.cliente = cliente;
	// }
//	@ManyToMany(mappedBy = "cuentasBancarias")
//	public Set<Cliente> getClientes() {
//		return clientes;
//	}
//
//	public void setClientes(Set<Cliente> clientes) {
//		this.clientes = clientes;
//	}

}
