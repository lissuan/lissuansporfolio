package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Lissuan
 */
@Entity
@Table(name = "inbox")
public class Inbox extends SuperEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date updatedInDB;

	private Date receivingDateTime;

	private String text;

	private String senderNumber;

	private String coding;

	private String udh;

	private String sMSCNumber;

	private int class1;

	private String textDecoded;

	private String recipientID;

	private String processed;

	public Inbox() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Inbox(String descripcion) {
		super(descripcion);
		// TODO Auto-generated constructor stub
	}

	@Column(name = "UpdatedInDB", nullable = false)
	public Date getUpdatedInDB() {
		return updatedInDB;
	}

	public void setUpdatedInDB(Date updatedInDB) {
		this.updatedInDB = updatedInDB;
	}

	@Column(name = "ReceivingDateTime", nullable = false)
	public Date getReceivingDateTime() {
		return receivingDateTime;
	}

	public void setReceivingDateTime(Date receivingDateTime) {
		this.receivingDateTime = receivingDateTime;
	}

	@Column(name = "Text", nullable = false, length = 65535)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Column(name = "SenderNumber", nullable = false, length = 20)
	public String getSenderNumber() {
		return senderNumber;
	}

	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}

	@Column(name = "Coding", nullable = false, length = 23)
	public String getCoding() {
		return coding;
	}

	public void setCoding(String coding) {
		this.coding = coding;
	}

	@Column(name = "UDH", nullable = false, length = 65535)
	public String getUdh() {
		return udh;
	}

	public void setUdh(String udh) {
		this.udh = udh;
	}

	@Column(name = "SMSCNumber", nullable = false, length = 20)
	public String getsMSCNumber() {
		return sMSCNumber;
	}

	public void setsMSCNumber(String sMSCNumber) {
		this.sMSCNumber = sMSCNumber;
	}

	@Column(name = "Class", nullable = false)
	public int getClass1() {
		return class1;
	}

	public void setClass1(int class1) {
		this.class1 = class1;
	}

	@Column(name = "TextDecoded", nullable = false, length = 65535)
	public String getTextDecoded() {
		return textDecoded;
	}

	public void setTextDecoded(String textDecoded) {
		this.textDecoded = textDecoded;
	}

	@Column(name = "RecipientID", nullable = false, length = 65535)
	public String getRecipientID() {
		return recipientID;
	}

	public void setRecipientID(String recipientID) {
		this.recipientID = recipientID;
	}

	@Column(name = "Processed", nullable = false, length = 6)
	public String getProcessed() {
		return processed;
	}

	public void setProcessed(String processed) {
		this.processed = processed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Inbox)) {
			return false;
		}
		final Inbox o = (Inbox) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
