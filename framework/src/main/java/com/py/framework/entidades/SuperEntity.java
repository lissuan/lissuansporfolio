package com.py.framework.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public class SuperEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	protected Long id = 0L;
	protected String descripcion = "";

	/**
	 * Constructor por defecto para {@link SuperEntity}
	 */
	public SuperEntity() {
	}

	/**
	 * Constructor con {@link String} descripcion
	 */
	public SuperEntity(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el Identificador
	 * 
	 * @return {@link Long}
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public Long getId() {
		return this.id;
	}

	/**
	 * Actualiza el Identificador
	 * 
	 * @param id
	 *            {@link Long}
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Obtiene la Descripcion
	 * 
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", nullable = false)
	public String getDescripcion() {
		return this.descripcion;
	}

	/**
	 * Actualiza la Descripcion
	 * 
	 * @param descripcion
	 *            {@link String}
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Sucursal)) {
			return false;
		}
		final Sucursal o = (Sucursal) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
