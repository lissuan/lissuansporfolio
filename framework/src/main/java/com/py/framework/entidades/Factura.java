package com.py.framework.entidades;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FACTURA")
public class Factura extends SuperEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private byte[] archivoPdf;
	private Timestamp fechaSubida;
	private String tipoFactura;
	private boolean anulada;

	public Factura() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Factura(String descripcion) {
		super(descripcion);
		// TODO Auto-generated constructor stub
	}

	public Factura(byte[] archivoPdf) {
		super();
		this.archivoPdf = archivoPdf;
	}

	@Lob
	@Column(name = "ARCHIVOPDF", nullable = false)
	public byte[] getArchivoPdf() {
		return archivoPdf;
	}

	public void setArchivoPdf(byte[] archivoPdf) {
		this.archivoPdf = archivoPdf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Factura)) {
			return false;
		}
		final Factura o = (Factura) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Column(name = "FECHASUBIDA", nullable = true)
	public Timestamp getFechaSubida() {
		return fechaSubida;
	}

	public void setFechaSubida(Timestamp fechaSubida) {
		this.fechaSubida = fechaSubida;
	}

	@Column(name = "TIPOFACTURA", nullable = true)
	public String getTipoFactura() {
		return tipoFactura;
	}

	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}

	@Column(name = "ANULADA", nullable = true)
	public boolean isAnulada() {
		return anulada;
	}

	public void setAnulada(boolean anulada) {
		this.anulada = anulada;
	}
}
