package com.py.framework.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Clase Entidad para el manejo de <b>EMPRESAS</b>.
 * 
 * @see Sucursal
 * @see Rol
 * @see Usuario
 * @see Servicio
 * @see Cliente
 * 
 * @author <a href="mailto:moyacuba@gmail.com">Iosvany Moya Cruz </a>
 */
@Entity
@Table(name = "EMPRESA")
public class Empresa extends SuperEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String ruc = "";
	private String carpeta = "";
	private boolean activa = false;
	private boolean tieneSucursales = false;

	/**
	 * Constructor por defecto para {@link Empresa}
	 */
	public Empresa() {
		super();
	}

	/**
	 * Constructor con descripcion para {@link Empresa}
	 */
	public Empresa(String descripcion) {
		super(descripcion);
	}

	/**
	 * Constructor con datos basicos para {@link Empresa}
	 */
	public Empresa(String descripcion, String ruc, String carpeta,
			boolean activa, boolean tieneSucursales) {
		this(descripcion);
		this.ruc = ruc;
		this.carpeta = carpeta;
		this.activa = activa;
		this.tieneSucursales = tieneSucursales;
	}

	/**
	 * Está Activa?
	 * 
	 * @return {@link Boolean}
	 */
	@Column(name = "ACTIVA", nullable = false)
	public boolean isActiva() {
		return activa;
	}

	/**
	 * Actualiza Activa
	 * 
	 * @param activa
	 *            {@link Boolean}
	 */
	public void setActiva(boolean activa) {
		this.activa = activa;
	}

	/**
	 * Obtiene el RUC
	 * 
	 * @return {@link String}
	 */
	@Column(name = "RUC", nullable = false)
	public String getRuc() {
		return this.ruc;
	}

	/**
	 * Actualiza el RUC
	 * 
	 * @param ruc
	 *            {@link String}
	 */
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	/**
	 * Obtiene la carpeta
	 * 
	 * @return {@link String}
	 */
	@Column(name = "CARPETA", nullable = false)
	public String getCarpeta() {
		return carpeta;
	}

	/**
	 * Actualiza la carpeta
	 * 
	 * @param carpeta
	 *            {@link String}
	 */
	public void setCarpeta(String carpeta) {
		this.carpeta = carpeta;
	}

	// /**
	// * Obtiene el empresaLabel
	// *
	// * @return {@link String}
	// */
	// @Column(name = "EMPRESA_LABEL", nullable = false)
	// public String getEmpresaLabel() {
	// return empresaLabel;
	// }
	//
	// /**
	// * Actualiza el empresaLabel
	// *
	// * @param carpeta
	// * {@link String}
	// */
	// public void setEmpresaLabel(String empresaLabel) {
	// this.empresaLabel = empresaLabel;
	// }
	//
	// /**
	// * Obtiene el empresasLabel
	// *
	// * @return {@link String}
	// */
	// @Column(name = "EMPRESAS_LABEL", nullable = false)
	// public String getEmpresasLabel() {
	// return empresasLabel;
	// }
	//
	// /**
	// * Actualiza el empresasLabel
	// *
	// * @param empresasLabel
	// * {@link String}
	// */
	// public void setEmpresasLabel(String empresasLabel) {
	// this.empresasLabel = empresasLabel;
	// }
	//
	// /**
	// * Obtiene el sucursalLabel
	// *
	// * @return {@link String}
	// */
	// @Column(name = "SUCURSAL_LABEL", nullable = false)
	// public String getSucursalLabel() {
	// return sucursalLabel;
	// }
	//
	// /**
	// * Actualiza el sucursalLabel
	// *
	// * @param sucursalLabel
	// * {@link String}
	// */
	// public void setSucursalLabel(String sucursalLabel) {
	// this.sucursalLabel = sucursalLabel;
	// }
	//
	// /**
	// * Obtiene el sucursalesLabel
	// *
	// * @return {@link String}
	// */
	// @Column(name = "SUCURSALES_LABEL", nullable = false)
	// public String getSucursalesLabel() {
	// return sucursalesLabel;
	// }
	//
	// /**
	// * Actualiza el sucursalesLabel
	// *
	// * @param sucursalesLabel
	// * {@link String}
	// */
	// public void setSucursalesLabel(String sucursalesLabel) {
	// this.sucursalesLabel = sucursalesLabel;
	// }
	//
	// /**
	// * Obtiene el clienteLabel
	// *
	// * @return {@link String}
	// */
	// @Column(name = "CLIENTE_LABEL", nullable = false)
	// public String getClienteLabel() {
	// return clienteLabel;
	// }
	//
	// /**
	// * Actualiza el clienteLabel
	// *
	// * @param clienteLabel
	// * {@link String}
	// */
	// public void setClienteLabel(String clienteLabel) {
	// this.clienteLabel = clienteLabel;
	// }
	//
	// /**
	// * Obtiene el clientesLabel
	// *
	// * @return {@link String}
	// */
	// @Column(name = "CLIENTES_LABEL", nullable = false)
	// public String getClientesLabel() {
	// return clientesLabel;
	// }
	//
	// /**
	// * Actualiza el clientesLabel
	// *
	// * @param clientesLabel
	// * {@link String}
	// */
	// public void setClientesLabel(String clientesLabel) {
	// this.clientesLabel = clientesLabel;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Transient
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Empresa)) {
			return false;
		}
		final Empresa o = (Empresa) obj;
		return this.id.equals(o.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Transient
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * @return the tieneSucursales
	 */
	@Column(name = "TIENESUCURSALES", nullable = true)
	public boolean isTieneSucursales() {
		return tieneSucursales;
	}

	/**
	 * @param tieneSucursales
	 *            the tieneSucursales to set
	 */
	public void setTieneSucursales(boolean tieneSucursales) {
		this.tieneSucursales = tieneSucursales;
	}

}
