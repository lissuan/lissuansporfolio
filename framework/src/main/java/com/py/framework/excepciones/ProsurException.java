package com.py.framework.excepciones;

@SuppressWarnings("serial")
public class ProsurException extends Exception{

	public ProsurException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProsurException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ProsurException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ProsurException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ProsurException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
