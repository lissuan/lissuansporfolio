package com.py.framework.excepciones;

import org.springframework.stereotype.Component;

@Component("exceptionsManager")
public  class ExceptionsManager {

	private boolean edMostrarExcepcion = false;
    private String edTextoMensaje = ""; 
	
    public void reportarExcepcion(Exception excepcion) {
	    if (excepcion instanceof ProsurException) {
			edMostrarExcepcion = true;
			edTextoMensaje = excepcion.getMessage();
		}
	}

	/**
	 * @return the edMostraExcepcion
	 */
	public boolean isEdMostraExcepcion() {
		return edMostrarExcepcion;
	}

	/**
	 * @param edMostraExcepcion
	 *            the edMostraExcepcion to set
	 */
	public void setEdMostraExcepcion(boolean edMostraExcepcion) {
		this.edMostrarExcepcion = edMostraExcepcion;
	}

	/**
	 * @return the edTextoMensaje
	 */
	public String getEdTextoMensaje() {
		return edTextoMensaje;
	}

	/**
	 * @param edTextoMensaje the edTextoMensaje to set
	 */
	public void setEdTextoMensaje(String edTextoMensaje) {
		this.edTextoMensaje = edTextoMensaje;
	}
}
