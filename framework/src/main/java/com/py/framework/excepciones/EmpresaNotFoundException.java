package com.py.framework.excepciones;

@SuppressWarnings("serial")
public class EmpresaNotFoundException extends ProsurException{

	
	public EmpresaNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmpresaNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EmpresaNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpresaNotFoundException(String message) {
		super(message);
	}

	public EmpresaNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
