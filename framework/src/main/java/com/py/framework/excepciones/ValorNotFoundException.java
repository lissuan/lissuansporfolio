package com.py.framework.excepciones;

@SuppressWarnings("serial")
public class ValorNotFoundException extends ProsurException {

	public ValorNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ValorNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ValorNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ValorNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ValorNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
