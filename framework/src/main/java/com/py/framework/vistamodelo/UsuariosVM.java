package com.py.framework.vistamodelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Rol;
import com.py.framework.entidades.Sucursal;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.RolServ;
import com.py.framework.servicios.domain.SucursalServ;
import com.py.framework.servicios.domain.UsuarioServ;
import com.py.framework.servicios.security.Autorizacion;
import com.py.framework.validadores.FormUsuarioValidator;
import com.py.framework.validadores.NombreCuentaAvailabilityValidator;
import com.py.framework.validadores.PasswordMatchValidator;

public class UsuariosVM {

	@WireVariable
	private EmpresaServ empresaServ;

	@WireVariable
	private UsuarioServ usuarioServ;

	@WireVariable
	private SucursalServ sucursalServ;

	@WireVariable
	private RolServ rolServ;

	@WireVariable 
	private BCryptPasswordEncoder bcryptEncoder;

	@WireVariable
	private FormUsuarioValidator userFormValidator;

	@WireVariable
	private NombreCuentaAvailabilityValidator nombreCuentaAvailabilityValidator;

	@WireVariable
	private PasswordMatchValidator passwordMatchValidator;
	
	@WireVariable
	private ExceptionsManager exceptionsManager;

	/**
	 * Listado de usuarios
	 */
	private Autorizacion autorizacion = Autorizacion.getSingleton();
	private Usuario principal;
	private Empresa empresa;
	private Usuario selectedUser = null;
	private ListModelList<Usuario> listUser = new ListModelList<Usuario>();

	/**
	 * Empresa
	 */
	private Empresa selectedEmpresa;
	private ListModelList<Empresa> listEmpresa = new ListModelList<Empresa>();

	/**
	 * Filter fields
	 */
	private Integer flStatusAll = new Integer(-1);
	private Integer flStatusActive = new Integer(1);
	private Integer flStatusInactive = new Integer(0);
	private Integer flStatus = flStatusAll;
	private String flSearchTerm = "";
	private Rol flRol = null;
	private ListModelList<Rol> flAllRoles = new ListModelList<Rol>();
	/**
	 * Edit fields
	 */
	private List<Object> edModified = null;
	private boolean edNuevo = false;
	private String edFirstName = "";
	private String edLastName = "";
	private String edNombreCuenta = "";
	private String edNewPassword1 = "";
	private String edNewPassword2 = "";
	private Rol edRol = null;
	private boolean edActive = false;
	private ListModelList<Rol> edAllRoles = new ListModelList<Rol>();
	private ListModelList<Sucursal> edAllSucursales = new ListModelList<Sucursal>();
	private String edConfirmaBorrarUsuario = "";
	private String edConfirmaCerrarUsuario = "";
	private Sucursal selectedSucursal = null;
	private List<Sucursal> sucursalesMarcadas = new ArrayList<Sucursal>();
	private boolean mostrarVentanaBusquedaRealizada = false;
	private String edConfirmaBorrarUsuarioLogueado = "";

	@Init
	public void init() {
		edAllSucursales.setMultiple(true);
		principal = (Usuario) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		empresa = principal.getEmpresa();
		listEmpresa.addAll(empresaServ.findByActiva(true));
		selectedEmpresa = empresa;
		loadEmpresaData();
		edModified = new ArrayList<Object>();
		// cmdDoSearch();
	}

	public void loadEmpresaData() {
		edAllSucursales.clear();
		edAllSucursales.addAll(sucursalServ.findByEmpresa(selectedEmpresa));
		List<Rol> t = rolServ.findByEmpresaOrderByDescripcion(selectedEmpresa);
		edAllRoles.clear();
		edAllRoles.addAll(t);
		flAllRoles.clear();
		flAllRoles.add(null);
		flAllRoles.addAll(t);
	}

	public boolean hasRole(String role) {
		Collection<? extends GrantedAuthority> l = principal.getAuthorities();
		for (GrantedAuthority i : l) {
			if (i.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	@Command
	@NotifyChange({ "selectedUser", "listUser", "edNuevo", "edNewPassword1",
			"edNewPassword2", "edNombreCuenta", "exceptionsManager" })
	public void cmdUsuarioGuardar() {
		if (this.edNuevo) {
			String encClave = this.bcryptEncoder.encode(edNewPassword1);
			Usuario selectedUser1 = new Usuario();
			selectedUser1.setNombreCuenta(edNombreCuenta);
			selectedUser1.setContrasena(encClave);
			selectedUser1.setNombres(edFirstName);
			selectedUser1.setApellidos(edLastName);
			selectedUser1.setActivo(edActive);
			selectedUser1.setRol(edRol);
			selectedUser1.setEmpresa(selectedEmpresa);
			if (!selectedEmpresa.isTieneSucursales()) {
				if (edAllSucursales.getSelection().isEmpty()) {
					List<Sucursal> sucursales = new ArrayList<Sucursal>();
					sucursales
							.add(sucursalServ.getSucursalSistema(selectedEmpresa));
					edAllSucursales.setSelection(sucursales);
				}	
			}
			
			selectedUser1.setSucursales(edAllSucursales.getSelection());
			try {
				selectedUser1 = usuarioServ.saveAndFlush(selectedUser1);
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				exceptionsManager.reportarExcepcion(e);
				return;
			}
			listUser.add(selectedUser1);
			edNuevo = false;
			edModified.clear();
			edNewPassword1 = "";
			edNewPassword2 = "";
			edNombreCuenta = "";
			selectedUser1 = null;
			resetEdit();
		} else {
			if (edRol != null) {
				this.selectedUser.setRol(edRol);
			}
			selectedUser.setNombres(edFirstName);
			selectedUser.setApellidos(edLastName);
			selectedUser.setNombreCuenta(edNombreCuenta);
			selectedUser.setActivo(edActive);
			selectedUser.setSucursales(edAllSucursales.getSelection());
			int index = listUser.indexOf(selectedUser);
			Usuario saved = null;
			try {
				saved = usuarioServ.save(this.selectedUser);
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				exceptionsManager.reportarExcepcion(e);
				return;
			}
			if (index >= 0) {
				listUser.set(index, saved);
			} else {
				listUser.add(saved);
			}
			selectedUser = null;
			edModified.clear();
			edNewPassword1 = "";
			edNewPassword2 = "";
			edNombreCuenta = "";
			edNuevo = false;
			selectedUser = null;
			resetEdit();
		}
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarUsuario" })
	public void cmdUsuarioCerrarConfirmar() {
		edConfirmaCerrarUsuario = "Hay cambios que no se han guardado. ¿Desea cerrar sin guardar?";
	}

	@NotifyChange("exceptionsManager")
	@Command
	public void cmdCerrarVentanaException() {
		exceptionsManager.setEdMostraExcepcion(false);
		exceptionsManager.setEdTextoMensaje("");
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarUsuario" })
	public void cmdUsuarioCerrarCancelar() {
		edConfirmaCerrarUsuario = "";
	}

	@Command
	@NotifyChange({ "selectedUser", "edConfirmaCerrarUsuario" })
	public void cmdUsuarioCerrar() {
		edConfirmaCerrarUsuario = "";
		resetEdit();
		selectedUser = null;
	}

	@Command
	@NotifyChange({ "listUser" })
	public void cmdDoSearch() {
		listUser.clear();
		listUser.addAll(usuarioServ.searchUser(selectedEmpresa, flSearchTerm,
				flRol, flStatus == -1 ? null : flStatus));

	}

	@Command
	@NotifyChange({ "listUser" })
	public void cmdDoSearchParametros(@BindingParam("item") Integer flStatus1) {
		listUser.clear();
		listUser.addAll(usuarioServ.searchUser(selectedEmpresa, flSearchTerm,
				flRol, flStatus1));
	}

	@Command
	@NotifyChange({ "selectedUser", "edNuevo", "edHaveAssignments",
			"edListSucursalItems", "edIsPartiallyAssigned", "edDirty",
			"edNewPassword1", "edNewPassword2", "edNombreCuenta" })
	public void cmdUsuarioNuevoMostrar() {
		resetEdit();
		edNombreCuenta = "";
		edNewPassword1 = "";
		edNewPassword2 = "";
		edNuevo = true;
		edRol = edAllRoles.get(0);
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarUsuario" })
	public void cmdUsuarioBorrarConfirmar() {
		edConfirmaBorrarUsuario = "¿Desea borrar el usuario '"
				+ selectedUser.getNombreCuenta() + "'?";
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarUsuario" })
	public void cmdUsuarioBorrarCancelar() {
		edConfirmaBorrarUsuario = "";
	}

	@Command
	@NotifyChange({ "selectedUser", "listUser", "edConfirmaBorrarUsuario",
			"edConfirmaBorrarUsuarioLogueado","exceptionsManager" })
	public void cmdUsuarioBorrar() {
		if (!selectedUser.getNombreCuenta().equals(principal.getNombreCuenta())) {
			listUser.remove(selectedUser);
			try {
				usuarioServ.delete(selectedUser);
			} catch (ValorNotFoundException e) {
				exceptionsManager.reportarExcepcion(e);
				return;
			}
			edConfirmaBorrarUsuario = "";
			resetEdit();
			selectedUser = null;
		} else {
			edConfirmaBorrarUsuarioLogueado = "No se puede borrar el mismo usuario con que está conectador.";
		}
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarUsuarioLogueado",
			"edConfirmaBorrarUsuario" })
	public void cmdCerrarNoSePuedeBorrarUsuario() {
		edConfirmaBorrarUsuarioLogueado = "";
		edConfirmaBorrarUsuario = "";
	}

	@Command
	@NotifyChange({ "edDirty" })
	public void cmdSucursalSelection() {

		sucursalesMarcadas.add(selectedSucursal);

	}

	@NotifyChange({ "edFirstName", "edLastName", "edNombreCuenta",
			"edNewPassword1", "edNewPassword2", "edAllSucursales",
			"selectedUser" })
	public void resetEdit() {
		edNuevo = false;
		edFirstName = "";
		edLastName = "";
		edNombreCuenta = "";
		edNewPassword1 = "";
		edNewPassword2 = "";
		edRol = null;
		edActive = false;
		edAllSucursales.clearSelection();
		edModified.clear();
		selectedUser = null;
	}

	@DependsOn({ "listUser" })
	@NotifyChange({ "mostrarVentanaBusquedaRealizada" })
	public void mostrarVentanaBusquedaRealizada() {
		mostrarVentanaBusquedaRealizada = true;
	}

	@Command
	@NotifyChange({ "mostrarVentanaBusquedaRealizada" })
	public void cmdNoMostrarVentanaBusqueda() {
		mostrarVentanaBusquedaRealizada = false;
	}

	public FormUsuarioValidator getUserFormValidator() {
		return userFormValidator;
	}

	public void setUserFormValidator(FormUsuarioValidator userFormValidator) {
		this.userFormValidator = userFormValidator;
	}

	public NombreCuentaAvailabilityValidator getNombreCuentaAvailabilityValidator() {
		return nombreCuentaAvailabilityValidator;
	}

	public void setNombreCuentaAvailabilityValidator(
			NombreCuentaAvailabilityValidator nombreCuentaAvailabilityValidator) {
		this.nombreCuentaAvailabilityValidator = nombreCuentaAvailabilityValidator;
	}

	public PasswordMatchValidator getPasswordMatchValidator() {
		return passwordMatchValidator;
	}

	public void setPasswordMatchValidator(
			PasswordMatchValidator passwordMatchValidator) {
		this.passwordMatchValidator = passwordMatchValidator;
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	/**
	 * @return the empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Usuario getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(Usuario selectedUser) {
		int index = listUser.lastIndexOf(selectedUser);
		selectedUser = usuarioServ.expandeSucursales(selectedUser);
		listUser.set(index, selectedUser);
		this.selectedUser = selectedUser;
		edAllSucursales.setSelection(selectedUser.getSucursales());
		edRol = selectedUser.getRol();
		edNuevo = false;
		edFirstName = selectedUser.getNombres();
		edLastName = selectedUser.getApellidos();
		edNombreCuenta = selectedUser.getNombreCuenta();
		edActive = selectedUser.isActivo();
	}

	public Sucursal getSelectedSucursal() {
		return selectedSucursal;
	}

	public void setSelectedSucursal(Sucursal selectedSucursal) {
		this.selectedSucursal = selectedSucursal;
	}

	public ListModelList<Usuario> getListUser() {
		return this.listUser;
	}

	public void setListUser(ListModelList<Usuario> listUser) {
		this.listUser = listUser;
	}

	public List<Sucursal> getSucursalesMarcadas() {
		return sucursalesMarcadas;
	}

	public void setSucursalesMarcadas(List<Sucursal> sucursalesMarcadas) {
		this.sucursalesMarcadas = sucursalesMarcadas;
	}

	public Empresa getSelectedEmpresa() {
		return selectedEmpresa;
	}

	@NotifyChange({ "listUser", "selectedUser", "edAllSucursales",
			"edAllRoles", "flAllRoles","selectedEmpresa" })
	public void setSelectedEmpresa(Empresa selectedEmpresa) {
		this.selectedEmpresa = selectedEmpresa;
		// this.nombreCuentaAvailabilityValidator.setEmpresa(selectedEmpresa);
		this.selectedUser = null;
		loadEmpresaData();
		resetEdit();
		// cmdDoSearch();
	}

	public ListModelList<Empresa> getListEmpresa() {
		return listEmpresa;
	}

	public void setListEmpresa(ListModelList<Empresa> listEmpresa) {
		this.listEmpresa = listEmpresa;
	}

	public Integer getFlStatusAll() {
		return flStatusAll;
	}

	public void setFlStatusAll(Integer flStatusAll) {
		this.flStatusAll = flStatusAll;
	}

	public Integer getFlStatusActive() {
		return flStatusActive;
	}

	public void setFlStatusActive(Integer flStatusActive) {
		this.flStatusActive = flStatusActive;
	}

	public Integer getFlStatusInactive() {
		return flStatusInactive;
	}

	public void setFlStatusInactive(Integer flStatusInactive) {
		this.flStatusInactive = flStatusInactive;
	}

	public Integer getFlStatus() {
		return flStatus;
	}

	@NotifyChange("listUser")
	public void setFlStatus(Integer flStatus) {
		this.flStatus = flStatus;
	}

	public String getFlSearchTerm() {
		return flSearchTerm;
	}

	@NotifyChange("listUser")
	public void setFlSearchTerm(String flSearchTerm) {
		this.flSearchTerm = flSearchTerm;
	}

	public Rol getFlRol() {
		return flRol;
	}

	@NotifyChange("listUser")
	public void setFlRol(Rol flRol) {
		this.flRol = flRol;
	}

	public ListModelList<Rol> getFlAllRoles() {
		return flAllRoles;
	}

	public void setFlAllRoles(ListModelList<Rol> flAllRoles) {
		this.flAllRoles = flAllRoles;
	}

	@DependsOn({ "edNuevo", "edModified" })
	public boolean getEdDirty() {
		boolean ms;
		if (selectedUser != null) {
			Set<Sucursal> s = edAllSucursales.getSelection();
			ms = !s.equals(selectedUser.getSucursales());
		} else
			ms = false;
		return edNuevo || edModified.size() != 0 || ms;
	}

	public List<Object> getEdModified() {
		return edModified;
	}

	public void setEdModified(Object key, boolean modified) {
		if (modified) {
			if (edModified.contains(key))
				edModified.remove(key);
		} else {
			if (!edModified.contains(key))
				edModified.add(key);
		}
	}

	@DependsOn({ "edNuevo", "selectedUser" })
	public boolean getEdNuevo() {
		return edNuevo;
	}

	public void setEdNuevo(boolean edNuevo) {
		this.edNuevo = edNuevo;
	}

	@DependsOn({ "edFirstName", "selectedUser" })
	public String getEdFirstName() {
		return edFirstName;
	}

	@NotifyChange({ "edFirstName", "edModified", "dDirty" })
	public void setEdFirstName(String edFirstName) {
		this.edFirstName = edFirstName;
		setEdModified("edFirstName", edNuevo ? edFirstName.isEmpty()
				: this.edFirstName.equals(selectedUser.getNombres()));
	}

	@DependsOn({ "edLastName", "selectedUser" })
	public String getEdLastName() {
		return edLastName;
	}

	@NotifyChange({ "edLastName", "edModified", "dDirty" })
	public void setEdLastName(String edLastName) {
		this.edLastName = edLastName;
		setEdModified("edLastName", edNuevo ? edLastName.isEmpty()
				: this.edLastName.equals(selectedUser.getApellidos()));
	}

	@DependsOn({ "edNombreCuenta", "selectedUser" })
	public String getEdNombreCuenta() {
		return edNombreCuenta;
	}

	@NotifyChange({ "edNombreCuenta", "edModified", "dDirty" })
	public void setEdNombreCuenta(String edNombreCuenta) {
		this.edNombreCuenta = edNombreCuenta;
		setEdModified("edNombreCuenta", edNuevo ? edNombreCuenta.isEmpty()
				: this.edNombreCuenta.equals(selectedUser.getNombreCuenta()));
	}

	@DependsOn({ "edNewPassword1", "selectedUser" })
	public String getEdNewPassword1() {

		return edNewPassword1;
	}

	@NotifyChange({ "edNewPassword1", "edModified", "dDirty" })
	public void setEdNewPassword1(String edNewPassword1) {
		this.edNewPassword1 = edNewPassword1;
		setEdModified(
				"edPassword",
				(this.edNewPassword1.isEmpty() && this.edNewPassword2.isEmpty()));
	}

	@DependsOn({ "edNewPassword2", "selectedUser" })
	public String getEdNewPassword2() {
		return edNewPassword2;
	}

	@NotifyChange({ "edNewPassword2", "edModified", "dDirty" })
	public void setEdNewPassword2(String edNewPassword2) {
		this.edNewPassword2 = edNewPassword2;
		setEdModified(
				"edPassword",
				(this.edNewPassword1.isEmpty() && this.edNewPassword2.isEmpty()));
	}

	public boolean isMostrarVentanaBusquedaRealizada() {
		return mostrarVentanaBusquedaRealizada;
	}

	public void setMostrarVentanaBusquedaRealizada(
			boolean mostrarVentanaBusquedaRealizada) {
		this.mostrarVentanaBusquedaRealizada = mostrarVentanaBusquedaRealizada;
	}

	@DependsOn({ "edRol", "selectedUser" })
	public Rol getEdRol() {
		return edRol;
	}

	@NotifyChange({ "edRol", "edModified", "dDirty" })
	public void setEdRol(Rol edRol) {
		this.edRol = edRol;
		setEdModified("edRol",
				edNuevo ? false : this.edRol.equals(selectedUser.getRol()));
	}

	@DependsOn({ "edActive", "selectedUser" })
	public boolean getEdActive() {
		return edActive;
	}

	@NotifyChange({ "edActive", "edModified", "dDirty" })
	public void setEdActive(boolean edActive) {
		this.edActive = edActive;
		setEdModified("edActive", edNuevo ? false
				: this.edActive == selectedUser.isActivo());
	}

	public ListModelList<Rol> getEdAllRoles() {
		return edAllRoles;
	}

	public void setEdAllRoles(ListModelList<Rol> edAllRoles) {
		this.edAllRoles = edAllRoles;
	}

	public ListModelList<Sucursal> getEdAllSucursales() {
		return edAllSucursales;
	}

	public void setEdAllSucursales(ListModelList<Sucursal> edAllSucursales) {
		this.edAllSucursales = edAllSucursales;
	}

	@DependsOn({ "edConfirmaBorrarUsuario" })
	public String getEdConfirmaBorrarUsuario() {
		return edConfirmaBorrarUsuario;
	}

	public void setEdConfirmaBorrarUsuario(String edConfirmaBorrarUsuario) {
		this.edConfirmaBorrarUsuario = edConfirmaBorrarUsuario;
	}

	@DependsOn({ "edConfirmaCerrarUsuario" })
	public String getEdConfirmaCerrarUsuario() {
		return edConfirmaCerrarUsuario;
	}

	public void setEdConfirmaCerrarUsuario(String edConfirmaCerrarUsuario) {
		this.edConfirmaCerrarUsuario = edConfirmaCerrarUsuario;
	}

	public Autorizacion getAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(Autorizacion autorizacion) {
		this.autorizacion = autorizacion;
	}

	public String getEdConfirmaBorrarUsuarioLogueado() {
		return edConfirmaBorrarUsuarioLogueado;
	}

	public void setEdConfirmaBorrarUsuarioLogueado(
			String edConfirmaBorrarUsuarioLogueado) {
		this.edConfirmaBorrarUsuarioLogueado = edConfirmaBorrarUsuarioLogueado;
	}

	public ExceptionsManager getExceptionsManager() {
		return exceptionsManager;
	}

	public void setExceptionsManager(ExceptionsManager exceptionsManager) {
		this.exceptionsManager = exceptionsManager;
	}
}
