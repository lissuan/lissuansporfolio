package com.py.framework.vistamodelo;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Outbox;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.servicios.domain.ComponenteUpLoadArchivo;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.InboxServ;
import com.py.framework.servicios.domain.OutboxServ;
import com.py.framework.validadores.sms.AbstractSmsValidator;

public class EnvioSmsVM {

	@WireVariable
	private EmpresaServ empresaServ;

	@WireVariable
	private ComponenteUpLoadArchivo componenteUpLoadArchivo;

	@WireVariable
	private ExceptionsManager exceptionsManager;

	@WireVariable
	private InboxServ inboxServ;

	@WireVariable
	private OutboxServ outbouxServ;

	@WireVariable
	private AbstractSmsValidator abstractSmsValidator;
	/**
	 * Permisos
	 */
	private Usuario principal;
	private Empresa empresa;

	private String telefono = null;
	private ListModelList<String> listaTelefonos = new ListModelList<>();
	private String mensaje = "";
	private String caracteresRestantes = "Caracteres restantes: ";
	private boolean archivoSeleccionado = false;

	@Init
	public void init() {
		principal = (Usuario) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		empresa = principal.getEmpresa();
	}

	public boolean hasRole(String role) {
		Collection<? extends GrantedAuthority> l = principal.getAuthorities();
		for (GrantedAuthority i : l) {
			if (i.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	@NotifyChange({ "listaTelefonos", "telefono" })
	@Command
	public void cmdNuevoTelefono() {
		if (!listaTelefonos.contains(telefono)) {
			listaTelefonos.add(telefono);
		}
		telefono = "";
	}

	@NotifyChange({ "archivoSeleccionado" })
	@Command
	public void onUpLoadArchivo(
			@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) {
		try {
			componenteUpLoadArchivo.InicializarComponente(ctx);
			componenteUpLoadArchivo.finalizarComponente();
			if (componenteUpLoadArchivo.getListaPhones().size() > 0) {
				archivoSeleccionado = true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
		}
	}

	@NotifyChange({ "mensaje" })
	@Command
	public void cmdEnviarSms() {
		List<String> listaPhones = componenteUpLoadArchivo.getListaPhones();
		outbouxServ.persistList(listaPhones, mensaje);
	}

	@NotifyChange({ "exceptionsManager", "edConfirmaCerrarCliente" })
	@Command
	public void cmdCerrarVentanaException() {
		exceptionsManager.setEdMostraExcepcion(false);
		exceptionsManager.setEdTextoMensaje("");
	}

	@NotifyChange({ "caracteresRestantes" })
	@Command
	public void mostrarCaracteresRestantes() {
		caracteresRestantes = "Caracteres restantes: ";
		int resta = 160 - mensaje.length();
		caracteresRestantes = caracteresRestantes + (String.valueOf(resta));
	}

	public EmpresaServ getEmpresaServ() {
		return empresaServ;
	}

	public void setEmpresaServ(EmpresaServ empresaServ) {
		this.empresaServ = empresaServ;
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public ListModelList<String> getListaTelefonos() {
		return listaTelefonos;
	}

	public void setListaTelefonos(ListModelList<String> listaTelefonos) {
		this.listaTelefonos = listaTelefonos;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ComponenteUpLoadArchivo getComponenteUpLoadArchivo() {
		return componenteUpLoadArchivo;
	}

	public void setComponenteUpLoadArchivo(
			ComponenteUpLoadArchivo componenteUpLoadArchivo) {
		this.componenteUpLoadArchivo = componenteUpLoadArchivo;
	}

	public ExceptionsManager getExceptionsManager() {
		return exceptionsManager;
	}

	public void setExceptionsManager(ExceptionsManager exceptionsManager) {
		this.exceptionsManager = exceptionsManager;
	}

	public AbstractSmsValidator getAbstractSmsValidator() {
		return abstractSmsValidator;
	}

	public void setAbstractSmsValidator(
			AbstractSmsValidator abstractSmsValidator) {
		this.abstractSmsValidator = abstractSmsValidator;
	}

	public String getCaracteresRestantes() {
		return caracteresRestantes;
	}

	public void setCaracteresRestantes(String caracteresRestantes) {
		this.caracteresRestantes = caracteresRestantes;
	}

	public boolean isArchivoSeleccionado() {
		return archivoSeleccionado;
	}

	public void setArchivoSeleccionado(boolean archivoSeleccionado) {
		this.archivoSeleccionado = archivoSeleccionado;
	}

}
