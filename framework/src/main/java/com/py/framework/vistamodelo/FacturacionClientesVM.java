package com.py.framework.vistamodelo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelList;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Correo;
import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Factura;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.servicios.domain.ClienteServ;
import com.py.framework.servicios.domain.ContactoServ;
import com.py.framework.servicios.domain.CorreoServ;
import com.py.framework.servicios.domain.CuentaBancariaServ;
import com.py.framework.servicios.domain.EnviarEmailServ;
import com.py.framework.servicios.domain.FacturaServ;
import com.py.framework.servicios.domain.RolServ;
import com.py.framework.servicios.domain.UsuarioServ;
import com.py.framework.validadores.AbstractClienteValidator;
import com.py.framework.validadores.configCorreo.AbstractConfigCorreoValidator;
import com.py.framework.validadores.contacto.AbstractContactosValidator;

public class FacturacionClientesVM {

	@WireVariable
	private EnviarEmailServ enviarEMailServ;
	@WireVariable
	private ClienteServ clienteServ;
	@WireVariable
	private ExceptionsManager exceptionsManager;
	@WireVariable
	private AbstractClienteValidator abstractClienteValidator;
	@WireVariable
	private FacturaServ facturaServ;
	@WireVariable
	private CorreoServ correoServ;
	@WireVariable
	private RolServ rolServ;
	@WireVariable
	private UsuarioServ usuarioServ;
	@WireVariable
	private BCryptPasswordEncoder bcryptEncoder;
	@WireVariable
	private AbstractConfigCorreoValidator abstractConfigCorreoValidator;
	@WireVariable
	private ContactoServ contactoServ;
	@WireVariable
	private AbstractContactosValidator abstractContactosValidator;
	@WireVariable
	private CuentaBancariaServ cuentaServ;

	/**
	 * para config el mail
	 * */
	private boolean edConfigurarCorreoMostrar = false;
	private String edConfigHost;
	private int edConfigPort;
	private String edConfigPassword;
	private String edConfigUserName;

	/**
	 * Filter fields
	 */
	private Integer flStatusAll = new Integer(-1);
	private Integer flStatusActive = new Integer(1);
	private Integer flStatusInactive = new Integer(0);
	private Integer flStatus = flStatusAll;
	private String flSearchTerm = "";

	/**
	 * Permisos
	 */
	private Usuario principal;
	private Empresa empresa;

	/**
	 * Para los contactos
	 * */
	private ListModelList<Contacto> listaContactos = new ListModelList<Contacto>();
	private ListModelList<Contacto> listContactosGuardar = new ListModelList<Contacto>();
	private Contacto edSelectedContacto = null;
	private boolean mostrarBuscarContactos = false;
	private boolean mostrarAgregarContactos = false;
	private String edUsuarioCorreo = "";
	private String edCelular = "";
	private String edTelefono = "";
	private String edIdNextel = "";
	private String edNombreContacto = "";
	private String edNombreContactoTerminoBusqueda = "";
	private boolean edNuevoContacto = false;
	private boolean edNuevaCuentaBancaria = false;

	/**
	 * Pa las cuentas
	 * */
	private ListModelList<String> listaStringCuentas = new ListModelList<String>();
	private String edSelectedStringCuenta = "";
	/**
	 * Propias de facturacionClienteVM
	 * */
	private ListModelList<Cliente> listaClientes = new ListModelList<Cliente>();
	private Cliente edSelectedCliente = null;
	private ListModelList<Factura> listaAdjuntos = new ListModelList<Factura>();
	private Factura edSelectedFactura = null;
	private String edConfirmaBorrarCliente = "";
	private boolean edNuevo;
	private boolean mostraVentaEnviarMail;
	private String textoMail = "";
	private String para = "";
	private String cc = "";
	private String asunto = "";
	private byte[] adjunto = null;

	/**
	 * generales para el East
	 * */
	private String edConfirmaCerrarEste = "";
	private String edConfirmaBorrarElemento = "";
	private String edConfirmaCerrarCancelar = "";

	/**
	 * añadidos entidad
	 * */
	private String rasonSocial = "";
	private String nombreResponsableDirecto = "";

	private String domocilioFiscal = "'";
	private String col = "";
	private String municipioDelegacion = "";
	private String telefono = "";
	private String codigoPostal = "";
	private String registroFederalContribuyente = "";
	private String cuentaBancaria = "";
	private String nombreCliente = "";
	private String correoContacto = "";
	private String telefonoID = "";
	private String destinatarioFactura = "";
	private String domicionOrigenServicio = "";
	private String domicionEntregaServicio = "";
	private String lineaTransportista = "";
	private String rutasFrecuente = "";
	private String costosRutas = "";
	private String comisionClientes = "";
	private String plazoCredito = "";
	private String domicilioLineaTransportistas = "";
	private Date horarioInicio = null;
	private Date horarioEntregaServicio = null;
	private String personaAutorizadaRecivirServicio = "";
	private String personasALasQueNotificar = "";
	private String especificacionesServio = "";
	private String folioServio = "";
	private String folioFactura = "";
	private byte[] archivoInformes = null;
	private String instruccionesCliente = "";
	private String correoPersonasNotificar;

	// private ListModelList<CuentaBancaria> listaCuentasBancarias = new
	// ListModelList<CuentaBancaria>();
	// private CuentaBancaria edSelectedCuentaBancaria = null;

	@org.zkoss.bind.annotation.Init
	public void Init() {
		principal = (Usuario) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		empresa = principal.getEmpresa();
		// listaCuentasBancarias.clear();
		listaClientes.clear();
		listaClientes.addAll(clienteServ.findAll());
		listaStringCuentas.add("BANCOMER");
		listaStringCuentas.add("BANAMEX");
		listaStringCuentas.add("BANORTE");
		listaStringCuentas.add("SERFIN");
		listaStringCuentas.add("SANTANDER_HSBC");
		listaStringCuentas.add("IVERLAT");
	}

	/**
	 * Para los contactos
	 * */
	@Command
	@NotifyChange({ "listaContactos" })
	public void cmdContactosBuscar() {
		listaContactos.clear();
		listaContactos.addAll(contactoServ
				.findDescripcionContainigH(edNombreContactoTerminoBusqueda));
	}

	@Command
	@NotifyChange({ "mostrarBuscarContactos", "listContactosGuardar",
			"edNombreContacto", "listaContactos" })
	public void cmdMostrarEleccionContactos() {
		mostrarBuscarContactos = true;
		listaContactos.clear();
		listaContactos.addAll(contactoServ.findAll());
		edNombreContacto = "";
	}

	@Command
	@NotifyChange({ "mostrarAgregarContactos", "edUsuarioCorreo", "edCelular",
			"edTelefono", "edIdNextel", "edNombreContacto", "edNuevoContacto",
			"listaContactos" })
	public void cmdMostrarAgregarContactos() {
		mostrarAgregarContactos = true;
		edUsuarioCorreo = "";
		edCelular = "";
		edTelefono = "";
		edIdNextel = "";
		edNombreContacto = "";
		edNuevoContacto = true;
		listaContactos.clear();
	}

	@Command
	@NotifyChange({ "listContactosGuardar", "edSelectedContacto" })
	public void cmdQuitarElementoDeListaGuardar() {
		listContactosGuardar.remove(edSelectedContacto);
		edSelectedContacto = null;
	}

	@Command
	@NotifyChange({ "mostrarBuscarContactos", "listaContactos" })
	public void cmdCancelarAgregarContactos() {
		listaContactos.clear();
		mostrarBuscarContactos = false;
	}

	@Command
	@NotifyChange({ "mostrarAgregarContactos", "edNuevoContacto" })
	public void cmdCancelarAgregarContacto() {
		mostrarAgregarContactos = false;
		edNuevoContacto = false;
	}

	@Command
	@NotifyChange({ "mostrarBuscarContactos" })
	public void cmdSalvarContactos() {
		mostrarBuscarContactos = false;
	}

	@Command
	@NotifyChange({ "listaContactos", "listContactosGuardar" })
	public void cmdPasarIzquierdaADerecha() {
		if (!listaContactos.contains(edSelectedContacto)) {
			listaContactos.add(edSelectedContacto);
		}
		listContactosGuardar.remove(edSelectedContacto);

	}

	@Command
	@NotifyChange({ "listaContactos", "listContactosGuardar" })
	public void cmdPasarDerechaAIzquierda() {
		if (!listContactosGuardar.contains(edSelectedContacto)) {
			listaContactos.remove(edSelectedContacto);
			listContactosGuardar.add(edSelectedContacto);
		}
	}

	@Command
	@NotifyChange({ "listaContactos", "edNuevoContacto",
			"mostrarAgregarContactos" })
	public void cmdSalvarContacto() {
		Contacto contactoSalvar = null;
		if (edNuevoContacto) {
			contactoSalvar = new Contacto();
		} else {
			contactoSalvar = edSelectedContacto;
		}
		contactoSalvar.setDescripcion(edNombreContacto);
		contactoSalvar.setCelular(edCelular);
		contactoSalvar.setIdNextel(edIdNextel);
		contactoSalvar.setTelefono(edTelefono);
		contactoSalvar.setNombreContacto(edNombreContacto);
		contactoSalvar.setNombreCuenta(edUsuarioCorreo);
		contactoSalvar.setActivo(false);
		contactoSalvar.setUsuarioCorreo(edUsuarioCorreo);
		contactoSalvar = contactoServ.saveAndFlush(contactoSalvar);
		if (edNuevoContacto) {
			listaContactos.add(contactoSalvar);
			edNuevoContacto = false;
			mostrarAgregarContactos = false;
		} else {

		}
	}

	// @Command
	// @NotifyChange({ "listaCuentasBancarias", "edNuevaCuentaBancaria",
	// "exceptionsManager", "cuentaBancaria", "edSelectedCuentaBancaria" })
	// public void cmdGuardarCuentaBancaria() {
	// CuentaBancaria cuentaSalvar = null;
	// if (edNuevaCuentaBancaria) {
	// cuentaSalvar = new CuentaBancaria();
	// } else {
	// cuentaSalvar = edSelectedCuentaBancaria;
	// }
	// cuentaSalvar.setDescripcion(edSelectedStringCuenta);
	// if (cuentaBancaria == null || cuentaBancaria.isEmpty()) {
	// try {
	// throw new ValorNotFoundException(
	// "No puede estar vacio el numero de cuenta");
	//
	// } catch (Exception e) {
	// exceptionsManager.reportarExcepcion(e);
	// return;
	// }
	// }
	// cuentaSalvar.setNumeroCuentaBancaria(cuentaBancaria);
	// cuentaSalvar.setTipoCuenta(cuentaServ
	// .getTipoCuentaXString(edSelectedStringCuenta));
	// listaCuentasBancarias.add(cuentaSalvar);
	// edSelectedCuentaBancaria = cuentaSalvar;
	// edNuevaCuentaBancaria = false;
	// }
	//
	// @Command
	// @NotifyChange({ "edNuevaCuentaBancaria" })
	// public void cmdAgregarCuentaBancariaMostrar() {
	// edNuevaCuentaBancaria = true;
	// }
	//
	// @Command
	// @NotifyChange({ "edNuevaCuentaBancaria" })
	// public void cmdCerrarCuentaBancaria() {
	// edNuevaCuentaBancaria = false;
	// }
	//
	// @Command
	// @NotifyChange({ "listaCuentasBancarias", "cuentaBancaria" })
	// public void cmdEliminarCuentaBancaria() {
	// listaCuentasBancarias.remove(edSelectedCuentaBancaria);
	// cuentaBancaria = "";
	// }

	@Command
	public boolean hasRole(String role) {
		Collection<? extends GrantedAuthority> l = principal.getAuthorities();
		for (GrantedAuthority i : l) {
			if (i.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	@Command
	@NotifyChange({ "listaClientes" })
	public void cmdDoSearch() {
		listaClientes.clear();
		List<Cliente> listaClienteAux = clienteServ
				.findByEmpresaAndDescripcionContaining(empresa, flSearchTerm);
		if (listaClienteAux != null) {
			listaClientes.addAll(listaClienteAux);
		}
	}

	@Command
	@NotifyChange({ "listaClientes" })
	public void cmdDoSearchParams(@BindingParam("item") Integer flStatus1) {
		listaClientes.clear();
		List<Cliente> listaClienteAux = null;
		if (flStatus1 == 1) {
			listaClienteAux = clienteServ
					.FindByEmpresaAndActivaAndDescripcionContaining(empresa,
							true, flSearchTerm);
			if (listaClienteAux != null) {
				listaClientes.addAll(listaClienteAux);
			}
		}
		if (flStatus1 == 0) {
			listaClienteAux = clienteServ
					.FindByEmpresaAndActivaAndDescripcionContaining(empresa,
							false, flSearchTerm);
			if (listaClienteAux != null) {
				listaClientes.addAll(listaClienteAux);
			}

		}
		if (flStatus1 != 0 && flStatus1 != 1) {
			listaClienteAux = clienteServ
					.findByEmpresaAndDescripcionContaining(empresa,
							flSearchTerm);
			if (listaClienteAux != null) {
				listaClientes.addAll(listaClienteAux);
			}
		}
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarEste" })
	public void cmdClienteCerrarConfirmar() {
		setEdConfirmaCerrarEste("Desea cerrar?");
	}

	@Command
	@NotifyChange({ "edNuevo", "edSelectedCliente", "rasonSocial",
			"nombreResponsableDirecto", "domocilioFiscal", "col",
			"municipioDelegacion", "telefono", "codigoPostal",
			"registroFederalContribuyente", "cuentaBancaria", "nombreCliente",
			"correoContacto", "telefonoID", "destinatarioFactura",
			"domicionOrigenServicio", "domicionEntregaServicio",
			"lineaTransportista", "rutasFrecuente", "costosRutas",
			"comisionClientes", "plazoCredito", "domicilioLineaTransportistas",
			"horarioInicio", "horarioEntregaServicio",
			"personaAutorizadaRecivirServicio", "personasALasQueNotificar",
			"especificacionesServio", "folioServio", "folioFactura",
			"archivoInformes", "instruccionesCliente", "edSelectedCliente",
			"listaAdjuntos", "correoPersonasNotificar", "listaContactos",
			"listaCuentasBancarias", "listContactosGuardar" })
	public void cmdClienteNuevo() {
		edNuevo = true;
		edSelectedCliente = null;
		listaAdjuntos.clear();
		resetearCampos();
	}

	@Command
	@NotifyChange({})
	public void onUploadFactura(
			@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx)
			throws Exception {
		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		String dir = Executions.getCurrent().getDesktop().getWebApp()
				.getRealPath("/");
		Media media = upEvent.getMedia();
		// String format = media.getFormat();
		dir = dir + media.getName();
		File file = new File(dir);
		file.createNewFile();
		if (media.isBinary()) {
			archivoInformes = media.getByteData();
		} else {
			String stringData = media.getStringData();
			if (stringData != null) {
				// String[] split = stringData.split("\n");
				Writer writer = new FileWriter(dir);
				BufferedWriter bwriter = new BufferedWriter(writer);
				bwriter.append(stringData);
				bwriter.close();
			}
		}
		if (archivoInformes != null) {
			Factura factura = new Factura();
			factura.setDescripcion(media.getName());
			factura.setArchivoPdf(archivoInformes);
			factura.setFechaSubida(new Timestamp(new java.util.Date().getTime()));
			factura.setTipoFactura(media.getFormat());
			listaAdjuntos.add(factura);
		}
	}

	@Command
	@NotifyChange({ "exceptionsManager" })
	public void cmdDescargarElemento() {
		File auxGuardar = enviarEMailServ.devolverPDF(
				edSelectedFactura.getArchivoPdf(),
				edSelectedFactura.getDescripcion());
		try {
			Filedownload.save(auxGuardar, null);
		} catch (FileNotFoundException e) {
			exceptionsManager.reportarExcepcion(e);
		}
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarElemento" })
	public void cmdClienteBorrarConfirmar() {
		setEdConfirmaBorrarElemento("¿Desea borrar el cliente'"
				+ edSelectedCliente.getDescripcion() + "'?");
	}

	@NotifyChange({ "edNuevo", "edConfirmaCerrarEste", "edSelectedCliente" })
	@Command
	public void cerrarVentanaInformacionCerrar() {
		edNuevo = false;
		edConfirmaCerrarEste = "";
		edSelectedCliente = null;
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarEste" })
	public void cmdCerrarCancelar() {
		edConfirmaCerrarEste = "";
	}

	@NotifyChange({ "exceptionsManager", "edConfirmaCerrarCliente" })
	@Command
	public void cmdCerrarVentanaException() {
		exceptionsManager.setEdMostraExcepcion(false);
		exceptionsManager.setEdTextoMensaje("");
	}

	@NotifyChange({ "mostraVentaEnviarMail", "para" })
	@Command
	public void cmdMostrarEnviarMail() {
		mostraVentaEnviarMail = true;
		if (edSelectedCliente != null) {
			para = edSelectedCliente.getCorreoContacto();
			for (Contacto iContacto : edSelectedCliente.getContactos()) {
				if (!para.contains(iContacto.getNombreCuenta())) {
					para = para + ";" + iContacto.getNombreCuenta();
				}
			}
		}
	}

	@Command
	@NotifyChange({ "mostraVentaEnviarMail" })
	public void cmdCancelarEnviarMail() {
		mostraVentaEnviarMail = false;
	}

	@Command
	@NotifyChange({ "mostraVentaEnviarMail", "edConfigurarCorreoMostrar",
			"exceptionsManager" })
	public void cmdEnviarMail() {
		para = para + ";" + cc;
		String[] contactos = para.split(";");
		Set<Cliente> listaCliente = new HashSet<Cliente>();
		for (int i = 0; i < contactos.length; i++) {
			Cliente clienteAdicionar = clienteServ
					.findByCorreoContacto(contactos[i]);
			if (clienteAdicionar != null) {
				listaCliente.add(clienteAdicionar);
			}
		}
		Correo correoAEnviar = new Correo();
		if (listaCliente.isEmpty()) {
			try {
				throw new ValorNotFoundException(
						"No estan registrados los clientes");
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				exceptionsManager.reportarExcepcion(e);
				return;
			}
		}
		correoAEnviar.setCliente(listaCliente);
		correoAEnviar.setCopiaA(cc);
		Set<Factura> listaFacturas = new HashSet<Factura>();
		for (Factura iFactura : listaAdjuntos) {
			iFactura = facturaServ.saveAndFlush(iFactura);
			listaFacturas.add(iFactura);
		}
		correoAEnviar.setFacturas(listaFacturas);
		correoAEnviar.setRemitente(principal.getUsername());
		correoAEnviar.setTextoCorreo(textoMail);
		correoAEnviar.setRemitido(para);
		correoAEnviar.setAsunto(asunto);
		correoAEnviar = correoServ.saveAndFlush(correoAEnviar);
		for (Cliente icliente : listaCliente) {
			if (icliente.getListaCorreos() != null) {
				icliente.getListaCorreos().add(correoAEnviar);
			} else {
				Set<Correo> nuevaLista = new HashSet<Correo>();
				nuevaLista.add(correoAEnviar);
			}
			clienteServ.saveAndFlus(icliente);
		}
		enviarEMailServ.sendMail(correoAEnviar, contactos);

		mostraVentaEnviarMail = false;
	}

	@Command
	@NotifyChange({ "edNuevo", "rasonSocial", "nombreResponsableDirecto",
			"domocilioFiscal", "col", "municipioDelegacion", "telefono",
			"codigoPostal", "registroFederalContribuyente", "cuentaBancaria",
			"nombreCliente", "correoContacto", "telefonoID",
			"destinatarioFactura", "domicionOrigenServicio",
			"domicionEntregaServicio", "lineaTransportista", "rutasFrecuente",
			"costosRutas", "comisionClientes", "plazoCredito",
			"domicilioLineaTransportistas", "horarioInicio",
			"horarioEntregaServicio", "personaAutorizadaRecivirServicio",
			"personasALasQueNotificar", "especificacionesServio",
			"folioServio", "folioFactura", "archivoInformes",
			"instruccionesCliente", "edSelectedCliente", "listaAdjuntos",
			"correoPersonasNotificar", "listaContactos",
			"listaCuentasBancarias", "listContactosGuardar" })
	public void cmdClienteNuevoSalvar() {
		Cliente clienteSalvar = null;
		if (edNuevo) {
			clienteSalvar = new Cliente();
		} else {
			clienteSalvar = edSelectedCliente;
		}

		clienteSalvar.setArchivoInformes(new byte[4]);
		clienteSalvar.setDescripcion(nombreCliente);
		clienteSalvar.setCodigoPostal(codigoPostal);
		clienteSalvar.setCol(col);
		clienteSalvar.setComisionClientes(comisionClientes);
		clienteSalvar.setCostosRutas(costosRutas);
		clienteSalvar.setCorreoContacto(correoContacto);
		clienteSalvar.setCuentaBancaria(cuentaBancaria);
		clienteSalvar.setDestinatarioFactura(destinatarioFactura);
		clienteSalvar
				.setDomicilioLineaTransportistas(domicilioLineaTransportistas);
		clienteSalvar.setDomicionEntregaServicio(domicionEntregaServicio);
		clienteSalvar.setDomicionOrigenServicio(domicionOrigenServicio);
		clienteSalvar.setDomocilioFiscal(domocilioFiscal);
		clienteSalvar.setEmpresa(empresa);
		clienteSalvar.setEspecificacionesServio(especificacionesServio);
		clienteSalvar.setFolioFactura(folioFactura);
		clienteSalvar.setFolioServio(folioServio);
		clienteSalvar.setActivo(true);
		if (horarioEntregaServicio != null) {
			clienteSalvar.setHorarioEntregaServicio(new Timestamp(
					horarioEntregaServicio.getTime()));
		}
		if (horarioInicio != null) {
			clienteSalvar.setHorarioInicio(new Timestamp(horarioInicio
					.getTime()));
		}
		clienteSalvar.setInstruccionesCliente(instruccionesCliente);
		clienteSalvar.setLineaTransportista(lineaTransportista);
		clienteSalvar.setMunicipioDelegacion(municipioDelegacion);
		clienteSalvar.setNombreCliente(nombreCliente);
		clienteSalvar.setNombreResponsableDirecto(nombreResponsableDirecto);
		clienteSalvar
				.setPersonaAutorizadaRecivirServicio(personaAutorizadaRecivirServicio);
		clienteSalvar.setPersonasALasQueNotificar(personasALasQueNotificar);
		clienteSalvar.setPlazoCredito(plazoCredito);
		clienteSalvar.setRasonSocial(rasonSocial);
		clienteSalvar
				.setRegistroFederalContribuyente(registroFederalContribuyente);
		clienteSalvar.setRutasFrecuente(rutasFrecuente);
		clienteSalvar.setTelefono(telefono);
		clienteSalvar.setCorreoPersonasNotificar(correoPersonasNotificar);
		clienteSalvar.setTelefonoID(telefonoID);
		Set<Factura> lisAuxFactura = new HashSet<Factura>();
		for (Factura ifactura : listaAdjuntos) {
			lisAuxFactura.add(facturaServ.saveAndFlush(ifactura));
		}
		clienteSalvar.setListaFacturas(lisAuxFactura);
		if (edNuevo) {
			String[] split = correoContacto.split("@");
			if (usuarioServ.findUserByEmpresaAndNombreCuenta(empresa, split[0]) == null) {
				String encClave = this.bcryptEncoder.encode(correoContacto);
				Contacto clienteUsuario = new Contacto();
				clienteUsuario.setActivo(true);
				clienteUsuario.setBloqueado(false);
				clienteUsuario.setContrasena(encClave);
				clienteUsuario.setNombreCuenta(correoContacto);
				clienteUsuario.setUsuarioCorreo(correoContacto);
				clienteUsuario.setContrasena(encClave);
				clienteUsuario.setNombreContacto(edNombreContacto);
				clienteUsuario.setCelular(telefono);
				clienteUsuario.setIdNextel(edIdNextel);
				clienteUsuario.setRol(rolServ.getRolClienteUsuario(empresa));
				clienteUsuario = contactoServ.saveAndFlush(clienteUsuario);
				clienteSalvar.setUsuario(clienteUsuario);
			}
		}
		// ahora inserto los contactos del cliente
		Set<Contacto> listacontAuxGuar = new HashSet<Contacto>();
		listacontAuxGuar.addAll(listContactosGuardar);
		clienteSalvar.setContactos(listacontAuxGuar);
		clienteSalvar = clienteServ.saveAndFlus(clienteSalvar);
		clienteSalvar = clienteServ.saveAndFlus(clienteSalvar);
		if (edNuevo) {
			listaClientes.add(clienteSalvar);
		}
		edNuevo = false;
		resetearCampos();
		edSelectedCliente = null;
	}

	@Command
	@NotifyChange({ "listaAdjuntos" })
	public void cmdEliminarAdjuntoDeLista() {
		listaAdjuntos.remove(edSelectedFactura);
	}

	private void resetearCampos() {
		// TODO Auto-generated method stub
		setArchivoInformes(null);
		setCodigoPostal("");
		setCol("");
		setComisionClientes("");
		setCorreoContacto("");
		setCostosRutas("");
		setCuentaBancaria("");
		setDestinatarioFactura("");
		setDomicilioLineaTransportistas("");
		setDomicionEntregaServicio("");
		setDomicionOrigenServicio("");
		setDomocilioFiscal("");
		setEspecificacionesServio("");
		setFolioFactura("");
		setFolioServio("");
		setHorarioEntregaServicio(null);
		setHorarioInicio(null);
		setInstruccionesCliente("");
		setLineaTransportista("");
		setMunicipioDelegacion("");
		setNombreCliente("");
		setNombreResponsableDirecto("");
		setPersonaAutorizadaRecivirServicio("");
		setPersonasALasQueNotificar("");
		setPlazoCredito("");
		setRasonSocial("");
		setRegistroFederalContribuyente("");
		setRutasFrecuente("");
		setTelefono("");
		setTelefonoID("");
		setCorreoPersonasNotificar("");
		listaAdjuntos.clear();
		listaContactos.clear();
		listContactosGuardar.clear();
		// listaCuentasBancarias.clear();

	}

	@Command
	@NotifyChange({ "edConfirmaCerrarEste" })
	public void cmdFacturacionClientesCerrarConfirmar() {
		setEdConfirmaCerrarEste("¿Está seguro que desea cerrar?");
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarElemento" })
	public void cmdFacturacionClientesBorrarConfirmar() {
		setEdConfirmaBorrarElemento("¿Desea borrar el '"
				+ edSelectedCliente.getDescripcion() + "'?");
	}

	@NotifyChange({ "edConfirmaBorrarElemento" })
	@Command
	public void cmdElementoBorrarCancelar() {
		edConfirmaBorrarElemento = "";
	}

	@Command
	@NotifyChange({ "listaClientes", "edConfirmaBorrarElemento",
			"edSelectedCliente", "exceptionsManager" })
	public void cmdElementoBorrar() {

//		String[] split = edSelectedCliente.getCorreoContacto().split("@");
		Usuario usuarioEliminar = usuarioServ.findUserByEmpresaAndNombreCuenta(
				empresa, edSelectedCliente.getCorreoContacto());
		Contacto contactoEliminar = contactoServ.findByNombreCuenta(edSelectedCliente.getCorreoContacto());
		try {
			if (usuarioEliminar != null) {
				usuarioServ.delete(usuarioEliminar);
			} else {
				
				edSelectedCliente.setUsuario(null);
				edSelectedCliente.getContactos().clear();
				edSelectedCliente = clienteServ.saveAndFlus(edSelectedCliente);
				contactoServ.delete(contactoEliminar);
			}
			clienteServ.delete(edSelectedCliente);
		} catch (ValorNotFoundException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
		}
		listaClientes.remove(edSelectedCliente);
		edConfirmaBorrarElemento = "";
		edSelectedCliente = null;
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarEste" })
	public void cmdFacturacionClientesCerrarCancelar() {
		setEdConfirmaCerrarCancelar("");
	}

	@NotifyChange({ "edNuevo", "rasonSocial", "nombreResponsableDirecto",
			"domocilioFiscal", "col", "municipioDelegacion", "telefono",
			"codigoPostal", "registroFederalContribuyente", "cuentaBancaria",
			"nombreCliente", "correoContacto", "telefonoID",
			"destinatarioFactura", "domicionOrigenServicio",
			"domicionEntregaServicio", "lineaTransportista", "rutasFrecuente",
			"floatcostosRutas", "floatcomisionClientes", "intplazoCredito",
			"domicilioLineaTransportistas", "horarioInicio",
			"horarioEntregaServicio", "personaAutorizadaRecivirServicio",
			"personasALasQueNotificar", "especificacionesServio",
			"intfolioServio", "intfolioFactura", "instruccionesCliente",
			"edSelectedCliente", "listaAdjuntos", "listContactosGuardar",
			"listaCuentasBancarias", "listContactosGuardar" })
	public void setEdSelectedCliente(Cliente edSelectedCliente) {
		this.edSelectedCliente = edSelectedCliente;
		setCodigoPostal("");
		codigoPostal = this.edSelectedCliente.getCodigoPostal();
		col = this.edSelectedCliente.getCol();
		comisionClientes = this.edSelectedCliente.getComisionClientes();
		correoContacto = this.edSelectedCliente.getCorreoContacto();
		costosRutas = this.edSelectedCliente.getCostosRutas();
		cuentaBancaria = this.edSelectedCliente.getCuentaBancaria();
		destinatarioFactura = this.edSelectedCliente.getDestinatarioFactura();
		domicilioLineaTransportistas = this.edSelectedCliente
				.getDomicilioLineaTransportistas();
		domicionEntregaServicio = this.edSelectedCliente
				.getDomicionEntregaServicio();
		domicionOrigenServicio = this.edSelectedCliente
				.getDomicionOrigenServicio();
		domocilioFiscal = this.edSelectedCliente.getDomocilioFiscal();
		especificacionesServio = this.edSelectedCliente
				.getEspecificacionesServio();
		folioFactura = this.edSelectedCliente.getFolioFactura();
		folioServio = this.edSelectedCliente.getFolioServio();
		if (this.edSelectedCliente.getHorarioEntregaServicio() != null) {
			horarioEntregaServicio = new Date(this.edSelectedCliente
					.getHorarioEntregaServicio().getTime());
		}
		if (this.edSelectedCliente.getHorarioInicio() != null) {
			horarioInicio = new Date(this.edSelectedCliente.getHorarioInicio()
					.getTime());
		}
		instruccionesCliente = this.edSelectedCliente.getInstruccionesCliente();
		lineaTransportista = this.edSelectedCliente.getLineaTransportista();
		municipioDelegacion = this.edSelectedCliente.getMunicipioDelegacion();
		nombreCliente = this.edSelectedCliente.getNombreCliente();
		nombreResponsableDirecto = this.edSelectedCliente
				.getNombreResponsableDirecto();
		personaAutorizadaRecivirServicio = this.edSelectedCliente
				.getPersonaAutorizadaRecivirServicio();
		personasALasQueNotificar = this.edSelectedCliente
				.getPersonasALasQueNotificar();
		plazoCredito = this.edSelectedCliente.getPlazoCredito();
		rasonSocial = this.edSelectedCliente.getRasonSocial();
		registroFederalContribuyente = this.edSelectedCliente
				.getRegistroFederalContribuyente();
		rutasFrecuente = this.edSelectedCliente.getRutasFrecuente();
		telefono = this.edSelectedCliente.getTelefono();
		telefonoID = this.edSelectedCliente.getTelefonoID();
		listContactosGuardar.clear();
		listContactosGuardar.addAll(this.edSelectedCliente.getContactos());
		listaAdjuntos.clear();
		listaAdjuntos.addAll(this.edSelectedCliente.getListaFacturas());
		// listaCuentasBancarias.clear();
		// listaCuentasBancarias.add(this.edSelectedCliente.getCuentasBancarias());
	}

	@Command
	@NotifyChange({ "edConfigurarCorreoMostrar" })
	public void cmdConfigurarCorreoMostrar() {
		setEdConfigurarCorreoMostrar(true);

	}

	@Command
	@NotifyChange({ "edConfigurarCorreoMostrar" })
	public void cmdCancelarConfigurarCorreo() {
		setEdConfigurarCorreoMostrar(false);
	}

	public Integer getFlStatusAll() {
		return flStatusAll;
	}

	public void setFlStatusAll(Integer flStatusAll) {
		this.flStatusAll = flStatusAll;
	}

	public Integer getFlStatusActive() {
		return flStatusActive;
	}

	public void setFlStatusActive(Integer flStatusActive) {
		this.flStatusActive = flStatusActive;
	}

	public Integer getFlStatusInactive() {
		return flStatusInactive;
	}

	public void setFlStatusInactive(Integer flStatusInactive) {
		this.flStatusInactive = flStatusInactive;
	}

	public Integer getFlStatus() {
		return flStatus;
	}

	public void setFlStatus(Integer flStatus) {
		this.flStatus = flStatus;
	}

	public String getFlSearchTerm() {
		return flSearchTerm;
	}

	public void setFlSearchTerm(String flSearchTerm) {
		this.flSearchTerm = flSearchTerm;
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public ExceptionsManager getExceptionsManager() {
		return exceptionsManager;
	}

	public void setExceptionsManager(ExceptionsManager exceptionsManager) {
		this.exceptionsManager = exceptionsManager;
	}

	public boolean isEdNuevo() {
		return edNuevo;
	}

	public void setEdNuevo(boolean edNuevo) {
		this.edNuevo = edNuevo;
	}

	public ListModelList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ListModelList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Cliente getEdSelectedCliente() {
		return edSelectedCliente;
	}

	public String getEdConfirmaBorrarCliente() {
		return edConfirmaBorrarCliente;
	}

	public void setEdConfirmaBorrarCliente(String edConfirmaBorrarCliente) {
		this.edConfirmaBorrarCliente = edConfirmaBorrarCliente;
	}

	public String getTextoMail() {
		return textoMail;
	}

	public void setTextoMail(String textoMail) {
		this.textoMail = textoMail;
	}

	public boolean isMostraVentaEnviarMail() {
		return mostraVentaEnviarMail;
	}

	public void setMostraVentaEnviarMail(boolean mostraVentaEnviarMail) {
		this.mostraVentaEnviarMail = mostraVentaEnviarMail;
	}

	public String getPara() {
		return para;
	}

	public void setPara(String para) {
		this.para = para;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getRasonSocial() {
		return rasonSocial;
	}

	public void setRasonSocial(String rasonSocial) {
		this.rasonSocial = rasonSocial;
	}

	public String getNombreResponsableDirecto() {
		return nombreResponsableDirecto;
	}

	public void setNombreResponsableDirecto(String nombreResponsableDirecto) {
		this.nombreResponsableDirecto = nombreResponsableDirecto;
	}

	public String getDomocilioFiscal() {
		return domocilioFiscal;
	}

	public void setDomocilioFiscal(String domocilioFiscal) {
		this.domocilioFiscal = domocilioFiscal;
	}

	public String getCol() {
		return col;
	}

	public void setCol(String col) {
		this.col = col;
	}

	public String getMunicipioDelegacion() {
		return municipioDelegacion;
	}

	public void setMunicipioDelegacion(String municipioDelegacion) {
		this.municipioDelegacion = municipioDelegacion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getRegistroFederalContribuyente() {
		return registroFederalContribuyente;
	}

	public void setRegistroFederalContribuyente(
			String registroFederalContribuyente) {
		this.registroFederalContribuyente = registroFederalContribuyente;
	}

	public String getCuentaBancaria() {
		return cuentaBancaria;
	}

	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getCorreoContacto() {
		return correoContacto;
	}

	public void setCorreoContacto(String correoContacto) {
		this.correoContacto = correoContacto;
	}

	public String getTelefonoID() {
		return telefonoID;
	}

	public void setTelefonoID(String telefonoID) {
		this.telefonoID = telefonoID;
	}

	public String getDestinatarioFactura() {
		return destinatarioFactura;
	}

	public void setDestinatarioFactura(String destinatarioFactura) {
		this.destinatarioFactura = destinatarioFactura;
	}

	public String getDomicionOrigenServicio() {
		return domicionOrigenServicio;
	}

	public void setDomicionOrigenServicio(String domicionOrigenServicio) {
		this.domicionOrigenServicio = domicionOrigenServicio;
	}

	public String getDomicionEntregaServicio() {
		return domicionEntregaServicio;
	}

	public void setDomicionEntregaServicio(String domicionEntregaServicio) {
		this.domicionEntregaServicio = domicionEntregaServicio;
	}

	public String getLineaTransportista() {
		return lineaTransportista;
	}

	public void setLineaTransportista(String lineaTransportista) {
		this.lineaTransportista = lineaTransportista;
	}

	public String getRutasFrecuente() {
		return rutasFrecuente;
	}

	public void setRutasFrecuente(String rutasFrecuente) {
		this.rutasFrecuente = rutasFrecuente;
	}

	public String getDomicilioLineaTransportistas() {
		return domicilioLineaTransportistas;
	}

	public void setDomicilioLineaTransportistas(
			String domicilioLineaTransportistas) {
		this.domicilioLineaTransportistas = domicilioLineaTransportistas;
	}

	public Date getHorarioInicio() {
		return horarioInicio;
	}

	public void setHorarioInicio(Date horarioInicio) {
		this.horarioInicio = horarioInicio;
	}

	public Date getHorarioEntregaServicio() {
		return horarioEntregaServicio;
	}

	public void setHorarioEntregaServicio(Date horarioEntregaServicio) {
		this.horarioEntregaServicio = horarioEntregaServicio;
	}

	public String getPersonaAutorizadaRecivirServicio() {
		return personaAutorizadaRecivirServicio;
	}

	public void setPersonaAutorizadaRecivirServicio(
			String personaAutorizadaRecivirServicio) {
		this.personaAutorizadaRecivirServicio = personaAutorizadaRecivirServicio;
	}

	public String getPersonasALasQueNotificar() {
		return personasALasQueNotificar;
	}

	public void setPersonasALasQueNotificar(String personasALasQueNotificar) {
		this.personasALasQueNotificar = personasALasQueNotificar;
	}

	public String getEspecificacionesServio() {
		return especificacionesServio;
	}

	public void setEspecificacionesServio(String especificacionesServio) {
		this.especificacionesServio = especificacionesServio;
	}

	public byte[] getArchivoInformes() {
		return archivoInformes;
	}

	public void setArchivoInformes(byte[] archivoInformes) {
		this.archivoInformes = archivoInformes;
	}

	public String getInstruccionesCliente() {
		return instruccionesCliente;
	}

	public void setInstruccionesCliente(String instruccionesCliente) {
		this.instruccionesCliente = instruccionesCliente;
	}

	public void setCostosRutas(String costosRutas) {
		this.costosRutas = costosRutas;
	}

	public String getCostosRutas() {
		return costosRutas;
	}

	public void setComisionClientes(String comisionClientes) {
		this.comisionClientes = comisionClientes;
	}

	public String getComisionClientes() {
		return comisionClientes;
	}

	public String getPlazoCredito() {
		return plazoCredito;
	}

	public void setPlazoCredito(String plazoCredito) {
		this.plazoCredito = plazoCredito;
	}

	public String getFolioServio() {
		return folioServio;
	}

	public void setFolioServio(String folioServio) {
		this.folioServio = folioServio;
	}

	public String getFolioFactura() {
		return folioFactura;
	}

	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}

	public String getEdConfirmaCerrarEste() {
		return edConfirmaCerrarEste;
	}

	public void setEdConfirmaCerrarEste(String edConfirmaCerrarEste) {
		this.edConfirmaCerrarEste = edConfirmaCerrarEste;
	}

	public String getEdConfirmaBorrarElemento() {
		return edConfirmaBorrarElemento;
	}

	public void setEdConfirmaBorrarElemento(String edConfirmaBorrarElemento) {
		this.edConfirmaBorrarElemento = edConfirmaBorrarElemento;
	}

	public String getEdConfirmaCerrarCancelar() {
		return edConfirmaCerrarCancelar;
	}

	public void setEdConfirmaCerrarCancelar(String edConfirmaCerrarCancelar) {
		this.edConfirmaCerrarCancelar = edConfirmaCerrarCancelar;
	}

	public byte[] getAdjunto() {
		return adjunto;
	}

	public void setAdjunto(byte[] adjunto) {
		this.adjunto = adjunto;
	}

	public AbstractClienteValidator getAbstractClienteValidator() {
		return abstractClienteValidator;
	}

	public void setAbstractClienteValidator(
			AbstractClienteValidator abstractClienteValidator) {
		this.abstractClienteValidator = abstractClienteValidator;
	}

	public ListModelList<Factura> getListaAdjuntos() {
		return listaAdjuntos;
	}

	public void setListaAdjuntos(ListModelList<Factura> listaAdjuntos) {
		this.listaAdjuntos = listaAdjuntos;
	}

	public Factura getEdSelectedFactura() {
		return edSelectedFactura;
	}

	public void setEdSelectedFactura(Factura edSelectedFactura) {
		this.edSelectedFactura = edSelectedFactura;
	}

	public boolean isEdConfigurarCorreoMostrar() {
		return edConfigurarCorreoMostrar;
	}

	public void setEdConfigurarCorreoMostrar(boolean edConfigurarCorreoMostrar) {
		this.edConfigurarCorreoMostrar = edConfigurarCorreoMostrar;
	}

	public String getEdConfigHost() {
		return edConfigHost;
	}

	public void setEdConfigHost(String edConfigHost) {
		this.edConfigHost = edConfigHost;
	}

	public int getEdConfigPort() {
		return edConfigPort;
	}

	public void setEdConfigPort(int edConfigPort) {
		this.edConfigPort = edConfigPort;
	}

	public String getEdConfigPassword() {
		return edConfigPassword;
	}

	public void setEdConfigPassword(String edConfigPassword) {
		this.edConfigPassword = edConfigPassword;
	}

	public String getEdConfigUserName() {
		return edConfigUserName;
	}

	public void setEdConfigUserName(String edConfigUserName) {
		this.edConfigUserName = edConfigUserName;
	}

	public AbstractConfigCorreoValidator getAbstractConfigCorreoValidator() {
		return abstractConfigCorreoValidator;
	}

	public void setAbstractConfigCorreoValidator(
			AbstractConfigCorreoValidator abstractConfigCorreoValidator) {
		this.abstractConfigCorreoValidator = abstractConfigCorreoValidator;
	}

	public String getCorreoPersonasNotificar() {
		return correoPersonasNotificar;
	}

	public void setCorreoPersonasNotificar(String correoPersonasNotificar) {
		this.correoPersonasNotificar = correoPersonasNotificar;
	}

	public Contacto getEdSelectedContacto() {
		return edSelectedContacto;
	}

	public void setEdSelectedContacto(Contacto edSelectedContacto) {
		this.edSelectedContacto = edSelectedContacto;
	}

	public ListModelList<Contacto> getListaContactos() {
		return listaContactos;
	}

	public void setListaContactos(ListModelList<Contacto> listaContactos) {
		this.listaContactos = listaContactos;
	}

	public boolean isMostrarBuscarContactos() {
		return mostrarBuscarContactos;
	}

	public void setMostrarBuscarContactos(boolean mostrarBuscarContactos) {
		this.mostrarBuscarContactos = mostrarBuscarContactos;
	}

	public String getEdUsuarioCorreo() {
		return edUsuarioCorreo;
	}

	public void setEdUsuarioCorreo(String edUsuarioCorreo) {
		this.edUsuarioCorreo = edUsuarioCorreo;
	}

	public String getEdCelular() {
		return edCelular;
	}

	public void setEdCelular(String edCelular) {
		this.edCelular = edCelular;
	}

	public String getEdTelefono() {
		return edTelefono;
	}

	public void setEdTelefono(String edTelefono) {
		this.edTelefono = edTelefono;
	}

	public String getEdIdNextel() {
		return edIdNextel;
	}

	public void setEdIdNextel(String edIdNextel) {
		this.edIdNextel = edIdNextel;
	}

	public String getEdNombreContacto() {
		return edNombreContacto;
	}

	public void setEdNombreContacto(String edNombreContacto) {
		this.edNombreContacto = edNombreContacto;
	}

	public ListModelList<Contacto> getListContactosGuardar() {
		return listContactosGuardar;
	}

	public void setListContactosGuardar(
			ListModelList<Contacto> listContactosGuardar) {
		this.listContactosGuardar = listContactosGuardar;
	}

	public boolean isMostrarAgregarContactos() {
		return mostrarAgregarContactos;
	}

	public void setMostrarAgregarContactos(boolean mostrarAgregarContactos) {
		this.mostrarAgregarContactos = mostrarAgregarContactos;
	}

	public String getEdNombreContactoTerminoBusqueda() {
		return edNombreContactoTerminoBusqueda;
	}

	public void setEdNombreContactoTerminoBusqueda(
			String edNombreContactoTerminoBusqueda) {
		this.edNombreContactoTerminoBusqueda = edNombreContactoTerminoBusqueda;
	}

	public boolean isEdNuevoContacto() {
		return edNuevoContacto;
	}

	public void setEdNuevoContacto(boolean edNuevoContacto) {
		this.edNuevoContacto = edNuevoContacto;
	}

	public AbstractContactosValidator getAbstractContactosValidator() {
		return abstractContactosValidator;
	}

	public void setAbstractContactosValidator(
			AbstractContactosValidator abstractContactosValidator) {
		this.abstractContactosValidator = abstractContactosValidator;
	}

	public boolean isEdNuevaCuentaBancaria() {
		return edNuevaCuentaBancaria;
	}

	public void setEdNuevaCuentaBancaria(boolean edNuevaCuentaBancaria) {
		this.edNuevaCuentaBancaria = edNuevaCuentaBancaria;
	}

	// public ListModelList<CuentaBancaria> getListaCuentasBancarias() {
	// return listaCuentasBancarias;
	// }
	//
	// public void setListaCuentasBancarias(
	// ListModelList<CuentaBancaria> listaCuentasBancarias) {
	// this.listaCuentasBancarias = listaCuentasBancarias;
	// }
	//
	// public CuentaBancaria getEdSelectedCuentaBancaria() {
	// return edSelectedCuentaBancaria;
	// }
	//
	// @NotifyChange({ "edSelectedCuentaBancaria", "cuentaBancaria" })
	// public void setEdSelectedCuentaBancaria(
	// CuentaBancaria edSelectedCuentaBancaria) {
	// this.edSelectedCuentaBancaria = edSelectedCuentaBancaria;
	// cuentaBancaria = this.edSelectedCuentaBancaria
	// .getNumeroCuentaBancaria();
	// }

	public ListModelList<String> getListaStringCuentas() {
		return listaStringCuentas;
	}

	public void setListaStringCuentas(ListModelList<String> listaStringCuentas) {
		this.listaStringCuentas = listaStringCuentas;
	}

	public String getEdSelectedStringCuenta() {
		return edSelectedStringCuenta;
	}

	public void setEdSelectedStringCuenta(String edSelectedStringCuenta) {
		this.edSelectedStringCuenta = edSelectedStringCuenta;
	}

}
