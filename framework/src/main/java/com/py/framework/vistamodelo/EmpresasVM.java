package com.py.framework.vistamodelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Sucursal;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.SucursalServ;
import com.py.framework.validadores.AbstractEmpresaValidator;
import com.py.framework.validadores.DescripcionEmpresaValidator;
import com.py.framework.validadores.SucursalValidator;

public class EmpresasVM {

	@WireVariable
	private EmpresaServ empresaServ;

	@WireVariable
	private SucursalServ sucursalServ;

	@WireVariable
	private DescripcionEmpresaValidator descripcionEmpresaValidator;

	@WireVariable
	private AbstractEmpresaValidator abstractEmpresaValidator;

	@WireVariable
	private SucursalValidator sucursalValidator;

	@WireVariable
	private ExceptionsManager exceptionsManager;
	/**
	 * Permisos
	 */
	private Usuario principal;
	private Empresa empresa;

	/**
	 * Listado de empresas
	 */
	private Empresa selectedEmpresa = null;
	private ListModelList<Empresa> listEmpresa = null;

	/**
	 * Filter fields
	 */
	private Integer flStatusAll = new Integer(-1);
	private Integer flStatusActive = new Integer(1);
	private Integer flStatusInactive = new Integer(0);
	private Integer flStatus = flStatusAll;
	private String flSearchTerm = "";
	/**
	 * Edit fields
	 */
	private List<Object> edModified = new ArrayList<Object>();
	private boolean edNuevo = false;
	private boolean edActiva = false;
	private String edDescription = "";
	private String edRuc = "";
	private String edCarpeta = "";
	private String edEmpresaLabel = "";
	private String edEmpresasLabel = "";
	private String edSucursalLabel = "";
	private String edSucursalesLabel = "";
	private String edClienteLabel = "";
	private String edClientesLabel = "";
	private ListModelList<Sucursal> edSucursales = new ListModelList<Sucursal>();
	private String edConfirmaBorrarEmpresa = "";
	private String edConfirmaCerrarEmpresa = "";
	private boolean principalIsGlobal = false;
	private boolean edTieneSucursales = false;

	/**
	 * Sucursal fields
	 */
	private Sucursal selectedSucursal = null;
	private boolean edSucursalNueva = false;
	private String sucursalDescripcion = "";
	private boolean sucursalActiva = false;

	@Init
	public void init() {
		principal = (Usuario) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		empresa = principal.getEmpresa();
		listEmpresa = new ListModelList<Empresa>();
		principalIsGlobal = principal.isGlobal();
	}

	public boolean hasRole(String role) {
		Collection<? extends GrantedAuthority> l = principal.getAuthorities();
		for (GrantedAuthority i : l) {
			if (i.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	@Command
	@NotifyChange({ "selectedEmpresa", "listEmpresa",
			"edNoDesmarcarSucursales", "sucursalPuedeBorrar",
			"exceptionsManager" })
	public void cmdEmpresaNueva() {
		if (edNuevo == false) {
			cmdEmpresaEditar();
			return;
		}
		selectedEmpresa = new Empresa();
		selectedEmpresa.setDescripcion(edDescription);
		selectedEmpresa.setRuc(edRuc);
		selectedEmpresa.setCarpeta(edCarpeta);
//		selectedEmpresa.setEmpresaLabel(edEmpresaLabel);
//		selectedEmpresa.setEmpresasLabel(edEmpresasLabel);
//		selectedEmpresa.setSucursalesLabel(edSucursalesLabel);
//		selectedEmpresa.setSucursalLabel(edSucursalLabel);
//		selectedEmpresa.setClienteLabel(edClienteLabel);
//		selectedEmpresa.setClientesLabel(edClientesLabel);
		selectedEmpresa.setActiva(edActiva);
		selectedEmpresa.setTieneSucursales(edTieneSucursales);
		Empresa saved;
		try {
			saved = empresaServ.saveAndFlush(selectedEmpresa);
		} catch (ValorNotFoundException e1) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e1);
			return;
		}
		if (!edSucursales.isEmpty()) {
			for (Sucursal item : edSucursales) {
				item.setEmpresa(saved);
				try {
					sucursalServ.saveAndFlush(item);
				} catch (ValorNotFoundException e) {
					// TODO Auto-generated catch block
					exceptionsManager.reportarExcepcion(e);
					return;
				}
			}
		}
		if (!edTieneSucursales) {
			Sucursal nueva = new Sucursal();
			nueva.setActiva(true);
			nueva.setDescripcion(saved.getDescripcion());
			nueva.setEmpresa(saved);
			try {
				sucursalServ.saveAndFlush(nueva);
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				exceptionsManager.reportarExcepcion(e);
				return;
			}
		}
		listEmpresa.add(saved);
		selectedEmpresa = null;
		resetEdit();
	}

	@Command
	@NotifyChange({ "selectedEmpresa", "listEmpresa",
			"edNoDesmarcarSucursales", "exceptionsManager" })
	public void cmdEmpresaEditar() {
		if (edDescription.isEmpty())
			return;
		// hago la logica para si cambia el nombre de la empresa c el nombre de
		// la sucur
		if (!edDescription.equals(selectedEmpresa.getDescripcion())) {
			selectedEmpresa.setDescripcion(edDescription);
			Sucursal cambiarSucursal = sucursalServ
					.findByDescripcionAndEmpresa(
							selectedEmpresa.getDescripcion(), selectedEmpresa);
			cambiarSucursal.setDescripcion(edDescription);
		} else {
			// tengo que poner la sucursal que esta a la empresa
			for (Sucursal sucu : edSucursales) {
				sucu.setEmpresa(selectedEmpresa);
				try {
					sucursalServ.saveAndFlush(sucu);
				} catch (ValorNotFoundException e) {
					// TODO Auto-generated catch block
					exceptionsManager.reportarExcepcion(e);
					return;
				}
			}
		}
		selectedEmpresa.setRuc(edRuc);
		selectedEmpresa.setCarpeta(edCarpeta);
		selectedEmpresa.setActiva(edActiva);
		selectedEmpresa.setTieneSucursales(edTieneSucursales);
		int index = listEmpresa.indexOf(selectedEmpresa);
		Empresa saved = null;
		try {
			saved = empresaServ.saveAndFlush(selectedEmpresa);
		} catch (ValorNotFoundException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		if (index >= 0) {
			listEmpresa.set(index, saved);
		} else {
			listEmpresa.add(saved);
		}
		selectedEmpresa = null;
		resetEdit();
	}

	@Command
	@NotifyChange({ "selectedSucursal", "edSucursalNueva", "edSucursales",
			"edNoDesmarcarSucursales", "sucursalPuedeBorrar",
			"exceptionsManager" })
	public void cmdSucursalNueva() {
		if (edSucursalNueva) {
			Sucursal suc = new Sucursal(sucursalDescripcion, sucursalActiva);
			suc.setEmpresa(selectedEmpresa);
			try {
				suc = sucursalServ.saveAndFlush(suc);
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				exceptionsManager.reportarExcepcion(e);
				return;
			}
			edSucursalNueva = false;
			edSucursales.add(suc);
		} else {
			cmdSucursalEditar();
		}
	}

	@Command
	@NotifyChange({ "selectedSucursal", "edNuevaSucursal",
			"edNoDesmarcarSucursales", "sucursalPuedeBorrar" })
	public void cmdSucursalEditar() {
		selectedSucursal.setActiva(sucursalActiva);
		selectedSucursal.setDescripcion(sucursalDescripcion);
		int i = edSucursales.indexOf(selectedSucursal);
		Sucursal s = null;
		try {
			s = sucursalServ.saveAndFlush(selectedSucursal);
		} catch (ValorNotFoundException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		edSucursales.set(i, s);
		selectedSucursal = null;
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarEmpresa" })
	public void cmdEmpresaCerrarConfirmar() {
		edConfirmaCerrarEmpresa = "Hay cambios que no se han guardado. ¿Desea cerrar sin guardar?";
	}

	@Command
	@NotifyChange({ "edConfirmaCerrarEmpresa" })
	public void cmdEmpresaCerrarCancelar() {
		edConfirmaCerrarEmpresa = "";
	}

	@Command
	@NotifyChange({ "selectedEmpresa", "edConfirmaCerrarEmpresa" })
	public void cmdEmpresaCerrar() {
		edConfirmaCerrarEmpresa = "";
		resetEdit();
		selectedEmpresa = null;
	}

	@Command
	@NotifyChange("listEmpresa")
	public void cmdDoSearch() {
		listEmpresa.clear();
		if (principal.isGlobal()) {
			listEmpresa = new ListModelList<Empresa>(empresaServ.searchEmpresa(
					flSearchTerm, flStatus));
		} else {
			listEmpresa.add(empresa);
		}
	}

	@Command
	@NotifyChange("listEmpresa")
	public void cmdDoSearchParams(@BindingParam("item") Integer flStatus1) {
		listEmpresa.clear();
		if (principal.isGlobal()) {
			listEmpresa = new ListModelList<Empresa>(empresaServ.searchEmpresa(
					flSearchTerm, flStatus1));
		} else {
			if (flStatus1 != 0) {
				listEmpresa.add(empresa);
			}
		}
	}

	@Command
	@NotifyChange({ "selectedEmpresa", "edNuevo", "edDirty",
			"sucursalDescripcion", "edEmpresaLabel", "edEmpresasLabel",
			"edSucursalLabel", "edSucursalesLabel", "edClienteLabel",
			"edClientesLabel" })
	public void cmdEmpresaNuevaMostrar() {
		resetEdit();
		edNuevo = true;
	}

	@Command
	@NotifyChange({ "sucursalActiva", "edSucursalNueva", "sucursalDescripcion",
			"edEmpresaLabel", "edEmpresasLabel", "edSucursalLabel",
			"edSucursalesLabel", "edClienteLabel", "edClientesLabel",
			"edNoDesmarcarSucursales" })
	public void cmdSucursalNuevaMostrar() {
		edSucursalNueva = true;
		sucursalActiva = false;
		sucursalDescripcion = "";
		edEmpresaLabel = "";
		edEmpresasLabel = "";
		edSucursalLabel = "";
		edSucursalesLabel = "";
		edClienteLabel = "";
		edClientesLabel = "";

	}

	@Command
	@NotifyChange({ "selectedSucursal", "edSucursalNueva" })
	public void cmdSucursalCancelar() {
		edSucursalNueva = false;
		selectedSucursal = null;
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarEmpresa" })
	public void cmdEmpresaBorrarConfirmar() {
		edConfirmaBorrarEmpresa = "¿Desea borrar la empresa '"
				+ selectedEmpresa.getDescripcion() + "'?";
	}

	@Command
	@NotifyChange({ "edConfirmaBorrarEmpresa" })
	public void cmdEmpresaBorrarCancelar() {
		edConfirmaBorrarEmpresa = "";
	}

	@Command
	@NotifyChange({ "selectedEmpresa", "listEmpresa",
			"edConfirmaBorrarEmpresa", "edNoDesmarcarSucursales",
			"exceptionsManager" })
	public void cmdEmpresaBorrar() {
		listEmpresa.remove(selectedEmpresa);
		try {
			empresaServ.delete(selectedEmpresa);
		} catch (ValorNotFoundException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		resetEdit();
		edConfirmaBorrarEmpresa = "";
		selectedEmpresa = null;
	}

	@Command
	@NotifyChange({ "selectedSucursal", "edSucursales",
			"edNoDesmarcarSucursales", "sucursalPuedeBorrar",
			"exceptionsManager" })
	public void cmdSucursalBorrar() {
		edSucursales.remove(selectedSucursal);
		try {
			sucursalServ.delete(selectedSucursal);
		} catch (ValorNotFoundException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		selectedSucursal = null;
	}

	public void resetEdit() {
		edNuevo = false;
		edDescription = "";
		edRuc = "";
		edCarpeta = "";
		edSucursalesLabel = "";
		edSucursalLabel = "";
		edClienteLabel = "";
		edClientesLabel = "";
		edEmpresaLabel = "";
		edEmpresasLabel = "";
		edActiva = false;
		edTieneSucursales = false;
		edSucursales.clear();
		edModified.clear();

	}

	@NotifyChange("exceptionsManager")
	@Command
	public void cmdCerrarVentanaException() {
		exceptionsManager.setEdMostraExcepcion(false);
		exceptionsManager.setEdTextoMensaje("");
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Empresa getSelectedEmpresa() {
		return selectedEmpresa;
	}

	@NotifyChange({ "selectedEmpresa", "edEmpresaLabel", "edEmpresasLabel",
			"edSucursalesLabel", "edSucursalLabel", "edClienteLabel",
			"edClientesLabel", "edTieneSucursales", "edNoDesmarcarSucursales",
			"sucursalPuedeBorrar" })
	public void setSelectedEmpresa(Empresa selectedEmpresa) {
		this.selectedEmpresa = selectedEmpresa;
		edSucursales.clear();
		edSucursales.addAll(sucursalServ.findByEmpresa(selectedEmpresa));
		edNuevo = false;
		edDescription = selectedEmpresa.getDescripcion();
		edRuc = selectedEmpresa.getRuc();
		edCarpeta = selectedEmpresa.getCarpeta();
		edActiva = selectedEmpresa.isActiva();
		edTieneSucursales = selectedEmpresa.isTieneSucursales();
	}

	public ListModelList<Empresa> getListEmpresa() {
		return listEmpresa;
	}

	public Integer getFlStatusAll() {
		return flStatusAll;
	}

	public void setFlStatusAll(Integer flStatusAll) {
		this.flStatusAll = flStatusAll;
	}

	public Integer getFlStatusActive() {
		return flStatusActive;
	}

	public void setFlStatusActive(Integer flStatusActive) {
		this.flStatusActive = flStatusActive;
	}

	public Integer getFlStatusInactive() {
		return flStatusInactive;
	}

	public void setFlStatusInactive(Integer flStatusInactive) {
		this.flStatusInactive = flStatusInactive;
	}

	public Integer getFlStatus() {
		return flStatus;
	}

	public void setFlStatus(Integer flStatus) {
		this.flStatus = flStatus;
	}

	public String getFlSearchTerm() {
		return flSearchTerm;
	}

	public void setFlSearchTerm(String flSearchTerm) {
		this.flSearchTerm = flSearchTerm;
	}

	@DependsOn({ "edDescription", "edRuc", "edCarpeta", "edActiva",
			"edSucursalesLabel", "edSucursalLabel", "edClienteLabel",
			"edClientesLabel", "edEmpresaLabel", "edEmpresasLabel" })
	public boolean getEdDirty() {
		return edModified.size() != 0;
	}

	public List<Object> getEdModified() {
		return edModified;
	}

	public void setEdModified(Object key, boolean modified) {
		if (modified) {
			if (edModified.contains(key))
				edModified.remove(key);
		} else {
			if (!edModified.contains(key))
				edModified.add(key);
		}
	}

	@DependsOn({ "selectedEmpresa" })
	public boolean getEdNuevo() {
		return edNuevo;
	}

	public void setEdNuevo(boolean edNuevo) {
		this.edNuevo = edNuevo;
	}

	@DependsOn({ "edConfirmaBorrarEmpresa" })
	public String getEdConfirmaBorrarEmpresa() {
		return edConfirmaBorrarEmpresa;
	}

	public void setEdConfirmaBorrarEmpresa(String edConfirmaBorrarEmpresa) {
		this.edConfirmaBorrarEmpresa = edConfirmaBorrarEmpresa;
	}

	@DependsOn({ "edConfirmaCerrarEmpresa" })
	public String getEdConfirmaCerrarEmpresa() {
		return edConfirmaCerrarEmpresa;
	}

	public void setEdConfirmaCerrarEmpresa(String edConfirmaCerrarEmpresa) {
		this.edConfirmaCerrarEmpresa = edConfirmaCerrarEmpresa;
	}

	@DependsOn({ "selectedEmpresa" })
	public boolean getEdActiva() {
		return edActiva;
	}

	public void setEdActiva(boolean edActiva) {
		this.edActiva = edActiva;
		setEdModified("edActiva", edNuevo ? edActiva == false
				: this.edActiva == selectedEmpresa.isActiva());
	}

	@DependsOn({ "selectedEmpresa" })
	public String getEdDescription() {
		return edDescription;
	}

	public void setEdDescription(String edDescription) {
		this.edDescription = edDescription;
		setEdModified("edDescription", edNuevo ? this.edDescription.equals("")
				: this.edDescription.equals(selectedEmpresa.getDescripcion()));
	}

	@DependsOn({ "selectedEmpresa" })
	public String getEdRuc() {
		return edRuc;
	}

	public void setEdRuc(String edRuc) {
		this.edRuc = edRuc;
		setEdModified(
				"edRuc",
				edNuevo ? this.edRuc.equals("") : this.edRuc
						.equals(selectedEmpresa.getRuc()));
	}

	@DependsOn({ "selectedEmpresa" })
	public String getEdCarpeta() {
		return edCarpeta;
	}

	public void setEdCarpeta(String edCarpeta) {
		this.edCarpeta = edCarpeta;
		setEdModified("edCarpeta", edNuevo ? this.edCarpeta.equals("")
				: this.edCarpeta.equals(selectedEmpresa.getCarpeta()));
	}

	public String getEdEmpresaLabel() {
		return edEmpresaLabel;
	}

//	public void setEdEmpresaLabel(String edEmpresaLabel) {
//		this.edEmpresaLabel = edEmpresaLabel;
//
//		setEdModified(
//				"edEmpresaLabel",
//				edNuevo ? this.edEmpresaLabel.equals("") : this.edEmpresaLabel
//						.equals(selectedEmpresa.getEmpresaLabel()));
//	}

	public String getEdEmpresasLabel() {
		return edEmpresasLabel;
	}

//	public void setEdEmpresasLabel(String edEmpresasLabel) {
//		this.edEmpresasLabel = edEmpresasLabel;
//		setEdModified(
//				"edEmpresasLabel",
//				edNuevo ? this.edEmpresasLabel.equals("")
//						: this.edEmpresasLabel.equals(selectedEmpresa
//								.getEmpresasLabel()));
//	}

	public String getEdSucursalLabel() {
		return edSucursalLabel;
	}

//	public void setEdSucursalLabel(String edSucursalLabel) {
//		this.edSucursalLabel = edSucursalLabel;
//		setEdModified(
//				"edSucursalLabel",
//				edNuevo ? this.edSucursalLabel.equals("")
//						: this.edSucursalLabel.equals(selectedEmpresa
//								.getSucursalLabel()));
//	}

	public String getEdSucursalesLabel() {
		return edSucursalesLabel;
	}

//	public void setEdSucursalesLabel(String edSucursalesLabel) {
//		this.edSucursalesLabel = edSucursalesLabel;
//		setEdModified(
//				"edSucursalesLabel",
//				edNuevo ? this.edSucursalesLabel.equals("")
//						: this.edSucursalesLabel.equals(selectedEmpresa
//								.getSucursalesLabel()));
//	}

	public String getEdClienteLabel() {
		return edClienteLabel;
	}

//	public void setEdClienteLabel(String edClienteLabel) {
//		this.edClienteLabel = edClienteLabel;
//		setEdModified(
//				"edClienteLabel",
//				edNuevo ? this.edClienteLabel.equals("") : this.edClienteLabel
//						.equals(selectedEmpresa.getClienteLabel()));
//	}

	public String getEdClientesLabel() {
		return edClientesLabel;
	}
//
//	public void setEdClientesLabel(String edClientesLabel) {
//		this.edClientesLabel = edClientesLabel;
//		setEdModified(
//				"edClientesLabel",
//				edNuevo ? this.edClientesLabel.equals("")
//						: this.edClientesLabel.equals(selectedEmpresa
//								.getClientesLabel()));
//	}

	@DependsOn("selectedEmpresa")
	public ListModelList<Sucursal> getEdSucursales() {
		return edSucursales;
	}

	public void setEdSucursales(ListModelList<Sucursal> edSucursales) {
		this.edSucursales = edSucursales;
	}

	public boolean isEdSucursalNueva() {
		return edSucursalNueva;
	}

	public void setEdSucursalNueva(boolean edSucursalNueva) {
		this.edSucursalNueva = edSucursalNueva;
	}

	public String getSucursalDescripcion() {
		return sucursalDescripcion;
	}

	public void setSucursalDescripcion(String sucursalDescripcion) {
		this.sucursalDescripcion = sucursalDescripcion;
	}

	public boolean isSucursalActiva() {
		return sucursalActiva;
	}

	public void setSucursalActiva(boolean sucursalActiva) {
		this.sucursalActiva = sucursalActiva;
	}

	public DescripcionEmpresaValidator getDescripcionValidator() {
		return descripcionEmpresaValidator;
	}

	public void setDescripcionValidator(
			DescripcionEmpresaValidator descripcionValidator) {
		this.descripcionEmpresaValidator = descripcionValidator;
	}

	public AbstractEmpresaValidator getAbstractEmpresaValidator() {
		return abstractEmpresaValidator;
	}

	public void setAbstractEmpresaValidator(
			AbstractEmpresaValidator abstractEmpresaValidator) {
		this.abstractEmpresaValidator = abstractEmpresaValidator;
	}

	public Sucursal getSelectedSucursal() {
		return selectedSucursal;
	}

	@NotifyChange({ "sucursalActiva", "sucursalDescripcion", "selectedSucursal" })
	public void setSelectedSucursal(Sucursal selectedSucursal) {
		int index = edSucursales.indexOf(selectedSucursal);
		selectedSucursal = sucursalServ
				.expandeUsuarios(selectedSucursal);
		edSucursales.set(index, selectedSucursal);
		this.selectedSucursal = selectedSucursal;
		this.sucursalActiva = selectedSucursal.isActiva();
		this.sucursalDescripcion = selectedSucursal.getDescripcion();
	}

	public SucursalValidator getSucursalValidator() {
		return sucursalValidator;
	}

	public void setSucursalValidator(SucursalValidator sucursalValidator) {
		this.sucursalValidator = sucursalValidator;
	}

	public boolean isPrincipalIsGlobal() {
		return principalIsGlobal;
	}

	public void setPrincipalIsGlobal(boolean isGlobal) {
		this.principalIsGlobal = isGlobal;
	}

	public boolean isEdTieneSucursales() {
		return edTieneSucursales;
	}

	public void setEdTieneSucursales(boolean edTieneSucursales) {
		this.edTieneSucursales = edTieneSucursales;
	}

	public ExceptionsManager getExceptionsManager() {
		return exceptionsManager;
	}

	public void setExceptionsManager(ExceptionsManager exceptionsManager) {
		this.exceptionsManager = exceptionsManager;
	}
}
