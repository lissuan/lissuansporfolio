package com.py.framework.vistamodelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.TreeNode;

import com.py.framework.entidades.Carpeta;
import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Funcionalidad;
import com.py.framework.entidades.Rol;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.servicios.domain.CarpetaServ;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.FuncionalidadServ;
import com.py.framework.servicios.domain.RolServ;
import com.py.framework.servicios.domain.UsuarioServ;
import com.py.framework.validadores.AbstractRolesValidator;

public class RolesVM {

	@WireVariable
	private EmpresaServ empresaServ;

	@WireVariable
	RolServ rolServ;

	@WireVariable
	CarpetaServ carpetaServ;

	@WireVariable
	FuncionalidadServ funcionalidadServ;

	@WireVariable
	private AbstractRolesValidator abstractRolesValidator;

	@WireVariable
	private UsuarioServ usuarioServ;

	@WireVariable
	private ExceptionsManager exceptionsManager;

	private Usuario principal;
	private Empresa empresa;

	private Empresa selectedEmpresa;
	private ListModelList<Empresa> listEmpresa = new ListModelList<Empresa>();

	private Rol selectedRol;
	private ListModelList<Rol> listAllRoles = new ListModelList<Rol>();

	public class FolderTreeNode extends DefaultTreeNode<Carpeta> {
		private static final long serialVersionUID = -7912497593037329651L;
		private List<Funcionalidad> assigmentListFunctionalities;
		private Integer assigmentsTotal = null;

		public FolderTreeNode(Carpeta data) {
			super(data);
		}

		public FolderTreeNode(Carpeta key, FolderTreeNode[] folderTreeNodes) {
			super(key, folderTreeNodes);
		}

		public List<Funcionalidad> getAssigmentListFunctionalities() {
			return assigmentListFunctionalities;
		}

		public void setAssigmentListFunctionalities(
				List<Funcionalidad> assigmentListFunctionalities) {
			this.assigmentListFunctionalities = assigmentListFunctionalities;
		}

		protected int getAssigmentsTotal() {
			if (!(assigmentsTotal == null))
				return assigmentsTotal;
			int result = this.getData().getFuncionalidades().size();
			if (!this.isLeaf()) {
				for (TreeNode<Carpeta> node : this.getChildren()) {
					result += ((FolderTreeNode) node).getAssigmentsTotal();
				}
			}
			assigmentsTotal = Integer.valueOf(result);
			return result;
		}

		protected int getAssigments() {
			int result = 0;
			for (Funcionalidad f : this.getData().getFuncionalidades()) {
				result += assigmentListFunctionalities.contains(f) ? 1 : 0;
			}
			if (!this.isLeaf()) {
				for (TreeNode<Carpeta> node : this.getChildren()) {
					result += ((FolderTreeNode) node).getAssigments();
				}
			}
			return result;
		}

		public int getAssignmentLevel() {
			int a = this.getAssigments();
			int b = this.getAssigmentsTotal();
			return (b == 0 ? 0 : a == b ? 2 : a == 0 ? 0 : 1);
		}
	}

	private List<Carpeta> listAllFolders;
	private Map<Carpeta, FolderTreeNode> folder2node = new HashMap<Carpeta, FolderTreeNode>();
	private DefaultTreeModel<Carpeta> treeFolder;
	private FolderTreeNode selectedFolderTreeNode = null;

	public class FunctionalityItem {
		private Funcionalidad model;
		private List<Funcionalidad> assigmentList;
		private boolean originalAssigned;
		private Object beanContainer;

		public FunctionalityItem(Funcionalidad model,
				List<Funcionalidad> assigmentList, Object beanContainer) {
			this.model = model;
			this.assigmentList = assigmentList;
			this.originalAssigned = assigmentList.contains(model);
			this.beanContainer = beanContainer;
		}

		public Funcionalidad getModel() {
			return model;
		}

		public boolean getAssigned() {
			return assigmentList.contains(model);
		}

		public void setAssigned(boolean assigned) {
			if (assigned) {
				if (!assigmentList.contains(model))
					assigmentList.add(model);
			} else {
				if (assigmentList.contains(model))
					assigmentList.remove(model);
			}
			BindUtils.postNotifyChange(null, null, beanContainer, "treeFolder");
			BindUtils.postNotifyChange(null, null, beanContainer, "edDirty");
			// BindUtils.postNotifyChange(null,null,beanContainer,"edIsPartiallyAssigned");
		}

		public boolean isModified() {
			return getAssigned() != originalAssigned;
		}
	}

	private List<Funcionalidad> listOrigFunctionalities;
	private List<Funcionalidad> listEditFunctionalities;
	private ListModelList<FunctionalityItem> listFunctionalitiesItems = new ListModelList<RolesVM.FunctionalityItem>();

	private boolean newRolFlag;
	private boolean editRolFlag;
	private boolean deleteRolFlag;

	private String edRolDescripcion;
	private boolean edRolActivo;
	private boolean mostrartVentaNoBorrarRol = false;

	@Init
	public void Init() {
		principal = (Usuario) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		empresa = principal.getEmpresa();
		listEmpresa.addAll(empresaServ.findByActiva(true));
		selectedEmpresa = empresa;
		listAllRoles.addAll(rolServ
				.findByEmpresaOrderByDescripcion(selectedEmpresa));
		listAllFolders = carpetaServ.findAll();
		listEditFunctionalities = new ArrayList<Funcionalidad>();
		listOrigFunctionalities = new ArrayList<Funcionalidad>();

		FolderTreeNode root = new FolderTreeNode(null, new FolderTreeNode[] {});
		folder2node.put(null, root);
		for (Carpeta key : listAllFolders) {
			FolderTreeNode value;
			if (key.getHijos().size() > 0) {
				value = new FolderTreeNode(key, new FolderTreeNode[] {});
			} else {
				value = new FolderTreeNode(key);
			}
			value.setAssigmentListFunctionalities(listEditFunctionalities);
			folder2node.put(key, value);
		}
		for (Carpeta key : listAllFolders) {
			folder2node.get(key.getPadre()).add(folder2node.get(key));
		}
		treeFolder = new DefaultTreeModel<Carpeta>(root);

		setSelectedFolderTreeNode((FolderTreeNode) root.getChildAt(0));
	}

	public boolean hasRole(String role) {
		Collection<? extends GrantedAuthority> l = getPrincipal()
				.getAuthorities();
		for (GrantedAuthority i : l) {
			if (i.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}

	@Command
	@NotifyChange({ "selectedRol", "listFunctionalitiesItems", "treeFolder" })
	public void cmdRolCargar() {
		listAllRoles.clear();
		listAllRoles.addAll(rolServ
				.findByEmpresaOrderByDescripcion(selectedEmpresa));
		selectedRol = null;
		listEditFunctionalities.clear();
		listOrigFunctionalities.clear();
	}

	@Command
	@NotifyChange({ "newRolFlag", "listAllRoles", "listFunctionalitiesItems",
			"exceptionsManager" })
	public void cmdRolNuevo() {
		Rol entity = new Rol(edRolDescripcion, edRolActivo, selectedEmpresa);
		try {
			entity = rolServ.saveAndFlush(entity);
		} catch (ValorNotFoundException e) {
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		listAllRoles.add(entity);
		newRolFlag = false;
	}

	@Command
	@NotifyChange({ "deleteRolFlag", "listAllRoles",
			"listFunctionalitiesItems", "selectedRol",
			"mostrartVentaNoBorrarRol", "exceptionsManager" })
	public void cmdRolBorrar() {
		Rol item = selectedRol;

		List<Funcionalidad> funcionalidades = funcionalidadServ.findByRol(item);
		List<Usuario> usuarios = usuarioServ.findByRolAndEmpresa(item,
				item.getEmpresa());
		if ((funcionalidades == null || funcionalidades.isEmpty())
				&& (usuarios == null || usuarios.isEmpty())) {
			// validar si el rol no tiene incidencias
			try {
				rolServ.delete(item);
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				exceptionsManager.reportarExcepcion(e);
				return;
			}
			listAllRoles.remove(item);
			deleteRolFlag = false;
			selectedRol = null;
		} else {
			mostrartVentaNoBorrarRol = true;
			deleteRolFlag = false;
		}
	}

	@Command
	@NotifyChange({ "mostrartVentaNoBorrarRol" })
	public void cmdNoMostrarVentanaAvisoEliminar() {
		mostrartVentaNoBorrarRol = false;
	}

	@Command
	@NotifyChange({ "newRolFlag", "edRolActivo", "edRolDescripcion" })
	public void cmdRolNuevoMostrar() {
		newRolFlag = true;
		edRolActivo = false;
		edRolDescripcion = "";
	}

	@Command
	@NotifyChange({ "deleteRolFlag" })
	public void cmdRolBorrarMostrar() {
		deleteRolFlag = true;
	}

	@Command
	@NotifyChange({ "newRolFlag" })
	public void cmdRolNuevoCancelar() {
		newRolFlag = false;
	}

	@Command
	@NotifyChange({ "deleteRolFlag" })
	public void cmdRolBorrarCancelar() {
		deleteRolFlag = false;
	}

	@Command
	@NotifyChange({ "editRolFlag", "listAllRoles", "listFunctionalitiesItems",
			"exceptionsManager" })
	public void cmdRolEditar() {
		selectedRol.setDescripcion(edRolDescripcion);
		selectedRol.setActivo(edRolActivo);
		try {
			rolServ.saveAndFlush(selectedRol);
		} catch (ValorNotFoundException e) {
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		editRolFlag = false;
	}

	@Command
	@NotifyChange({ "editRolFlag", "edRolActivo", "edRolDescripcion" })
	public void cmdRolEditarMostrar() {
		edRolActivo = selectedRol.isActivo();
		edRolDescripcion = selectedRol.getDescripcion();
		editRolFlag = true;
	}

	@Command
	@NotifyChange({ "editRolFlag" })
	public void cmdRolEditarCancelar() {
		editRolFlag = false;
	}

	@Command
	@NotifyChange({ "edDirty", "listAllRoles", "treeFolder",
			"listFunctionalitiesItems", "exceptionsManager" })
	public void cmdFuncionalidadesAsignarMostrar() {
		selectedRol.setFuncionalidades(new HashSet<Funcionalidad>(
				listEditFunctionalities));
		try {
			rolServ.saveAndFlush(selectedRol);
		} catch (ValorNotFoundException e) {
			// TODO Auto-generated catch block
			exceptionsManager.reportarExcepcion(e);
			return;
		}
		listOrigFunctionalities.clear();
		listOrigFunctionalities.addAll(listEditFunctionalities);
	}

	@NotifyChange("exceptionsManager")
	@Command
	public void cmdCerrarVentanaException() {
		exceptionsManager.setEdMostraExcepcion(false);
		exceptionsManager.setEdTextoMensaje("");
	}

	public boolean getEdDirty() {
		if (selectedRol == null) {
			return false;
		}
		if (listEditFunctionalities.size() != listOrigFunctionalities.size()) {
			return true;
		}
		for (Funcionalidad f : listEditFunctionalities) {
			if (!listOrigFunctionalities.contains(f)) {
				return true;
			}
		}
		return false;
	}

	public List<FunctionalityItem> digestFunctionalities2Items(
			Set<Funcionalidad> lf) {
		List<FunctionalityItem> li = new ArrayList<RolesVM.FunctionalityItem>();
		for (Funcionalidad f : lf) {
			li.add(new FunctionalityItem(f, listEditFunctionalities, this));
		}
		return li;
	}

	public AbstractRolesValidator getAbstractRolesValidator() {
		return abstractRolesValidator;
	}

	public void setAbstractRolesValidator(
			AbstractRolesValidator abstractRolesValidator) {
		this.abstractRolesValidator = abstractRolesValidator;
	}

	public CarpetaServ getFolderServ() {
		return carpetaServ;
	}

	public void setFolderServ(CarpetaServ folderServ) {
		this.carpetaServ = folderServ;
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	public Rol getSelectedRol() {
		return selectedRol;
	}

	@NotifyChange({ "selectedRol", "listFunctionalitiesItems", "treeFolder" })
	public void setSelectedRol(Rol selectedRol) {
		this.selectedRol = selectedRol;
		Set<Funcionalidad> f = rolServ.expandFuncionalidades(selectedRol)
				.getFuncionalidades();
		listEditFunctionalities.clear();
		listEditFunctionalities.addAll(f);
		listOrigFunctionalities.clear();
		listOrigFunctionalities.addAll(f);
		listFunctionalitiesItems.clear();
		listFunctionalitiesItems
				.addAll(digestFunctionalities2Items(selectedFolderTreeNode
						.getData().getFuncionalidades()));
	}

	public ListModelList<Rol> getListAllRoles() {
		return listAllRoles;
	}

	public void setListAllRoles(ListModelList<Rol> listAllRoles) {
		this.listAllRoles = listAllRoles;
	}

	public List<Carpeta> getListAllFolders() {
		return listAllFolders;
	}

	public void setListAllFolders(List<Carpeta> listAllFolders) {
		this.listAllFolders = listAllFolders;
	}

	public Map<Carpeta, FolderTreeNode> getFolder2node() {
		return folder2node;
	}

	public void setFolder2node(Map<Carpeta, FolderTreeNode> folder2node) {
		this.folder2node = folder2node;
	}

	public DefaultTreeModel<Carpeta> getTreeFolder() {
		return treeFolder;
	}

	public void setTreeFolder(DefaultTreeModel<Carpeta> treeFolder) {
		this.treeFolder = treeFolder;
	}

	public FolderTreeNode getSelectedFolderTreeNode() {
		return selectedFolderTreeNode;
	}

	@NotifyChange({ "selectedFolderTreeNode", "listFunctionalitiesItems" })
	public void setSelectedFolderTreeNode(FolderTreeNode selectedFolderTreeNode) {
		this.selectedFolderTreeNode = selectedFolderTreeNode;
		listFunctionalitiesItems.clear();
		listFunctionalitiesItems
				.addAll(digestFunctionalities2Items(selectedFolderTreeNode
						.getData().getFuncionalidades()));
	}

	public List<Funcionalidad> getListOrigFunctionalities() {
		return listOrigFunctionalities;
	}

	public void setListOrigFunctionalities(
			List<Funcionalidad> listOrigFunctionalities) {
		this.listOrigFunctionalities = listOrigFunctionalities;
	}

	public List<Funcionalidad> getListEditFunctionalities() {
		return listEditFunctionalities;
	}

	public void setListEditFunctionalities(
			List<Funcionalidad> listEditFunctionalities) {
		this.listEditFunctionalities = listEditFunctionalities;
	}

	public ListModelList<FunctionalityItem> getListFunctionalitiesItems() {
		return listFunctionalitiesItems;
	}

	public void setListFunctionalitiesItems(
			ListModelList<FunctionalityItem> listFunctionalitiesItems) {
		this.listFunctionalitiesItems = listFunctionalitiesItems;
	}

	public boolean isNewRolFlag() {
		return newRolFlag;
	}

	public void setNewRolFlag(boolean newRolFlag) {
		this.newRolFlag = newRolFlag;
	}

	public boolean isEditRolFlag() {
		return editRolFlag;
	}

	public void setEditRolFlag(boolean editRolFlag) {
		this.editRolFlag = editRolFlag;
	}

	public boolean isDeleteRolFlag() {
		return deleteRolFlag;
	}

	public void setDeleteRolFlag(boolean deleteRolFlag) {
		this.deleteRolFlag = deleteRolFlag;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Empresa getSelectedEmpresa() {
		return selectedEmpresa;
	}

	public void setSelectedEmpresa(Empresa selectedEmpresa) {
		this.selectedEmpresa = selectedEmpresa;
	}

	public ListModelList<Empresa> getListEmpresa() {
		return listEmpresa;
	}

	public void setListEmpresa(ListModelList<Empresa> listEmpresa) {
		this.listEmpresa = listEmpresa;
	}

	public String getEdRolDescripcion() {
		return edRolDescripcion;
	}

	public void setEdRolDescripcion(String edRolDescripcion) {
		this.edRolDescripcion = edRolDescripcion;
	}

	public boolean isEdRolActivo() {
		return edRolActivo;
	}

	public void setEdRolActivo(boolean edRolActivo) {
		this.edRolActivo = edRolActivo;
	}

	public boolean isMostrartVentaNoBorrarRol() {
		return mostrartVentaNoBorrarRol;
	}

	public void setMostrartVentaNoBorrarRol(boolean mostrartVentaNoBorrarRol) {
		this.mostrartVentaNoBorrarRol = mostrartVentaNoBorrarRol;
	}

	public ExceptionsManager getExceptionsManager() {
		return exceptionsManager;
	}

	public void setExceptionsManager(ExceptionsManager exceptionsManager) {
		this.exceptionsManager = exceptionsManager;
	}
}
