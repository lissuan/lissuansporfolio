package com.py.framework.vistamodelo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelList;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Correo;
import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Factura;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ExceptionsManager;
import com.py.framework.servicios.domain.ClienteServ;
import com.py.framework.servicios.domain.ContactoServ;
import com.py.framework.servicios.domain.CorreoServ;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.EnviarEmailServ;
import com.py.framework.servicios.domain.FacturaServ;

public class historialMensajesVM {

	@WireVariable
	private CorreoServ correoServ;
	@WireVariable
	private EnviarEmailServ enviarEMailServ;
	@WireVariable
	private ContactoServ contactoServ;
	@WireVariable
	private FacturaServ facturaServ;
	@WireVariable
	private ClienteServ clienteServ;
	@WireVariable
	private ExceptionsManager exceptionsManager;
	private Contacto principalContacto;
	@WireVariable
	private EmpresaServ empresaServ;

	private String flSearchTerm = "";

	/**
	 * Permisos
	 */
	private Usuario principal;
	private Empresa empresa;

	private ListModelList<Correo> listaCorreos = new ListModelList<Correo>();
	private Correo edSelectedCorreo = null;
	private Cliente edSelectedCliente = null;
	private ListModelList<Factura> listaAdjuntos = new ListModelList<Factura>();
	private Factura edSelectedFactura = null;
	// private String edConfirmaCerrarCliente = "";
	private String edConfirmaBorrarCliente = "";
	private boolean edNuevo;
	private boolean mostraVentaEnviarMail;
	private String textoMail = "";
	private String para = "";
	private String cc = "";
	private String asunto = "";
	private String remitente = "";
	private byte[] adjunto = null;
	private byte[] archivoInformes;

	@org.zkoss.bind.annotation.Init
	public void Init() {
		Object objetoPrincipal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		listaCorreos.clear();
		if (objetoPrincipal instanceof Usuario) {
			principal = (Usuario) objetoPrincipal;
			empresa = principal.getEmpresa();
			edSelectedCliente = clienteServ.finByEmpresaAndUsuario(empresa,
					principal.getContacto());

		} else if (objetoPrincipal instanceof Contacto) {
			principalContacto = (Contacto) objetoPrincipal;
			empresa = empresaServ.findById(1L);
			edSelectedCliente = clienteServ
					.finByEmpresaAndUsuario(principalContacto);
		}
		listaCorreos.addAll(correoServ.findByCliente(edSelectedCliente));
	}

	@Command
	public boolean hasRole(String role) {
		if (principal != null) {
			Collection<? extends GrantedAuthority> l = principal
					.getAuthorities();

			for (GrantedAuthority i : l) {
				if (i.getAuthority().equals(role)) {
					return true;
				}
			}
		} else {
			Collection<? extends GrantedAuthority> l = principalContacto
					.getAuthorities();

			for (GrantedAuthority i : l) {
				if (i.getAuthority().equals(role)) {
					return true;
				}
			}

		}

		return false;
	}

	@Command
	@NotifyChange({ "listaCorreos" })
	public void cmdDoSearch() {
		listaCorreos.clear();
		listaCorreos.addAll(correoServ.findByClienteAndRemitenteContaining(
				edSelectedCliente, flSearchTerm));
	}

	@Command
	@NotifyChange({ "exceptionsManager" })
	public void cmdDescargarElemento() {
		File auxGuardar = enviarEMailServ.devolverPDF(
				edSelectedFactura.getArchivoPdf(),
				edSelectedFactura.getDescripcion());
		try {
			Filedownload.save(auxGuardar, null);
		} catch (FileNotFoundException e) {
			exceptionsManager.reportarExcepcion(e);
		}
	}

	@Command
	@NotifyChange({})
	public void onUploadFactura(
			@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx)
			throws Exception {
		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		String dir = Executions.getCurrent().getDesktop().getWebApp()
				.getRealPath("/");
		Media media = upEvent.getMedia();
		// String format = media.getFormat();
		dir = dir + media.getName();
		File file = new File(dir);
		file.createNewFile();
		if (media.isBinary()) {
			archivoInformes = media.getByteData();
		} else {
			String stringData = media.getStringData();
			if (stringData != null) {
				// String[] split = stringData.split("\n");
				Writer writer = new FileWriter(dir);
				BufferedWriter bwriter = new BufferedWriter(writer);
				bwriter.append(stringData);
				bwriter.close();
			}
		}
		if (archivoInformes != null) {
			Factura factura = new Factura();
			factura.setDescripcion(media.getName());
			factura.setArchivoPdf(archivoInformes);
			listaAdjuntos.add(factura);
		}
	}

	@Command
	@NotifyChange({ "listaAdjuntos" })
	public void cmdEliminarAdjuntoDeLista() {
		listaAdjuntos.remove(edSelectedFactura);
	}

	// @Command
	// @NotifyChange({ "mostraVentaEnviarMail" })
	// public void cmdEnviarMail() {
	// String[] contactos = para.split(";");
	// Set<Contacto> listaCliente = new HashSet<Cliente>();
	// for (int i = 0; i < contactos.length; i++) {
	// Cliente clienteAdicionar = new Cliente();
	// clienteAdicionar = clienteServ.findByCorreoContacto(contactos[i]);
	// if (clienteAdicionar != null) {
	// listaCliente.add(clienteAdicionar);
	// }
	// }
	// Correo correoAEnviar = new Correo();
	// correoAEnviar.setCliente(listaCliente);
	// correoAEnviar.setCopiaA(cc);
	// Set<Factura> listaFacturas = new HashSet<Factura>();
	// for (Factura iFactura : listaAdjuntos) {
	// iFactura = facturaServ.saveAndFlush(iFactura);
	// listaFacturas.add(iFactura);
	// }
	// correoAEnviar.setFacturas(listaFacturas);
	// correoAEnviar.setRemitente(principal.getUsername());
	// correoAEnviar.setTextoCorreo(textoMail);
	// correoAEnviar.setRemitido(para);
	// correoAEnviar.setAsunto(asunto);
	// correoAEnviar = correoServ.saveAndFlush(correoAEnviar);
	// enviarEMailServ.sendMail(correoAEnviar, contactos);
	// mostraVentaEnviarMail = false;
	// }

	@Command
	@NotifyChange({ "edSelectedCorreo" })
	public void cmdClienteCerrarConfirmar() {
		edSelectedCorreo = null;
	}

	@Command
	@NotifyChange({ "mostraVentaEnviarMail", "edSelectedCorreo" })
	public void cmdMostrarEnviarMail() {
		mostraVentaEnviarMail = true;
		edSelectedCorreo.setLeido(true);
	}

	@Command
	@NotifyChange({ "mostraVentaEnviarMail" })
	public void cmdCancelarEnviarMail() {
		mostraVentaEnviarMail = false;
	}

	public String getFlSearchTerm() {
		return flSearchTerm;
	}

	public void setFlSearchTerm(String flSearchTerm) {
		this.flSearchTerm = flSearchTerm;
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Correo getEdSelectedCorreo() {
		return edSelectedCorreo;
	}

	@NotifyChange({ "edSelectedCorreo", "asunto", "cc", "para", "textoMail",
			"listaAdjuntos", "remitente" })
	public void setEdSelectedCorreo(Correo edSelectedCorreo) {
		this.edSelectedCorreo = edSelectedCorreo;
		asunto = edSelectedCorreo.getAsunto();
		cc = edSelectedCorreo.getCopiaA();
		para = edSelectedCorreo.getRemitido();
		textoMail = edSelectedCorreo.getTextoCorreo();
		remitente = edSelectedCorreo.getRemitente();
		this.edSelectedCorreo.setLeido(true);
		correoServ.saveAndFlush(edSelectedCorreo);
		listaAdjuntos.clear();
		listaAdjuntos.addAll(edSelectedCorreo.getFacturas());
	}

	public Cliente getEdSelectedCliente() {
		return edSelectedCliente;
	}

	public void setEdSelectedCliente(Cliente edSelectedCliente) {
		this.edSelectedCliente = edSelectedCliente;
	}

	public ListModelList<Correo> getListaCorreos() {
		return listaCorreos;
	}

	public void setListaCorreos(ListModelList<Correo> listaCorreos) {
		this.listaCorreos = listaCorreos;
	}

	public ListModelList<Factura> getListaAdjuntos() {
		return listaAdjuntos;
	}

	public void setListaAdjuntos(ListModelList<Factura> listaAdjuntos) {
		this.listaAdjuntos = listaAdjuntos;
	}

	public Factura getEdSelectedFactura() {
		return edSelectedFactura;
	}

	public void setEdSelectedFactura(Factura edSelectedFactura) {
		this.edSelectedFactura = edSelectedFactura;
	}

	public String getEdConfirmaBorrarCliente() {
		return edConfirmaBorrarCliente;
	}

	public void setEdConfirmaBorrarCliente(String edConfirmaBorrarCliente) {
		this.edConfirmaBorrarCliente = edConfirmaBorrarCliente;
	}

	public boolean isEdNuevo() {
		return edNuevo;
	}

	public void setEdNuevo(boolean edNuevo) {
		this.edNuevo = edNuevo;
	}

	public boolean isMostraVentaEnviarMail() {
		return mostraVentaEnviarMail;
	}

	public void setMostraVentaEnviarMail(boolean mostraVentaEnviarMail) {
		this.mostraVentaEnviarMail = mostraVentaEnviarMail;
	}

	public String getTextoMail() {
		return textoMail;
	}

	public void setTextoMail(String textoMail) {
		this.textoMail = textoMail;
	}

	public String getPara() {
		return para;
	}

	public void setPara(String para) {
		this.para = para;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public byte[] getAdjunto() {
		return adjunto;
	}

	public ExceptionsManager getExceptionsManager() {
		return exceptionsManager;
	}

	public void setExceptionsManager(ExceptionsManager exceptionsManager) {
		this.exceptionsManager = exceptionsManager;
	}

	public void setAdjunto(byte[] adjunto) {
		this.adjunto = adjunto;
	}

	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
}
