package com.py.framework.vistamodelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.ComposerExt;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Window;

import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.SuperUsersDetails;
import com.py.framework.entidades.Usuario;
import com.py.framework.excepciones.ValorNotFoundException;
import com.py.framework.servicios.domain.ContactoServ;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.UsuarioServ;
import com.py.framework.servicios.security.Autorizacion;
import com.py.framework.validadores.AbstractCambiarValidator;

public class IndexSC extends SelectorComposer<Component> implements
		ComposerExt<Component> {
	private static final long serialVersionUID = 1L;

	public class GrupoSeccionesVM {
		private String tabLabel;
		private String permissionCheckName;
		private List<SeccionesVM> secciones = new ArrayList<IndexSC.SeccionesVM>();

		GrupoSeccionesVM(String tabLabel, String permissionCheckName,
				List<SeccionesVM> secciones) {
			this.tabLabel = tabLabel;
			this.permissionCheckName = permissionCheckName;
			this.secciones.addAll(secciones);
		}

		public boolean isAuthorized() {
			return hasRole(this.permissionCheckName);
		}

		public String getTabLabel() {
			return tabLabel;
		}

		public void setTabLabel(String tabLabel) {
			this.tabLabel = tabLabel;
		}

		public List<SeccionesVM> getSecciones() {
			return secciones;
		}

		public void setSecciones(List<SeccionesVM> secciones) {
			this.secciones = secciones;
		}

		public String getPermissionCheckName() {
			return permissionCheckName;
		}

		public void setPermissionCheckName(String permissionCheckName) {
			this.permissionCheckName = permissionCheckName;
		}

	}

	public class SeccionesVM {
		private String tabLabel;
		private String panelFile;
		private boolean closable = false;
		private String permissionCheckName;
		private boolean visible = false;
		private String imageFilename;
		private Tab tab = null;
		private Tabpanel tabpanel = null;

		SeccionesVM(String tabLabel, String panelFile, boolean closable,
				String permissionCheckName, boolean visible,
				String imageFilename) {
			this.tabLabel = tabLabel;
			this.panelFile = panelFile;
			this.closable = closable && hasRole(permissionCheckName);
			this.permissionCheckName = permissionCheckName;
			this.visible = visible;
			this.imageFilename = imageFilename;
		}

		public boolean isAuthorized() {
			return hasRole(this.permissionCheckName);
		}

		public String getTabLabel() {
			return tabLabel;
		}

		public void setTabLabel(String tabLabel) {
			this.tabLabel = tabLabel;
		}

		public String getPanelFile() {
			return panelFile;
		}

		public void setPanelFile(String panelFile) {
			this.panelFile = panelFile;
		}

		public boolean isClosable() {
			return closable;
		}

		public void setClosable(boolean closable) {
			this.closable = closable;
		}

		public String getPermissionCheckName() {
			return permissionCheckName;
		}

		public void setPermissionCheckName(String permissionCheckName) {
			this.permissionCheckName = permissionCheckName;
		}

		public boolean isVisible() {
			return visible;
		}

		public void setVisible(boolean visible) {
			this.visible = visible;
		}

		public String getImageFilename() {
			return imageFilename;
		}

		public void setImageFilename(String imageFilename) {
			this.imageFilename = imageFilename;
		}

		public Tab getTab() {
			return tab;
		}

		public void setTab(Tab tab) {
			this.tab = tab;
		}

		public Tabpanel getTabpanel() {
			return tabpanel;
		}

		public void setTabpanel(Tabpanel tabpanel) {
			this.tabpanel = tabpanel;
		}

	}

	private Usuario principal;
	private SuperUsersDetails principalAux;
	private Contacto principalContacto;
	@WireVariable
	private EmpresaServ empresaServ;
	@WireVariable
	private BCryptPasswordEncoder bcryptEncoder;
	@WireVariable
	private ContactoServ contactoServ;
	@WireVariable
	private UsuarioServ usuarioServ;

	private Empresa empresa;
	private String usuarioMostrar = "";

	private ListModelList<GrupoSeccionesVM> grupos = new ListModelList<GrupoSeccionesVM>();

	@Wire("#idMainTabBox")
	private Tabbox mainTabBox;
	@Wire("#idMainTree")
	private Tree mainTree;

	private boolean mostrarVentanaCambiar = false;

	public boolean hasRole(String role) {
		if (principal != null) {
			Collection<? extends GrantedAuthority> l = principal
					.getAuthorities();

			for (GrantedAuthority i : l) {
				if (i.getAuthority().equals(role)) {
					return true;
				}
			}
		} else {
			Collection<? extends GrantedAuthority> l = principalContacto
					.getAuthorities();

			for (GrantedAuthority i : l) {
				if (i.getAuthority().equals(role)) {
					return true;
				}
			}

		}

		return false;
	}

	@Override
	public void doBeforeComposeChildren(Component comp) throws Exception {
		Object objetoPrincipal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (objetoPrincipal instanceof Usuario) {
			principal = (Usuario) objetoPrincipal;
			empresa = principal.getEmpresa();
			usuarioMostrar = principal.getNombreCuenta();
		} else if (objetoPrincipal instanceof Contacto) {
			principalContacto = (Contacto) objetoPrincipal;
			usuarioMostrar = principalContacto.getNombreCuenta();
			empresa = empresaServ.findById(1L);
		}

		List<SeccionesVM> secciones = new ArrayList<IndexSC.SeccionesVM>();

		if (hasRole(Autorizacion.adminAdministracion)) {
			secciones.clear();
			secciones.add(new SeccionesVM("Roles", "roles.zul", true,
					Autorizacion.adminRoles, false,
					"/images/icons/group_key.png"));
			secciones
					.add(new SeccionesVM("Usuarios", "usuarios.zul", true,
							Autorizacion.adminUsuarios, false,
							"/images/icons/user.png"));
			grupos.add(new GrupoSeccionesVM("Administración",
					Autorizacion.adminAdministracion, secciones));
		}
		if (hasRole(Autorizacion.usuario)) {
			secciones.clear();
			secciones.add(new SeccionesVM("Historial mensajes",
					"historialMensajes.zul", true, Autorizacion.usuario, false,
					"/images/icons/email.png"));
			grupos.add(new GrupoSeccionesVM("Buzon", Autorizacion.usuario,
					secciones));
		}
		if (hasRole(Autorizacion.adminSmsSender)) {
			secciones.clear();
			secciones.add(new SeccionesVM("Enviar Sms", "envioSms.zul", true,
					Autorizacion.adminSmsSender, false,
					"/images/icons/email.png"));
			grupos.add(new GrupoSeccionesVM("Sms", Autorizacion.adminSmsSender,
					secciones));
		}
		super.doBeforeComposeChildren(comp);
	}

	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}

	@Listen("onClick=#miLogOut")
	public void logOut() {
		Executions.getCurrent().sendRedirect("/j_spring_security_logout");
	}

	@Listen("onClick=#miLogOut1")
	public void logOut1() {
		if (principal != null) {
			if (principal.getContacto() != null) {
				celular = principal.getContacto().getCelular();
				telefono = principal.getContacto().getTelefono();
				nextel = principal.getContacto().getIdNextel();
			}
		}
		if (principalContacto != null) {
			celular = principalContacto.getCelular();
			telefono = principalContacto.getTelefono();
			nextel = principalContacto.getIdNextel();
		}
		ventana.setVisible(true);
	}

	private String celular;
	private String telefono;
	private String nextel;
	private String password1;
	private String password2;
	private boolean visibleVentana = false;
	@WireVariable
	private AbstractCambiarValidator abstractCambiarValidator;

	@Wire("#ventana")
	private Window ventana;

	@Init
	public void init() {
		Object objetoPrincipal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (objetoPrincipal instanceof Usuario) {
			principal = (Usuario) objetoPrincipal;
			empresa = principal.getEmpresa();
		} else if (objetoPrincipal instanceof Contacto) {
			principalContacto = (Contacto) objetoPrincipal;
			empresa = empresaServ.findById(1L);
		}
		if (principal != null) {
			if (principal.getContacto() != null) {
				celular = principal.getContacto().getCelular();
				telefono = principal.getContacto().getTelefono();
				nextel = principal.getContacto().getIdNextel();
			}
		}
		if (principalContacto != null) {
			celular = principalContacto.getCelular();
			telefono = principalContacto.getTelefono();
			nextel = principalContacto.getIdNextel();
		}
	}

	@NotifyChange({ "visibleVentana" })
	@Command
	public void cmdAgregarContacto() {
		Contacto contactoSalvar = null;
		if (principal != null) {
			contactoSalvar = principal.getContacto();
		} else {
			contactoSalvar = principalContacto;
		}
		String encClave = null;
		if (password1 != null && password2 != null && !password1.isEmpty()
				&& !password2.isEmpty() && password1.equals(password2)) {
			encClave = this.bcryptEncoder.encode(password1);
			contactoSalvar.setContrasena(encClave);
		}

		contactoSalvar.setCelular(celular);
		contactoSalvar.setIdNextel(nextel);
		contactoSalvar.setTelefono(telefono);
		contactoSalvar = contactoServ.saveAndFlush(contactoSalvar);
		if (principal != null) {
			try {
				if (password1 != null && password2 != null
						&& !password1.isEmpty() && !password2.isEmpty()
						&& password1.equals(password2)) {
					principal.setContrasena(encClave);
				}
				principal.setContacto(contactoSalvar);
				principal = usuarioServ.saveAndFlush(principal);
			} catch (ValorNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			principalContacto = contactoSalvar;
		}
		visibleVentana = false;
	}

	@NotifyChange({ "visibleVentana" })
	@Command
	public void cmdCancelarAgregarContacto() {
		visibleVentana = false;
	}

	@Listen("onShowSeccion=#buttonBox")
	public void doShowSeccion(ForwardEvent event) {
		SeccionesVM sec = (SeccionesVM) event.getData();
		if (sec.getTab() == null) {
			Tab t = new Tab(sec.getTabLabel(), sec.getImageFilename());
			t.setClosable(sec.isClosable());
			Tabpanel tp = new Tabpanel();
			Include i = new Include(sec.getPanelFile());
			tp.appendChild(i);
			sec.setTab(t);
			sec.setTabpanel(tp);
		}
		if (sec.getTab().getParent() == null) {
			mainTabBox.getTabs().appendChild(sec.getTab());
			Tabpanel tp = new Tabpanel();
			Include i = new Include(sec.getPanelFile());
			tp.appendChild(i);
			mainTabBox.getTabpanels().appendChild(tp);
			sec.setTabpanel(tp);
		}
		sec.setVisible(true);
		mainTabBox.setSelectedTab(sec.getTab());
		mainTabBox.setSelectedPanel(sec.getTabpanel());
	}

	@Command
	@NotifyChange("")
	public void cmdCambiarDatosContactoMostrar() {
	}

	@Command
	public void cmdCambiarDatosContacto() {

	}

	public void hideSeccion(SeccionesVM sec) {
		sec.setVisible(false);
	}

	public Usuario getPrincipal() {
		return principal;
	}

	public void setPrincipal(Usuario principal) {
		this.principal = principal;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public ListModelList<GrupoSeccionesVM> getGrupos() {
		return grupos;
	}

	public void setGrupos(ListModelList<GrupoSeccionesVM> grupos) {
		this.grupos = grupos;
	}

	public SuperUsersDetails getPrincipalAux() {
		return principalAux;
	}

	public void setPrincipalAux(SuperUsersDetails principalAux) {
		this.principalAux = principalAux;
	}

	public Contacto getPrincipalContacto() {
		return principalContacto;
	}

	public void setPrincipalContacto(Contacto principalContacto) {
		this.principalContacto = principalContacto;
	}

	public boolean isMostrarVentanaCambiar() {
		return mostrarVentanaCambiar;
	}

	public void setMostrarVentanaCambiar(boolean mostrarVentanaCambiar) {
		this.mostrarVentanaCambiar = mostrarVentanaCambiar;
	}

	public boolean isVisibleVentana() {
		return visibleVentana;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNextel() {
		return nextel;
	}

	public void setNextel(String nextel) {
		this.nextel = nextel;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public void setVisibleVentana(boolean visibleVentana) {
		this.visibleVentana = visibleVentana;
	}

	public AbstractCambiarValidator getAbstractCambiarValidator() {
		return abstractCambiarValidator;
	}

	public void setAbstractCambiarValidator(
			AbstractCambiarValidator abstractCambiarValidator) {
		this.abstractCambiarValidator = abstractCambiarValidator;
	}

	public String getUsuarioMostrar() {
		return usuarioMostrar;
	}

	public void setUsuarioMostrar(String usuarioMostrar) {
		this.usuarioMostrar = usuarioMostrar;
	}

}
