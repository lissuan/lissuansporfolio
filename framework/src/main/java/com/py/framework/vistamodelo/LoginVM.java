package com.py.framework.vistamodelo;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.py.framework.entidades.Empresa;
import com.py.framework.servicios.domain.EmpresaServ;
import com.py.framework.servicios.domain.InstallInitDataServ;
import com.py.framework.servicios.domain.UsuarioServ;

public class LoginVM {
	
	@WireVariable
	private UsuarioServ usuarioServ;

	@WireVariable
	private EmpresaServ empresaServ;

	@WireVariable
	private InstallInitDataServ installInitDataServ;

	private boolean visibleInstallInitData = false;

	private Empresa empresaSelected = null;

	private List<Empresa> empresasList = new ArrayList<Empresa>(0);

	@Init
	public void init() {
		if (usuarioServ.findAll().size() < 1) {
			visibleInstallInitData = true;
			return;
		}
		empresaSelected = empresaServ.findById(1L);
	}

	@Command
	@NotifyChange({ "visibleInstallInitData", "empresasList", "empresaSelected" })
	public void installInitData() {
		try {
			installInitDataServ.Execute();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		visibleInstallInitData = false;
		empresasList.addAll(empresaServ.findByActiva(true));
		if (empresasList.size() > 0)
			empresaSelected = empresasList.get(0);
	}

	public UsuarioServ getUserServ() {
		return usuarioServ;
	}

	public void setUserServ(UsuarioServ userServ) {
		this.usuarioServ = userServ;
	}

	public boolean isVisibleInstallInitData() {
		return visibleInstallInitData;
	}

	public void setVisibleInstallInitData(boolean visibleInstallInitData) {
		this.visibleInstallInitData = visibleInstallInitData;
	}

	public Empresa getEmpresaSelected() {
		return empresaSelected;
	}

	public void setEmpresaSelected(Empresa empresaSelected) {
		this.empresaSelected = empresaSelected;
	}

	public List<Empresa> getEmpresasList() {
		return empresasList;
	}

	public void setEmpresasList(List<Empresa> empresasList) {
		this.empresasList = empresasList;
	}
}
