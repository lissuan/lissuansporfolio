package com.py.framework.repositorios;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Mensaje;

public interface MensajeRepository extends JpaRepository<Mensaje, Long> {

	public List<Mensaje> findByEmpresaAndFechaEntradaBetweenOrderByFechaEnviadoAsc(
			Empresa empresa, Timestamp fechaPrimera, Timestamp fechaSegunda);

	public List<Mensaje> findByEmpresa(Empresa empresa);
}
