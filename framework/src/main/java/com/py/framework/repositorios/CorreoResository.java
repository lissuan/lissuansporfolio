package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Correo;

public interface CorreoResository extends JpaRepository<Correo, Long> {

	List<Correo> findByCliente(Cliente cliente);

	List<Correo> findByClienteAndRemitenteContaining(Cliente edSelectedCliente,
			String flSearchTerm);

}
