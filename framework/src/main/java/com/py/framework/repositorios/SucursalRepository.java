package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Sucursal;

public interface SucursalRepository extends JpaRepository<Sucursal, Long> {
	public List<Sucursal> findByActivaAndUsuarios_Id(boolean activa, long id);
	
	public List<Sucursal> findByEmpresa(Empresa empresa);

	public Sucursal findByDescripcion(String descripcion);

	public Sucursal findByDescripcionAndEmpresa(String descripcion,
			Empresa empresa);

	public List<Sucursal> findByActivaAndEmpresaAndUsuarios_Id(boolean activa,
			Empresa empresa, Long id);

	public List<Sucursal> findByEmpresaAndActiva(Empresa selectedEmpresa,boolean activa);

}
