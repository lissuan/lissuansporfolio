package com.py.framework.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Factura;

public interface FacturaRepository extends JpaRepository<Factura, Long> {
    
}
