package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Rol;
import com.py.framework.entidades.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	@Query("SELECT u FROM Usuario u WHERE "
			+ " u.empresa = ?1 "
			+ " AND ((LOWER(u.descripcion) LIKE LOWER(?2)) OR (LOWER(u.apellidos) LIKE LOWER(?2)) OR (LOWER(u.nombreCuenta) LIKE LOWER(?2)))"
			+ " AND ((u.rol = ?3) or (?3=NULL))"
			+ " AND ((u.activo = 1 AND ?4 = 1) OR (u.activo <> 1 and ?4 = 0) OR ((?4=NULL)))")
	List<Usuario> searchUser(Empresa empresa, String searchTerm, Rol rol, Integer activo);

	Usuario findOneByEmpresaAndNombreCuenta(Empresa empresa, String nombreCuenta);
	
	Usuario findOneByEmpresa_IdAndNombreCuenta(Long id, String nombreCuenta);

	List<Usuario> findByActivoAndSucursales_IdAndRol_Descripcion(
			boolean activo, long id, String name);

	List<Usuario> findByActivoAndSucursales_Id(boolean activo, long servicio);

	List<Usuario> findByRolAndEmpresa(Rol item, Empresa empresa);
}
