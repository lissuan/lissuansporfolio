package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Funcionalidad;
import com.py.framework.entidades.Rol;

public interface FuncionalidadRepository extends
		JpaRepository<Funcionalidad, Long> {

	List<Funcionalidad>findByRoles(Rol rol);

}
