package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Empresa;
import com.py.framework.entidades.Rol;

public interface RolRepository extends JpaRepository<Rol, Long> {
	public List<Rol> findByEmpresaAndActivo(Empresa empresa, boolean activo);
	public List<Rol> findByEmpresa(Empresa empresa, Sort sort);
	public Rol findByEmpresaAndDescripcion(Empresa empresa,
			String descripcion);
}
