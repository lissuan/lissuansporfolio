package com.py.framework.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Inbox;

public interface InboxRepository extends JpaRepository<Inbox, Long> {
	
}
