package com.py.framework.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Carpeta;

public interface CarpetaRepository extends JpaRepository<Carpeta, Long> {

}
