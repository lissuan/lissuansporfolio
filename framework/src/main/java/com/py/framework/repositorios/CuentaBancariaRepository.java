package com.py.framework.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.CuentaBancaria;

public interface CuentaBancariaRepository extends
		JpaRepository<CuentaBancaria, Long> {
		
}
