package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.py.framework.entidades.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long> {
	@Query("SELECT e FROM Empresa e WHERE "
			+ "(LOWER(e.descripcion) LIKE LOWER(?1))"
			+ " AND ((e.activa = 1 AND ?2 = 1) OR (e.activa <> 1 and ?2 = 0) OR ((?2=-1)))")
	List<Empresa> searchEmpresa(String searchTerm, Integer activa);
	
//	List<Empresa> findByDescripcionContainingAndActiva(Integer activa,String searchTerm);

	List<Empresa> findByActiva(boolean activa, Sort sort);

	Empresa findByDescripcion(String descripcion);

	Empresa findByRuc(String ruc);

	Empresa findOneByActivaAndCarpeta(boolean activa, String carpeta);
	Empresa findOneByActivaAndId(boolean activa, Long id);

	List<Empresa> findByDescripcionContainingAndActiva(String descripcion,
			Boolean activa);

	Empresa findByCarpeta(String carpeta);

}
