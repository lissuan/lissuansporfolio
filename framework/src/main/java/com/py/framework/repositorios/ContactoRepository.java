package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Contacto;

public interface ContactoRepository extends JpaRepository<Contacto, Long> {

	List<Contacto> findByNombreContactoContaining(
			String edNombreContactoTerminoBusqueda);

	Contacto findByUsuarioCorreo(String edCantidadNiveles);

	Contacto findOneByNombreCuenta(String userStr);

	List<Contacto> findByDescripcionContaining(boolean b, String flSearchTerm);

	List<Contacto> findByDescripcionContaining(String flSearchTerm);

	Contacto findUserByNombreCuenta(String string);

}
