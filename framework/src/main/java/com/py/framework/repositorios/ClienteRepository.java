package com.py.framework.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.py.framework.entidades.Cliente;
import com.py.framework.entidades.Contacto;
import com.py.framework.entidades.Empresa;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	List<Cliente> findByEmpresaAndDescripcionContaining(Empresa empresa,
			boolean activo, String descripcion);

	List<Cliente> findByEmpresaAndDescripcionContaining(Empresa empresa,
			String flSearchTerm);

	Cliente findByCorreoContacto(String string);

	Cliente findByEmpresaAndUsuario(Empresa empresa, Contacto principal);

	Cliente findByUsuario(Contacto principalContacto);

}
