package com.py.leverIt.eclipse.platform.api;

public interface IAppMessage {
	public void showMessage(String message, int dialogType);
}
