package com.py.leverIt.eclipse.platform.api.viewsManager.menuModule;

import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;

import com.py.leverIt.eclipse.platform.api.constants.Apps;
import com.py.leverIt.eclipse.platform.api.viewsManager.ITreeMenuOption;

public interface IAppModuleMenu {

	public void addMenuOption(ITreeMenuOption treeMenuOption);

	public void addMenuTOMenuModule(Catalog c);

	public void removeMenuFromMenuModule(MTrimmedWindow tw, String labelMenu);

	public void habilitarMenu();

	public void deshabilitarMenu();

	public void workAreaPartsRegistry(MPartStack ps);

	public void buildTree(Apps _activeApp);
	
	public String getIdMenuPartFromPerspertive(String _activePerspective);
}
