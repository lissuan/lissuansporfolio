package com.py.leverIt.eclipse.platform.api.constants;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.py.leverIt.eclipse.platform.api.IModule;
import com.py.leverIt.eclipse.platform.api.viewsManager.IOpcionBasicMenu;
import com.py.leverIt.eclipse.platform.api.viewsManager.ITreeMenuOption;

public class Apps {

	private String _appName;
	private List<IModule> _modulesNames = new ArrayList<>();
	private List<ITreeMenuOption> _menuList = new ArrayList<>();
	private List<IOpcionBasicMenu> _basicMenuList = new ArrayList<>();
	private HashSet<String> _pluguinsNames = new HashSet<>();
	private String _perspectiveId;
	private String _appBundleSymbolicName;
	private String _appWorkAreaStack;
	private HashSet<String> _menuListBuilder = new HashSet<>();
	private HashSet<String> _submenuListBuilder = new HashSet<>();
	private HashSet<IOpcionBasicMenu> _basicMenuList2 = new HashSet<>();

	public Apps() {
		super();
	}

	public Apps(String _appName, List<IModule> _modulesNames) {
		super();
		this._appName = _appName;
		this._modulesNames = _modulesNames;
	}

	public String get_appName() {
		return _appName;
	}

	public void set_appName(String _appName) {
		this._appName = _appName;
	}

	public List<IModule> get_modulesNames() {
		return _modulesNames;
	}

	public void set_modulesNames(List<IModule> _modulesNames) {
		this._modulesNames = _modulesNames;
	}

	public List<ITreeMenuOption> get_menuList() {
		return _menuList;
	}

	public void set_menuList(List<ITreeMenuOption> _menuList) {
		this._menuList = _menuList;
	}

	public HashSet<String> get_pluguinsNames() {
		return _pluguinsNames;
	}

	public void set_pluguinsNames(HashSet<String> _pluguinsNames) {
		this._pluguinsNames = _pluguinsNames;
	}

	public String get_perspectiveId() {
		return _perspectiveId;
	}

	public void set_perspectiveId(String _perspectiveId) {
		this._perspectiveId = _perspectiveId;
	}

	public String get_appBundleSymbolicName() {
		return _appBundleSymbolicName;
	}

	public void set_appBundleSymbolicName(String _appBundleSymbolicName) {
		this._appBundleSymbolicName = _appBundleSymbolicName;
	}

	public List<IOpcionBasicMenu> get_basicMenuList() {
		return _basicMenuList;
	}

	public void set_basicMenuList(List<IOpcionBasicMenu> _basicMenuList) {
		this._basicMenuList = _basicMenuList;
	}

	public String get_appWorkAreaStack() {
		return _appWorkAreaStack;
	}

	public void set_appWorkAreaStack(String _appWorkAreaStack) {
		this._appWorkAreaStack = _appWorkAreaStack;
	}

	public HashSet<String> get_menuListBuilder() {
		return _menuListBuilder;
	}

	public void set_menuListBuilder(HashSet<String> _menuListBuilder) {
		this._menuListBuilder = _menuListBuilder;
	}

	public HashSet<String> get_submenuListBuilder() {
		return _submenuListBuilder;
	}

	public void set_submenuListBuilder(HashSet<String> _submenuListBuilder) {
		this._submenuListBuilder = _submenuListBuilder;
	}

	public HashSet<IOpcionBasicMenu> get_basicMenuList2() {
		return _basicMenuList2;
	}

	public void set_basicMenuList2(HashSet<IOpcionBasicMenu> _basicMenuList2) {
		this._basicMenuList2 = _basicMenuList2;
	}
}
