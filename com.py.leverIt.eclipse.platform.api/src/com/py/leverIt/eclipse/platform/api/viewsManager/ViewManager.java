package com.py.leverIt.eclipse.platform.api.viewsManager;

public interface ViewManager {

	public void addBasicMenu();

	public void addMainMenu();

	public void addWorkArea();

	public void addSouthArea();
}
