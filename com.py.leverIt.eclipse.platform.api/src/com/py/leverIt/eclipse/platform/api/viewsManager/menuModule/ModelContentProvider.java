package com.py.leverIt.eclipse.platform.api.viewsManager.menuModule;

import java.util.Map;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class ModelContentProvider implements ITreeContentProvider  {
	
	private static final Object[] EMPTY_ARRAY = new Object[0];

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) {
		 if (inputElement instanceof Catalog){
//	         return ((List) inputElement).toArray();
	         return ((Catalog) inputElement).getItems().toArray();
		 }else{
	         return EMPTY_ARRAY;
		 }
		 
	}

	@Override
	public Object[] getChildren(Object parentElement) {
//		if (((Catalog) parentElement).getCatalogs() instanceof Map) {
		if (parentElement instanceof Map) {
			//Catalog catalog = (Catalog) parentElement;
			Map m = ((Catalog) parentElement).getCatalogs();
			return m.keySet().toArray();
		}else if (parentElement instanceof Catalog){
			Catalog catalog = (Catalog) parentElement;
			return catalog.getItems().toArray();
		}else{
			return EMPTY_ARRAY;
		}
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof Catalog) {
			return ((Catalog) element).getName();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Catalog) {
			if (!((Catalog) element).getItems().isEmpty()){
				return true;	
			}
		}
		 return false;
	}






}
