package com.py.leverIt.eclipse.platform.api.viewsManager;

import org.eclipse.swt.graphics.Image;

public interface ITreeMenuOption {
	public Object getFormulario();

	public String getNombreFuncionalidad();

	public String getNombrePadre();

	public String getIdForm();
	
	public Image getChildIconImage();
	
	public Image getParentIconImage();
	
	public String getFormURI();
	
	public String getFormTabLabel();
}
