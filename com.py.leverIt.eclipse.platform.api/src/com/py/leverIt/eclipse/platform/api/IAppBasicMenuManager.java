package com.py.leverIt.eclipse.platform.api;

import com.py.leverIt.eclipse.platform.api.constants.Apps;
import com.py.leverIt.eclipse.platform.api.viewsManager.IOpcionBasicMenu;

public interface IAppBasicMenuManager {

	public void addMenu(String _labelMenu);

	public <T> void addSubmenu(String _labelMenu, String _labelSubMenu);

	public void addMenuOption(IOpcionBasicMenu opcionBasicMenu);
	
	public void deleteMenu(String _labelMenu);
	
	public void deleteSubMenu(String _labelSubmenu);
	
	public void buildMenuFromActionList(Apps _activeApp);
	
	
	
	
}
