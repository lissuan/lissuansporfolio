package com.py.leverIt.eclipse.platform.api.viewsManager;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public abstract class AbstractTreeMenuOption implements ITreeMenuOption {

	protected Object formulario;
	
	protected Image _parentIconImage;
	
	protected Image _childIconImage;
	
	protected Class clazz;
	
	public boolean canExecute() {
		return false;
	}

	public void execute() {

	}

	@Override
	public Object getFormulario() {
		if (formulario == null) {
			init();
		}
		return this.formulario;
	}

	@Override
	public abstract String getNombreFuncionalidad();

	@Override
	public abstract String getNombrePadre();
	
	public abstract String getIdForm();

	public abstract void init();

	public abstract void onBtnAceptar();

	public abstract void onBtnCancelar();

	public abstract void onBtnEnterior();

	public abstract void onBtnSiguiente();
	
	public abstract Image getParentIcon();
	
	public abstract Image getChildIcon();
	
	public abstract Class getFormClass();
	
	public Image getParentIconImage(){
		_parentIconImage = getParentIcon();
		if (_parentIconImage == null) {
			Bundle bundle = FrameworkUtil.getBundle(AbstractTreeMenuOption.class);
			URL url = FileLocator.find(bundle, 
			  new Path("icons/folder.png"), null);
			Image image = ImageDescriptor.createFromURL(url).createImage();
			return image;
		}
		return _parentIconImage;
	}

	public Image getChildIconImage(){
		_childIconImage = getChildIcon();
		if (_childIconImage == null) {
			Bundle bundle = FrameworkUtil.getBundle(AbstractTreeMenuOption.class);
			URL url = FileLocator.find(bundle, 
			  new Path("icons/bricks.png"), null);
			Image image = ImageDescriptor.createFromURL(url).createImage();
			return image;
		}
		return _childIconImage;
	}
	
	@Override
	public String getFormURI(){
		clazz = getFormClass();
		if (clazz != null){
			Bundle bundle = FrameworkUtil.getBundle(clazz);
			bundle.getSymbolicName();
			return "bundleclass://" + bundle.getSymbolicName() + "/" + clazz.getCanonicalName();
		}
		return null;
	}

}
