package com.py.leverIt.eclipse.platform.api.module;

import org.osgi.framework.BundleContext;

public interface IModuleManager {

	public boolean registerModule(BundleContext _context, ClassLoader _loader);

	public String getAppParent();

	public String getModuleId();
}
