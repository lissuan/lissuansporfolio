package com.py.leverIt.eclipse.platform.api.constants;

public class BasicMenuAction {

	private String _parentName;
	private String _actionName;
	private Object _handler;
	private String _appName;

	public BasicMenuAction(String _nombrePadre,
			String _nombreFuncionalidad, Object _handler) {
		super();
		this._parentName = _nombrePadre;
		this._actionName = _nombreFuncionalidad;
		this._handler = _handler;
	}

	public Object get_handler() {
		return _handler;
	}

	public void set_handler(Object _handler) {
		this._handler = _handler;
	}

	public String get_actionName() {
		return _actionName;
	}

	public void set_actionName(String _actionName) {
		this._actionName = _actionName;
	}

	public String get_parentName() {
		return _parentName;
	}

	public void set_parentName(String _parentName) {
		this._parentName = _parentName;
	}

	public String get_appName() {
		return _appName;
	}

	public void set_appName(String _appName) {
		this._appName = _appName;
	}

}
