package com.py.leverIt.eclipse.platform.api.messages;

public class AppMessageConstants {
	
//	EVENTOS
	public static final String SHOW_MESSAGE_EVENT  = "showMessage";
	
//	TIPO DE DIALOGOS
	public static final int DIALOG_TYPE_ERROR = 1;
	public static final int DIALOG_TYPE_INFORMATION = 2;
	public static final int DIALOG_TYPE_QUESTION = 3;
	public static final int DIALOG_TYPE_WARNING = 4;
	public static final int DIALOG_TYPE_CONFIRM = 5;
	public static final int DIALOG_TYPE_QUESTION_WITH_CANCEL = 6;
	
//	NOMBRE DE CLAVES PARA EL RECURSO ShowMessage
	public static final String KEY_MESSAGE  = "message";
	public static final String KEY_DIALOG_TYPE  = "dialogType";
	

}
