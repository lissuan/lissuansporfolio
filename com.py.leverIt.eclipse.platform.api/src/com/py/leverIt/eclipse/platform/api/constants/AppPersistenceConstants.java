package com.py.leverIt.eclipse.platform.api.constants;

public class AppPersistenceConstants {
	
//	EVENTS
	public static final String CONNECT_DB_EVENT  = "connectDB";
	public static final String CONNECT_DB_PROPS_EVENT  = "connectDBProps";
	public static final String DISCONNECT_DB_EVENT  = "disconnectDB";
	public static final String PERSIST_ENTITY_EVENT  = "persistEntity";
	public static final String REMOVE_ENTITY_EVENT  = "removeEntity";
	public static final String UPDATE_ENTITY_EVENT  = "updateEntity";
	public static final String QUERY_LIST_EVENT  = "queryList";
	public static final String QUERY_ROW_EVENT  = "queryRow";
	
//	PARAMETERS FOR CONNECTION AND CREATION OF ENTITY MANAGER
	public static final String PERSISTENCE_UNIT_NAME = "PersistenceUnitTest";
	public static final String PERSISTENCE_DB_URL = "javax.persistence.jdbc.url";
	public static final String PERSISTENCE_DB_PASSWORD = "javax.persistence.jdbc.password";
	public static final String PERSISTENCE_DB_USER = "javax.persistence.jdbc.user";
	public static final String PERSISTENCE_DB_DRIVER = "javax.persistence.jdbc.driver";

//	PARAMETERS TO INTERACT WITH THE DB		
	public static final String PERSISTENCE_ENTITY_OBJECT = "entityObject";
	public static final String PERSISTENCE_QUERY_FOR_LIST = "queryList";
	public static final String PERSISTENCE_QUERY_FOR_ROW = "queryRow";
	
		
}
