package com.py.leverIt.eclipse.platform.api.viewsManager;

public interface IOpcionBasicMenu {
	public String getFuncionalidad();

	public String getNombrePadre();

	public Object getHandler();

}
