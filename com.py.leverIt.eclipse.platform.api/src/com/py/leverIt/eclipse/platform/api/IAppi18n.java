package com.py.leverIt.eclipse.platform.api;

import java.util.HashMap;
import org.eclipse.e4.ui.model.application.MApplicationElement;
import org.eclipse.swt.widgets.Widget;
import org.osgi.framework.BundleContext;

public interface IAppi18n {
	public HashMap<String, String> internationalizeMessages(
			BundleContext context, String objectName);

	public void internationalizeWorkArea(BundleContext context,
			String objectName, MApplicationElement component);

	public void internationalizeWorkArea(BundleContext context,
			String objectName, Widget component);
}
