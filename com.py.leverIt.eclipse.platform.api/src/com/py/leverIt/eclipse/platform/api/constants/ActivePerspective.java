package com.py.leverIt.eclipse.platform.api.constants;

public interface ActivePerspective {

	public String ACTIVEPERSPECTIVE2 = "com.py.leverIt.eclipse.product1.perspective.perspective1";
	public String ACTIVEPERSPECTIVE1 = "com.py.leverit.eclipse.product1.perspective.0";
	public String PERSPECTIVE1MENUMODULEID  = "com.py.leverIt.eclipse.product1.part.menumodulos";
	public String PERSPECTIVE2MENUMODULEID  = "com.py.leverit.eclipse.product1.perspective2.part.mainmenu";
	public String PERSPECTIVE1WORKAREAID = "com.py.leverIt.eclipse.product1.partstack.workarea";
	public String PERSPECTIVE2WORKAREAID = "com.py.leverit.eclipse.product1.partstack.1";
}
