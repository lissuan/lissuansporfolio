package com.py.leverIt.eclipse.platform.api;

import java.util.List;

/**
 * @author marco
 *
 */
public interface IAppFileManager {
	/**
	 * List all files in the File Repository Directory
	 * @return a List containing the names of the files found
	 */
	public List<String> listFiles();	
	/**
	 * List all files in the File Repository Directory that matches
	 * the filter regular expression pattern  
	 * @param filter
	 * @return a List containing the names of the files found
	 */
	public List<String> listFilesFilter(String filter);	
	/**
	 * List all files in the Directory specified in the path parameter
	 * @param filter specifies the absolute path of the Directory
	 * @return a List containing the URI of the files found
	 */
	public List<String> listFilesFromPath(String path);	
	/**
	 * List all files in the Directory specified in the path parameter
	 * that matches the filter regular expression pattern  
	 * @param path specifies the absolute path of the Directory
	 * @return a List containing the URI of the files found
	 */
	public List<String> listFilesFromPathFilter(String path,String filter);			
	/**
	 * Copy a file TO the File Repository Directory  
	 * @param fileURI specifies the URI of the file to be copied
	 * @return true if the operation is completed, and false otherwise
	 */
	public boolean uploadFile(String fileURI);	
	/**
	 * Copy a List of files TO the File Repository Directory  
	 * @param fileURIs specifies the URI of the files to be copied in a List
	 * @return true if the operation is completed for all files, and false otherwise
	 */
	public boolean uploadFiles(List<String> fileURIs);	
	/**
	 * Copy a file FROM the File Repository Directory the specified destination path  
	 * @param fileURIDestination specifies destination path of the file to be copied
	 * @param fileName specifies the name of the file to be copied
	 * @return true if the operation is completed for the file, and false otherwise
	 */
	public boolean downloadFile(String fileURIDestination, String fileName);	
	/**
	 * Copy a List of files FROM the File Repository Directory the specified destination path  
	 * @param fileURIDestination specifies destination path of the files to be copied
	 * @param fileNames specifies the names of the files to be copied
	 * @return true if the operation is completed for the all the files, and false otherwise
	 */
	public boolean downloadFiles(String fileURIDestination, List<String> fileNames);	
	/**
	 * @return the absolute path of the File Repository Directory
	 */
	public String getRepositoryURI();

}
