package com.py.leverIt.eclipse.platform.api.constants;

import java.util.List;

/**
 * @author marco
 *
 */
public class AppFileManagerConstants {
	
	// FILES REPOSITORY PATH NAME
	public static final String FILES_REPOSITORY_DIRECTORY = "home";
	
	// EVENTS
	public static final String EVENT_LIST_FILES  = "listFiles";
	public static final String EVENT_LIST_FILES_FILTER  = "listFilesFilter";
	public static final String EVENT_LIST_FILES_FROM_PATH  = "listFilesFromPath";
	public static final String EVENT_LIST_FILES_FROM_PATH_FILTER  = "listFilesFromPathFilter";
	public static final String EVENT_UPLOAD_FILE = "uploadFile";
	public static final String EVENT_UPLOAD_FILES = "uploadFiles";
	public static final String EVENT_DOWNLOAD_FILE  = "downloadFile";
	public static final String EVENT_DOWNLOAD_FILES  = "downloadFiles";
	public static final String EVENT_GET_REPOSITORY_URI  = "getRepositoryURI";
		
	// PARAMETER NAMES	
	public static final String PARAMETER_FILTER  = "filter";
	public static final String PARAMETER_PATH  = "path";
	public static final String PARAMETER_FILE_URI  = "fileURI";
	public static final String PARAMETER_FILES_URI  = "fileURIs";
	public static final String PARAMETER_FILE_NAME = "fileName";
	public static final String PARAMETER_FILE_NAMES = "fileNames";
	public static final String PARAMETER_FILE_URI_DESTINATION = "fileURIDestination";
}
