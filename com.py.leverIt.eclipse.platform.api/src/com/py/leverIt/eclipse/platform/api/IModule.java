package com.py.leverIt.eclipse.platform.api;

public interface IModule {
	public String getAppParentName();

	public String getModuleId();

	public String getModuleName();
}
