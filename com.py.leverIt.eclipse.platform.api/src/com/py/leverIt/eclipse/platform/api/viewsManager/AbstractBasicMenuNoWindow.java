package com.py.leverIt.eclipse.platform.api.viewsManager;

public abstract class AbstractBasicMenuNoWindow implements IOpcionBasicMenu{

	public abstract void canExecute();

	public abstract void Execute();
}
