package com.py.leverIt.eclipse.platform.api;

import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

public interface IAppView {

	public void initAppView();

	public void exitAppView();

	public void setEpartService(EPartService ps);

	public EPartService getEPartService();

	public void setMenuModulePart(MPart part);

	public MPart getMenuModulePart();

	public void setApplication(MApplication app);

	public MApplication getApplication();

	public void setTrimmedWindow(MTrimmedWindow tw);

	public MTrimmedWindow getTimmedWindow();

	public void setMainMenu(MMenu menu);

	public MMenu getMainMenu();

	public void setWorkAreaPartStack(MPartStack partStack);

	public MPartStack getWorkAreaPartStack();
	
	public void setIEclipseContext(IEclipseContext iec);
	
	public IEclipseContext getIEclipseContext();

	public void setUISync(UISynchronize sync);
	
	public UISynchronize getUISync();
	
	public void setEModelService(EModelService ems);
	
	public EModelService getModelService();
	
	public EHandlerService getHandlerService();
	
	public void setHandlerService(EHandlerService handlerService);
	
	public IEventBroker getEventBroker();
		
	public void setEventBroker(IEventBroker eventBroker);
	
	

}
