package com.py.leverIt.eclipse.platform.api.viewsManager;

public abstract class AbstractBasicMenuOption implements IOpcionBasicMenu {
	
	protected Object formulario;
	
	public boolean canExecute() {
		return true;
	}

	public abstract void execute();
}
