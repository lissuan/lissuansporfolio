package com.py.leverIt.eclipse.platform.api.exception;

public class BussinesLogicException extends Exception{	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	
	public BussinesLogicException(String message) {
		super(message);
		
	}
}
