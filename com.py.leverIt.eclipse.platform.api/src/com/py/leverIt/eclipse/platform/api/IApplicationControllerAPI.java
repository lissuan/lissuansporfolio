package com.py.leverIt.eclipse.platform.api;

public interface IApplicationControllerAPI {

	public String getNombre();

	public String getId();

	public String getPerspectiveType();

}
