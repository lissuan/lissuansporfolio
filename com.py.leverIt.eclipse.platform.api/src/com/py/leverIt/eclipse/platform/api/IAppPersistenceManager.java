package com.py.leverIt.eclipse.platform.api;

import java.util.List;
import java.util.Map;

public interface IAppPersistenceManager {
	public boolean connectDb();
	public boolean connectDbProps(Map<String,String> props);
	public void disconnectDb();
	public boolean persistEntity(Object entity);
	public boolean removeEntity(Object entity);
	public boolean updateEntity(Object entity);
	public List<Object> queryList(String namedQuery,Object entity);
	public Object queryRow(String namedQuery, Object entity);

}
