package com.py.leverIt.eclipse.platform.api.viewsManager.menuModule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.graphics.Image;

public class Catalog extends AbstractModelObject{
	private String name;
	private String idForm;
	private Image image;
	private Map<String, String> catalogs = new HashMap<String, String>();
	private List<Catalog> items = new ArrayList<Catalog>();

	public Catalog(String name) {
		this.name = name;
		idForm = "idFormEmpty";
	}
	
	public Catalog(String name, String idForm){
		this.name = name;
		this.idForm = idForm;
	}
	
	public Catalog(String name, String idForm, Image image){
		this.name = name;
		this.idForm = idForm;
		this.image = image;
	}

	public Catalog(String name, Image image){
		this.name = name;
		this.image = image;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		firePropertyChange("name", this.name, this.name = name);
		
	}

	public Map getCatalogs() {
		return catalogs;
	}

	public void setCatalogs(Map catalogs) {
		firePropertyChange("catalogs", this.catalogs,
				this.catalogs = catalogs);
	
	}

	public List getItems() {
		return items;
	}

	public void setItems(List<Catalog> items) {
		firePropertyChange("items", this.items, this.items = items);
		
	}
	
	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		firePropertyChange("idForm", this.idForm, this.idForm = idForm);
		
	}
	
	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		firePropertyChange("image", this.image, this.image = image);
		
	}
	
}
