package com.py.leverIt.eclipse.platform.api.i18n;

public class AppI18nConstants {


	public static final String LANGUAGE_EN  = "en";
	public static final String LANGUAGE_ES  = "es";
	
	public static String language = LANGUAGE_EN;

	public static String getLanguage() {
		return language;
	}

	public static void setLanguage(String language) {
		AppI18nConstants.language = language;
	}
	
	
}
