package com.py.leverIt.eclipse.platform.api.general;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.EventHandler;

import com.py.leverIt.eclipse.platform.api.IAppBasicMenuManager;
import com.py.leverIt.eclipse.platform.api.IAppsManager;
import com.py.leverIt.eclipse.platform.api.general.events.FrameworkEvent;
import com.py.leverIt.eclipse.platform.api.perspectiviesManager.IPerspectiveManager;
//import com.py.leverIt.eclipse.platform.api.perspectiviesManager.IPerspectiveManager;
import com.py.leverIt.eclipse.platform.api.viewsManager.IOpcionBasicMenu;

public class UrianaOsgiContext implements BundleListener, FrameworkListener,
		ServiceListener, EventHandler {

	private static BundleContext _bundleContext;
	@SuppressWarnings("unused")
	private static EventAdmin _eventAdmin;
	private static HashSet<String> _servicesList = new HashSet<>();
	private static HashMap<String, BundleContext> _bundleContextMap = new HashMap<>();

	public static HashMap<String, BundleContext> get_bundleContextMap() {
		return _bundleContextMap;
	}

	public static void set_bundleContextMap(
			HashMap<String, BundleContext> _bundleContextMap) {
		UrianaOsgiContext._bundleContextMap = _bundleContextMap;
	}

	public static BundleContext get_bundleContext() {
		return _bundleContext;
	}

	public static void set_bundleContext(BundleContext _bundleContext) {
		UrianaOsgiContext._bundleContext = _bundleContext;
	}

	public static EventAdmin get_eventAdmin() {

		ServiceReference<?> _eventServiceRefe = get_bundleContext()
				.getServiceReference(EventAdmin.class.getName());
		EventAdmin _eventAdminObj = (EventAdmin) get_bundleContext()
				.getService(_eventServiceRefe);
		return _eventAdminObj;
	}

	public static void set_eventAdmin(EventAdmin _eventAdmin) {
		UrianaOsgiContext._eventAdmin = _eventAdmin;
	}

	public static Object findService(String _serviceName) {

		if (!_bundleContextMap.isEmpty()) {
			ServiceReference<?> _referenciaAbs = null;
			Collection<BundleContext> values = _bundleContextMap.values();
			for (BundleContext bundleContext : values) {
				_referenciaAbs = bundleContext
						.getServiceReference(_serviceName);
				if (_referenciaAbs != null) {
					return bundleContext.getService(_referenciaAbs);
				}
			}
		}
		return null;
	}

	public static void sendEvent(FrameworkEvent _event) {
		get_eventAdmin().sendEvent(_event);
	}

	public static void sendEventAsyncronous(FrameworkEvent _event) {
		get_eventAdmin().postEvent(_event);
	}

	@Override
	public void serviceChanged(ServiceEvent event) {
		if (event.getType() == ServiceEvent.REGISTERED) {
			ServiceReference<?> serviceReference = event.getServiceReference();
			boolean add = get_servicesList().add(serviceReference.toString());
			System.out.println(serviceReference.toString());
			if (add) {
//				if (serviceReference.toString().contains(
//						IApplicationControllerAPI.class.getName())) {
//					IApplicationControllerAPI objApp = (IApplicationControllerAPI) serviceReference
//							.getBundle().getBundleContext()
//							.getService(serviceReference);
//					getIAppManager().addApp(
//							objApp,
//							serviceReference.getBundle().getBundleContext()
//									.getBundle().getSymbolicName());
//					getIPerspectiveManager().addProyectToList(
//							objApp.getNombre());
//				}
//				if (serviceReference.toString().contains(
//						IModule.class.getName())) {
//					IModule objApp = (IModule) serviceReference.getBundle()
//							.getBundleContext().getService(serviceReference);
//					getIAppManager().addModuleToProyect(objApp,
//							objApp.getAppParentName(),
//							serviceReference.getBundle().getSymbolicName());
//					getIPerspectiveManager().addModuleToProyect(
//							objApp.getAppParentName(), objApp.getModuleId());
//				}
//				if (serviceReference.toString().contains(
//						IOpcionBasicMenu.class.getName())) {
//					IOpcionBasicMenu objBasicMenu = (IOpcionBasicMenu) serviceReference
//							.getBundle().getBundleContext()
//							.getService(serviceReference);
//					IAppBasicMenuManager iAppBasicService = (IAppBasicMenuManager) findService(IAppBasicMenuManager.class
//							.getName());
//					iAppBasicService.addMenuOption(objBasicMenu);
//				}
//				if (serviceReference.toString().contains(
//						ITreeMenuOption.class.getName())) {
//					ITreeMenuOption objBasicMenu = (ITreeMenuOption) serviceReference
//							.getBundle().getBundleContext()
//							.getService(serviceReference);
//					IAppModuleMenu objIappModuleMenu = (IAppModuleMenu) findService(IAppModuleMenu.class
//							.getName());
//					objIappModuleMenu.addMenuOption(objBasicMenu);
//					getIAppManager().addTreeMenuOptionToAppMenuList(
//							serviceReference, objBasicMenu);
//				}
			}
		}

	}

	@SuppressWarnings("unused")
	private boolean isRegistered(ServiceReference<?> serviceReference) {
		BundleContext bundleContext = _bundleContextMap.get(serviceReference
				.getBundle().getSymbolicName());
		if (bundleContext == null) {
			return true;
		}
		Object service = bundleContext.getService(serviceReference);
		if (service != null) {
			return true;
		}
		return false;
	}

	private IAppsManager getIAppManager() {
		IAppsManager findService = (IAppsManager) findService(IAppsManager.class
				.getName());
		return findService;
	}

	private IPerspectiveManager getIPerspectiveManager() {
		IPerspectiveManager findService = (IPerspectiveManager) findService(IPerspectiveManager.class
				.getName());
		return findService;
	}

	@Override
	public void frameworkEvent(org.osgi.framework.FrameworkEvent event) {
		System.out.println(event.toString());
	}

	@Override
	public void bundleChanged(BundleEvent event) {
		if (event.getType() == BundleEvent.STARTED) {
			if (!_bundleContextMap.containsKey(event.getBundle()
					.getSymbolicName())) {
				_bundleContextMap.put(event.getBundle().getSymbolicName(),
						event.getBundle().getBundleContext());
				System.out.println("Se activo el bundle "
						+ event.getBundle().getSymbolicName());
			}
		}
	}

	@Override
	public void handleEvent(Event arg0) {
		System.out.println("Se el evnto " + arg0.toString());

	}

	public static HashSet<String> get_servicesList() {
		return _servicesList;
	}

	public static void set_servicesList(HashSet<String> _servicesList) {
		UrianaOsgiContext._servicesList = _servicesList;
	}

}
