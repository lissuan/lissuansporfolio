package com.py.leverIt.eclipse.platform.api;

import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.py.leverIt.eclipse.platform.api.constants.Apps;
import com.py.leverIt.eclipse.platform.api.viewsManager.IOpcionBasicMenu;
import com.py.leverIt.eclipse.platform.api.viewsManager.ITreeMenuOption;

public interface IAppsManager {

	public void addApp(IApplicationControllerAPI _aplication,
			String _pluguinName);

	public void addModuleToProyect(IModule _module, String _aplication,
			String _pluguinName);

	public List<Apps> getAppsList();

	public void initApp(Apps _appToInit);

	public void pauseApp(Apps _appToPause);

	public void resumeApp(Apps _appToResume);

	public void restartApp(Apps _appToRestart);

	public void stopApp(Apps _appToStop);

	public List<ITreeMenuOption> getAppMenuList(String _aplication);

	public void addTreeMenuOptionToAppMenuList(
			@SuppressWarnings("rawtypes") ServiceReference _referencia,
			ITreeMenuOption _menuOption);

	public void addTreeMenuOptionToAppMenuList(String _appName,
			ITreeMenuOption _menuOption);

	public Apps getActiveApplication();

	public void setActiveApplication(String _activeApplication);

	public boolean addApp(String _name, BundleContext _bundleContext,
			String _appSkin);

	public boolean addModuleToApp(String _name, BundleContext bundleContext);

	public boolean addBasicMenuToApp(IOpcionBasicMenu _opcionBasicMenu,
			String _appName);
}
