package com.py.leverIt.eclipse.platform.api.general.events;

import java.util.Dictionary;
import java.util.Map;

import org.osgi.service.event.Event;

/**
 * The FrameworkEvent class is the base class for communication events
 * through the Uriana Framework.
 * 
 * @author Carlos Velasco
 * 
 */

public class FrameworkEvent extends Event {

	public FrameworkEvent(String topic, Dictionary<String, ?> properties) {
		super(topic, properties);
		// TODO Auto-generated constructor stub
	}

	public FrameworkEvent(String topic, Map<String, ?> properties) {
		super(topic, properties);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Default event to initialize the framework execution.
	 */
	public static final String URIANA_INITIALIZE = "URIANA_INITIALIZE";

	/**
	 * Default event to finalize the framework execution.
	 */
	public static final String URIANA_FINALIZE = "URIANA_FINALIZE";

	/**
	 * Default event to finalize framework modules execution.
	 */
	public static final String URIANA_MODULE_DESTROY = "URIANA_MODULE_DESTROY";

	/**
	 * type Event type denomination.
	 */
	protected String type;

	/**
	 * id Event caller identification. By default '*' which means 'not known'.
	 */
	protected String id;

	/**
	 * data The data object which this event transports.
	 */
	protected Object data;

	/**
	 * error A boolean value to specify error events.
	 */
	protected boolean error;

	/**
	 * errorMessage A message explaining the error cause.
	 */
	protected String errorMessage;

	/**
	 * errorCode A short string to handle the error.
	 */
	protected String errorCode;

	/*
	 * 
	 * */

	protected boolean synchronous;

	/**
	 * Constructs a new FrameworkEvent instance.
	 * 
	 * @param type
	 *            A name to specify the event's current type.
	 * @param data
	 *            A data object to be sent with the event.
	 * 
	 */
	// public FrameworkEvent(String type, Object data){
	// this.type = type;
	// this.id= "*";
	// this.data = data;
	// this.error = false;
	// this.errorCode = "";
	// this.errorMessage = "";
	// }
	//
	//
	// /**
	// * Constructs a new FrameworkEvent instance.
	// * @param type A name to specify the event's current type.
	// * @param id A callers identification string. By default '*'. It serves to
	// determine if a received event of the current type is of interest or not
	// for the current receptor.
	// * @param data A data object to be sent with the event.
	// *
	// */
	// public FrameworkEvent(String type, String id, Object data){
	// this.type = type;
	// this.id= id;
	// this.data = data;
	// this.error = false;
	// this.errorCode = "";
	// this.errorMessage = "";
	// }
	//
	// /**
	// * Constructs a new FrameworkEvent instance.
	// * @param type A name to specify the event's current type.
	// * @param id A callers identification string. By default '*'. It serves to
	// determine if a received event of the current type is of interest or not
	// for the current receptor.
	// * @param data A data object to be sent with the event.
	// * @param error A boolean value showing if this event refers to an error.
	// * @param errorCode A String code to handle the error (in case it is an
	// error Event).
	// * @param errorMessage A String explanation of the error.
	// *
	// */
	// public FrameworkEvent(String type, String id, Object data, boolean
	// error,
	// String errorCode, String errorMessage){
	// this.type = type;
	// this.id= id;
	// this.data = data;
	// this.error = error;
	// this.errorCode = errorCode;
	// this.errorMessage = errorMessage;
	// }

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isError() {
		return error;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setID(String id) {
		this.id = id;
	}

	public String getID() {
		return id;
	}

	public String getType() {
		return type;
	}

	public boolean isSynchronous() {
		return synchronous;
	}

	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}
}
