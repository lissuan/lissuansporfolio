package com.py.leverIt.eclipse.platform.api;

public interface IAppExceptions {

	public void reportException(Exception _exception);
}
