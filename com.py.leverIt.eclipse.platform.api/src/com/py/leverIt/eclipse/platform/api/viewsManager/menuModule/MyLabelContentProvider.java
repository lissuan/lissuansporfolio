package com.py.leverIt.eclipse.platform.api.viewsManager.menuModule;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class MyLabelContentProvider extends LabelProvider {
	@Override
	public String getText(Object element) {
		if (element instanceof Catalog) {
			return ((Catalog) element).getName();
		}
		return null;
	}

	public Image getImage(Object element) {
		return ((Catalog) element).getImage();
	}
}
