package com.py.leverIt.eclipse.platform.api.perspectiviesManager;

public interface IPerspectiveManager {

	public boolean addProyectToList(String _proyectName);

	public void switchPerspective(String _perspectiveName);
	
	public boolean addModuleToProyect(String _proyectName, String _moduleName);
	
}
