package hybrid.touchrate.com.touchcount.comunication;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.w3c.dom.CharacterData;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import hybrid.touchrate.com.touchcount.managers.CurrentManager;

/**
 * Created by lissuan on 3/17/2016.
 */
public class SendData {


    private static String SOAP_ACTION ;
    private static String METHOD_NAME;
    private static String NAMESPACE;
    private static String APP_NAME;
    private static CommonComunicate utils;


    public void init()
    {
        SOAP_ACTION = ConfigManager.getInstance().getConfig().getProperty("SOAP_ACTION_SEND");
        METHOD_NAME = ConfigManager.getInstance().getConfig().getProperty("METHOD_NAME_SEND");
        NAMESPACE = ConfigManager.getInstance().getConfig().getProperty("NAMESPACE");
        APP_NAME= ConfigManager.getInstance().getConfig().getProperty("APP_NAME");
        utils = new CommonComunicate();
    }

    /**
     * Main method that gets a string and the database. its in charge of sending
     * the information to the web service returns true if sent else false
     *
     * @param string XML with all the collected data to be sent
     * @return true if sent else false
     * @throws IOException
     */
    public boolean sendData(final String string, boolean logout) throws IOException, TransformerException, ParserConfigurationException {

        SoapObject Request = new SoapObject(NAMESPACE, METHOD_NAME);

        String parameters=SendParameters(logout);

        Request.addProperty("CustomParameters",parameters);
        Request.addProperty("XML", string);
        Request.addProperty("appName", APP_NAME);

        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.dotNet = true;// for .net server
        soapEnvelope.setOutputSoapObject(Request);
        soapEnvelope.headerOut = new org.kxml2.kdom.Element[1];// Creating the Soap header to be sent as part of the request. Web Service Security
        soapEnvelope.headerOut[0] = utils.buildSoapAuthHeader();// Calling method that creates the soap header structure

        try {

            String wsResult = utils.callWebService(soapEnvelope, SOAP_ACTION);
            Log.d("SEND DATA", "" + wsResult);
            if (checkIfSent(wsResult)) {
                Log.d("SEND DATA", "lo mande");
            //TODO wherever it has to be do when the updates takes place
                return true;
            } else {
                Log.d("SEND DATA", " no lo mande");
                //TODO wherever it has to be do when the updates takes
                return false;
            }

        } catch (Exception e) {
            Log.d("WEB SERVICE", "CAN'T READ WEB SERVICE");
            Log.d("WEB SERVICE ERROR", e.getMessage());
            return false;
        }
    }


    public String buildXmlReports(){
        return UpdateManager.getInstance().initUpdate();
    }



    @TargetApi(Build.VERSION_CODES.FROYO)
    private String SendParameters(boolean logout) throws ParserConfigurationException,
            TransformerException{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();

        Document doc = impl.createDocument(null, null, null);
        Element e1 = doc.createElement("Parameters");
        doc.appendChild((Node) e1);

        String storeId= CurrentManager.getInstance().getCurrentUserInteraction().getDeviceId();
        String uuid= CurrentManager.getInstance().getCurrentUserInteraction().getUuid();


        /**
         * storeId
         * */
        Element parameter = doc.createElement("Parameter");
        ((Node) e1).appendChild((Node) parameter);

        Element name =  doc.createElement("name");
        ((Node) parameter).appendChild((Node) name);
        ((Node) name).appendChild(doc.createTextNode("InventoryId"));

        Element value = doc.createElement("value");
        ((Node) parameter).appendChild((Node) value);
        ((Node) value).appendChild(doc.createTextNode(storeId));

        Element datatype = doc.createElement("datatype");
        ((Node) parameter).appendChild((Node) datatype);
        ((Node) datatype).appendChild(doc.createTextNode("string"));

     /*LogOut*/

        Element parameter2 =  doc.createElement("Parameter");
        ((Node) e1).appendChild((Node) parameter2);

        Element name2 =  doc.createElement("name");
        ((Node) parameter2).appendChild((Node) name2);
        ((Node) name2).appendChild(doc.createTextNode("LogOut"));

        //TODO ask juan for the logout param.
        Element value2 =  doc.createElement("value");
        ((Node) parameter2).appendChild((Node) value2);
        ((Node) value2).appendChild(doc.createTextNode(String.valueOf(logout?1:0)));

        Element datatype2 =  doc.createElement("datatype");
        ((Node) parameter2).appendChild((Node) datatype2);
        ((Node) datatype2).appendChild(doc.createTextNode("int"));

        /**
         * UUID
         **/

        Element parameter3 =  doc.createElement("Parameter");
        ((Node) e1).appendChild((Node) parameter3);

        Element name3 =  doc.createElement("name");
        ((Node) parameter3).appendChild((Node) name3);
        ((Node) name3).appendChild(doc.createTextNode("StoreId"));

        Element value3 =  doc.createElement("value");
        ((Node) parameter3).appendChild((Node) value3);
        ((Node) value3).appendChild(doc.createTextNode(uuid==null?"":uuid));

        Element datatype3 =  doc.createElement("datatype");
        ((Node) parameter3).appendChild((Node) datatype3);
        ((Node) datatype3).appendChild(doc.createTextNode("string"));


        /**
         *  Battery Level now will provide if the device had crashed*
         * */

        int batteryLevel= UtilsComucation.getErrorCode();


        Element parameter4 =  doc.createElement("Parameter");
        ((Node) e1).appendChild((Node) parameter4);

        Element name4 =  doc.createElement("name");
        ((Node) parameter4).appendChild((Node) name4);
        ((Node) name4).appendChild(doc.createTextNode("Battery"));

        Element value4 = doc.createElement("value");
        ((Node) parameter4).appendChild((Node) value4);
        ((Node) value4).appendChild(doc.createTextNode(Integer.toString((batteryLevel))));

        Element datatype4 = doc.createElement("datatype");
        ((Node) parameter4).appendChild((Node) datatype4);
        ((Node) datatype4).appendChild(doc.createTextNode("int"));


        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        java.io.StringWriter sw = new java.io.StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(domSource, sr);
        String parameters = sw.toString();
        Log.d("PARAMETERS", parameters);
        return parameters;
    }



    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "?";
    }
    public boolean checkIfSent(String string) {

        boolean sent = false;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(string));
            Document doc = db.parse(is);
            Element line;
            NodeList name = doc.getElementsByTagName("Message");
            line = (Element) name.item(0);

            if (1 == Integer.parseInt(getCharacterDataFromElement(line)))
                sent = true;

            Log.d("CONFIGURATION XML", "Message: " + getCharacterDataFromElement(line));// put in database

        } catch (Exception e) {
            e.printStackTrace();
        }

        return sent;

    }
}
