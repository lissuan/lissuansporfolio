package hybrid.touchrate.com.touchcount.comunication;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.InputStream;
import java.util.Properties;

import hybrid.touchrate.com.touchcount.managers.ReportController;

/**
 * Created by lissuan on 3/17/2016.
 */
public class ConfigManager {

    private static ConfigManager instance;


    public static ConfigManager getInstance() {
        if (instance==null){
            instance =new ConfigManager();
            load();
        }
        return instance;
    }
    private static Properties config;

    public static void load()
    {
        Resources resources = ReportController.getInstance().getContext().getResources();
        AssetManager assetManager = resources.getAssets();
        try {
            InputStream inputStream = assetManager.open("configTouch.properties");
            config = new Properties();
            config.load(inputStream);
            System.out.println("The config properties are now loaded");
            System.out.println("properties: " + config);
            } catch (Exception e) {
            System.err.println("Failed to open config property file");
            e.printStackTrace();
        }
    }

    public Properties getConfig() {
        return config;
    }

}
