package hybrid.touchrate.com.touchcount.model;

import android.content.res.Resources;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by lissuan on 3/8/2016.
 */
public class UserInteraction extends RealmObject {

    @PrimaryKey
    private int id;
    private String deviceId;
    private Date dateActionPerformed;
    private long durationTime;
    private Date dateFinished;
    private String uuid;
    private RealmList<Session> sessionRealmList;

    public UserInteraction() {
        durationTime=0;
    }

    public UserInteraction(String deviceId, Date dateActionPerformed, long durationTime, Date dateFinished, RealmList<Session> sessionRealmList) {
        this.deviceId = deviceId;
        this.dateActionPerformed = dateActionPerformed;
        this.durationTime = durationTime;
        this.dateFinished = dateFinished;


        this.sessionRealmList = sessionRealmList;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getDateActionPerformed() {
        return dateActionPerformed;
    }

    public void setDateActionPerformed(Date dateActionPerformed) {
        this.dateActionPerformed = dateActionPerformed;
    }

    public long getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(long durationTime) {
        this.durationTime = durationTime;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public RealmList<Session> getSessionRealmList() {
        return sessionRealmList;
    }

    public void setSessionRealmList(RealmList<Session> sessionRealmList) {
        this.sessionRealmList = sessionRealmList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
