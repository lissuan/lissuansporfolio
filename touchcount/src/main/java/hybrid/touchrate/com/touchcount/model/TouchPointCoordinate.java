package hybrid.touchrate.com.touchcount.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by lissuan on 3/11/2016.
 */
public class TouchPointCoordinate extends RealmObject {


    private String screenName;
    private float x;
    private float y;
    private Date datePerformed;

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Date getDatePerformed() {
        return datePerformed;
    }

    public void setDatePerformed(Date datePerformed) {
        this.datePerformed = datePerformed;
    }
}
