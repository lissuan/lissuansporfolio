package hybrid.touchrate.com.touchcount.managers;

import hybrid.touchrate.com.touchcount.model.Screen;
import hybrid.touchrate.com.touchcount.model.Session;
import hybrid.touchrate.com.touchcount.model.UserInteraction;

/**
 * Created by lissuan on 3/14/2016.
 */
public class CurrentManager {

    private UserInteraction currentUserInteraction;
    private Session currentSession;
    private Screen currentScreen;
    private static CurrentManager instance;


    public static CurrentManager getInstance(){
        if (instance==null){
            instance=new CurrentManager();
        }
        return instance;
    }

    public UserInteraction getCurrentUserInteraction() {
        return currentUserInteraction;
    }

    public void setCurrentUserInteraction(UserInteraction currentUserInteraction) {
        this.currentUserInteraction = currentUserInteraction;
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Screen getCurrentScreen() {
        return currentScreen;
    }

    public void setCurrentScreen(Screen currentScreen) {
        this.currentScreen = currentScreen;
    }
}
