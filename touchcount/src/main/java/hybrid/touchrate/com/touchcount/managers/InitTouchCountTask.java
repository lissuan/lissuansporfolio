package hybrid.touchrate.com.touchcount.managers;

import android.content.Context;

import java.util.Date;

import hybrid.touchrate.com.touchcount.model.ActionReport;
import hybrid.touchrate.com.touchcount.model.Screen;
import hybrid.touchrate.com.touchcount.model.Session;
import hybrid.touchrate.com.touchcount.model.UserInteraction;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lissuan on 3/10/2016.
 */
public class InitTouchCountTask implements Runnable {

    private Context context;
    private String deviceId;
    private String uuid;

    public InitTouchCountTask(Context context,String deviceId,String uuid) {
        this.context = context;
        this.deviceId  = deviceId;
        if (uuid!=null && !uuid.equals("")){
        this.uuid=uuid;}else{
            this.uuid="test";
        }
    }

    @Override
    public void run() {
        ReportController.getInstance().setMyConfig(new RealmConfiguration.Builder(context)
                .name("reportsRealm")
                .setModules(new TouchCountRealmModule())
                .deleteRealmIfMigrationNeeded()
                .build());
        Realm realm = Realm.getInstance(ReportController.getInstance().getMyConfig());
        realm.beginTransaction();
        realm.clear(ActionReport.class);
        realm.clear(Screen.class);
        realm.clear(Session.class);
        realm.clear(UserInteraction.class);
        realm.commitTransaction();
        realm.close();
        ReportController.getInstance().createUserIntereacction(new Date(),deviceId,uuid);
    }
}
