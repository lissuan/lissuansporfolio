package hybrid.touchrate.com.touchcount.managers;



import android.content.Context;

import java.util.Date;

import hybrid.touchrate.com.touchcount.model.ActionReport;
import hybrid.touchrate.com.touchcount.model.ActionReportType;
import hybrid.touchrate.com.touchcount.model.Screen;
import hybrid.touchrate.com.touchcount.model.Session;
import hybrid.touchrate.com.touchcount.model.UserInteraction;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;

/**
 * Created by lissuan on 3/9/2016.
 */
public class ReportController {

    private static ReportController instance;
    private static RealmConfiguration myConfig;
    private static Context context;


    public static ReportController getInstance(){
        if (instance==null){
            instance=new ReportController();
        }
        return instance;
    }

    public void initTouchCount(Context context,String deviceId,String uuid){
        setContext(context);
        InitTouchCountTask task1= new InitTouchCountTask(context, deviceId,uuid);
        task1.run();
    }



    /**
     * Creates a new @Link UserInteraction to store all the sessions and screens
     * */
    public void createUserIntereacction(Date date, String idDevice,String uuid){
        Realm realm = Realm.getInstance(getMyConfig());
        realm.beginTransaction();
        UserInteraction userInteraction = new UserInteraction();
        userInteraction.setId(getIdForUserInteraction());
        if (uuid!=null && !uuid.equals("")){
        userInteraction.setUuid(uuid);}else{
            userInteraction.setUuid("tesing uuid");
        }
        userInteraction.setDateActionPerformed(date);
        userInteraction.setDeviceId(idDevice);
        userInteraction.setSessionRealmList(new RealmList<Session>());
        Session currentSession = new Session();
        currentSession.setId(getIdForSession());
        currentSession.setDateActionPerformed(new Date());
        currentSession.setScreenRealmList(new RealmList<Screen>());
        Screen currentScreen = new Screen();
        currentScreen.setDateActionPerformed(date);
        currentScreen.setScreenName("LandindPage");
        currentScreen.setId(getIdForScreen());
        currentSession.getScreenRealmList().add(currentScreen);
        userInteraction.getSessionRealmList().add(currentSession);
        realm.copyToRealm(userInteraction);
        realm.commitTransaction();
        realm.close();
        CurrentManager.getInstance().setCurrentUserInteraction(userInteraction);
        CurrentManager.getInstance().setCurrentSession(currentSession);
        CurrentManager.getInstance().setCurrentScreen(currentScreen);

    }

    private UserInteraction getCurrentUserInteraction(){
       return CurrentManager.getInstance().getCurrentUserInteraction();
    }

    /**
     * the call of this method is when start a new session bteween the UserInteraction
     * */
    public void createSession(Date date) {
        Realm realm = Realm.getInstance(getMyConfig());
        realm.beginTransaction();
        Session currentSession= realm.createObject(Session.class);
        currentSession.setDateActionPerformed(date);
        currentSession.setScreenRealmList(new RealmList<Screen>());
        Screen currentScreen = realm.createObject(Screen.class);
        currentScreen.setDateActionPerformed(date);
        currentSession.getScreenRealmList().add(currentScreen);
        getCurrentUserInteraction().getSessionRealmList().add(currentSession);
        realm.commitTransaction();
        realm.close();
        CurrentManager.getInstance().setCurrentSession(currentSession);
    }

    /**
     * name : the name of the screen for the reports
     * screenValue : is the integer representing the layout scren
     * **/
    public void addScreenToCurrentSession(String name,int screenValue){
        Date date = new Date();
        Realm realm = Realm.getInstance(getMyConfig());
        realm.beginTransaction();
        Screen screen = new Screen();
        screen.setId(getIdForScreen());
        screen.setDateActionPerformed(date);
        screen.setScreenName(name);
        screen.setScreenValue(screenValue);
        realm.copyToRealm(screen);
        Session session = CurrentManager.getInstance().getCurrentSession();
        if (session.getScreenRealmList()==null){
            session.setScreenRealmList(new RealmList<Screen>());
        }
        session.getScreenRealmList().add(screen);
        realm.copyToRealmOrUpdate(session);
        realm.copyToRealmOrUpdate(getCurrentUserInteraction());
        realm.commitTransaction();
        realm.close();
        CurrentManager.getInstance().setCurrentScreen(screen);
    }


    private void addTouchToScreen(String parameter,String screenName){
       Realm realm = Realm.getInstance(getMyConfig());
       RealmList<Screen> screenList=  CurrentManager.getInstance().getCurrentSession().getScreenRealmList();
       for (Screen screenItem: screenList){
           if (screenItem.getScreenName().equals(screenName)){
               realm.beginTransaction();
               ActionReport actionReport = new ActionReport();
               actionReport.setActionValue(parameter);
               actionReport.setActionReportType(ActionReportType.TOUCHACTIONREPORT);
               actionReport.setDateActionPerformed(new Date());
               actionReport.setDateFinished(new Date());
               actionReport.setCustomValue("custom");
               realm.copyToRealm(actionReport);

               if (screenItem.getActionReportRealmList()==null){
                   screenItem.setActionReportRealmList(new RealmList<ActionReport>());
               }
               screenItem.getActionReportRealmList().add(actionReport);
               realm.copyToRealmOrUpdate(screenItem);
               realm.commitTransaction();
               realm.close();
           }
       }
    }

    public void addTouchScreenPointReportToCurrentScreen(float x,float y, String screenName){
        if (screenName!=null && !screenName.equals(""))
        {
            String parameter = "x: " +x +" y: "+ y;
            addTouchToScreen(parameter,screenName);
        }
    }

    public void addActionReportToCurrentScreen(String btnAction, String customParameter, int screenValue,String name){
        Realm realm = Realm.getInstance(getMyConfig());
        Screen actionScreen = CurrentManager.getInstance().getCurrentScreen();
        Date date = new Date();
        if (actionScreen==null){
            addScreenToCurrentSession(name,screenValue);
            actionScreen = realm.where(Screen.class).equalTo("screenValue",screenValue).findFirst();
        }
        realm.beginTransaction();
        ActionReport actionReport = new ActionReport();
        actionReport.setActionValue(customParameter);
        actionReport.setActionReportType(btnAction);
        actionReport.setDateActionPerformed(date);
        if (actionScreen.getActionReportRealmList()!=null){
            actionScreen.getActionReportRealmList().add(actionReport);
        }else{
            actionScreen.setActionReportRealmList(new RealmList<ActionReport>());
            actionScreen.getActionReportRealmList().add(actionReport);
        }
        realm.commitTransaction();
        realm.close();
    }
    public RealmConfiguration getMyConfig() {
        return myConfig;
    }

    public void setMyConfig(RealmConfiguration myConfig) {
        this.myConfig = myConfig;
    }
    private int getIdForSession(){
        int idSession=0;
        if (CurrentManager.getInstance().getCurrentSession()==null){
            Realm realm  = Realm.getInstance(getMyConfig());
            long cantidad = realm.where(Session.class).count() +1;
            realm.close();
            idSession = ((Long)cantidad).intValue();
        }else {
            idSession=CurrentManager.getInstance().getCurrentSession().getId()+1;
        }
        return idSession;
    }
    private int getIdForScreen(){
        int idScreen=0;
        if (CurrentManager.getInstance().getCurrentScreen()==null){
            Realm realm  = Realm.getInstance(getMyConfig());
            long cantidad = realm.where(Screen.class).count() +1;
            realm.close();
            idScreen = ((Long)cantidad).intValue();
        }else {
            idScreen=CurrentManager.getInstance().getCurrentScreen().getId()+1;
        }
        return idScreen;
    }
    private int getIdForUserInteraction(){
        int idUserInteraction=0;
        if (CurrentManager.getInstance().getCurrentUserInteraction()==null){
            Realm realm  = Realm.getInstance(getMyConfig());
            long cantidad = realm.where(UserInteraction.class).count() +1;
            realm.close();
            idUserInteraction = ((Long)cantidad).intValue();
        }else {
            idUserInteraction=CurrentManager.getInstance().getCurrentUserInteraction().getId()+1;
        }
        return idUserInteraction;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        ReportController.context = context;
    }
}
