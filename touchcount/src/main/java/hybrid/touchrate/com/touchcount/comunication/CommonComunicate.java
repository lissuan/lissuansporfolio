package hybrid.touchrate.com.touchcount.comunication;

import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

/**
 * Created by lissuan on 3/17/2016.
 */
public class CommonComunicate {

    private static String URL;
    private static String NAMESPACE;


    public CommonComunicate()
    {
        init();
    }
    private void init() {

        URL = ConfigManager.getInstance().getConfig().getProperty("WS_PATH");
        NAMESPACE = ConfigManager.getInstance().getConfig().getProperty("NAMESPACE");

    }

    public String callWebService( SoapSerializationEnvelope soapEnvelope,  String SoapAction) throws Exception
    {
        String result=null;
        try
        {
            HttpTransportSE aht = new HttpTransportSE(URL);
            aht.call(SoapAction, soapEnvelope);
            SoapPrimitive resultString = (SoapPrimitive) soapEnvelope.getResponse();// this is where the result is saved
            result = resultString.toString();
        }
        catch (Exception e){
            throw e;
        }
        return result;
    }


    public Element buildSoapAuthHeader() {

        Element h = new Element().createElement(NAMESPACE, "AuthHeader"); // Do not change Namespace name
        Element username = new Element().createElement(NAMESPACE, "Username"); // Do not change Namespace name
        username.addChild(Node.TEXT, ConfigManager.getInstance().getConfig().getProperty("USER_NAME"));
        h.addChild(Node.ELEMENT, username);
        Element pass = new Element().createElement(NAMESPACE, "Password"); // Do not change Namespace name
        pass.addChild(Node.TEXT, ConfigManager.getInstance().getConfig().getProperty("PASS"));
        h.addChild(Node.ELEMENT, pass);
        return h;

    }
}
