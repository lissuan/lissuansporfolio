package hybrid.touchrate.com.touchcount.comunication;

import android.app.Application;
import android.content.Context;

/**
 * Created by lissuan on 3/17/2016.
 */
public class TouchContextApp extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        TouchContextApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return TouchContextApp.context;
    }
}
