package hybrid.touchrate.com.touchcount.comunication;

import android.app.Activity;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.view.View;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by soto on 5/29/2014.
 */
public class UtilsComucation {



    public static String getStringFromDate(Date date){

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String reportDate = df.format(today);

        return reportDate;
    }

    public static void saveFile(String path,String[] data)
    {
        try{
            File installed= new File (path);
            if(installed.exists())
                installed.delete();
            installed.createNewFile();

            FileOutputStream fOut = new FileOutputStream(installed);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

            BufferedWriter bw = new BufferedWriter(myOutWriter);

            for(int i=0;i<data.length;i++)
            {
                bw.write(data[i]);
                if(i!=data.length-1)
                  bw.newLine();
            }

            bw.flush();
            bw.close();
            myOutWriter.close();
            fOut.flush();
            fOut.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public static String readFile(String path) {

        String ret = null;
        try {
            InputStream inputStream = new FileInputStream(path);
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();

                ret = stringBuilder.toString();
            }
        }
        catch (Exception e) {

        }

        return ret;
    }

   public static int  getErrorCode()
    {
        int res = 0;
       // String pathFile= Environment.getExternalStorageDirectory().getAbsolutePath()+ Global.config.getProperty("PROJECT_FLD")+ Global.config.getProperty("ERROR_FLAG");
       /* String  crashedfile= UtilsComucation.readFile(pathFile);

        if (crashedfile==null) {
            res=100;
        }
        else{
            res=Integer.parseInt(crashedfile);
            new File(pathFile).delete();
        }*/
        return res;
    }

    public static Spanned formatDecimal(float number) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return Html.fromHtml(formatter.format(number).replace(".", ".<sup><small>") + "</small></sup>");
    }
    public static String formatDecimal2(float number) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        return formatter.format(number);
    }

    public static <T> T findViewById(View parent, int id) {
        return (T) parent.findViewById(id);
    }

    @SuppressWarnings("unchecked")
    public static <T> T findViewById(Activity activity, int id) {
        return (T) activity.findViewById(id);
    }

    public static String decodeStringFromBase64(String base64String)
    {
        try {
            byte[] bytes=   Base64.decode(base64String, Base64.DEFAULT);
            String str = new String(bytes, "UTF-8");
            return str;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return "";
        }

    }

}
