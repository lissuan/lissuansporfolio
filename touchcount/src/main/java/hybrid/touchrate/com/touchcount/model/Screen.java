package hybrid.touchrate.com.touchcount.model;

import java.util.ArrayList;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lissuan on 3/7/2016.
 */
public class Screen extends RealmObject {

    @PrimaryKey
    private int id;
    private String screenName;
    private int screenValue=0;
    private Date dateActionPerformed;
    private long durationTime=0;
    private Date dateFinished;
    private RealmList<ActionReport> actionReportRealmList;

    public Screen() {
    }

    public Screen(String screenName, Date dateActionPerformed, long durationTime, Date dateFinished, RealmList<ActionReport> actionReportRealmList) {
        this.screenName = screenName;
        this.dateActionPerformed = dateActionPerformed;
        this.durationTime = durationTime;
        this.dateFinished = dateFinished;
        this.actionReportRealmList = new RealmList<>();
    }

    public RealmList<ActionReport> getActionReportRealmList() {
        return actionReportRealmList;
    }

    public void setActionReportRealmList(RealmList<ActionReport> actionReportRealmList) {
        this.actionReportRealmList = actionReportRealmList;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Date getDateActionPerformed() {
        return dateActionPerformed;
    }

    public void setDateActionPerformed(Date dateActionPerformed) {
        this.dateActionPerformed = dateActionPerformed;
    }

    public long getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(long durationTime) {
        this.durationTime = durationTime;
    }

    public Date getDateFinished() {
        return dateFinished;
    }
    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public int getScreenValue() {
        return screenValue;
    }

    public void setScreenValue(int screenValue) {
        this.screenValue = screenValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
