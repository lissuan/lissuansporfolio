package hybrid.touchrate.com.touchcount.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by lissuan on 3/8/2016.
 */
public class Session extends RealmObject {

    @PrimaryKey
    private int id;
    private String sessionName;
    private RealmList<Screen> screenRealmList;
    private Date dateActionPerformed;
    private long durationTime;
    private Date dateFinished;


    public Session() {
        durationTime = 0;
    }

    public Session(String sessionName, RealmList<Screen> screenRealmList, Date dateActionPerformed, long durationTime, Date dateFinished) {
        this.sessionName = sessionName;
        this.screenRealmList = screenRealmList;
        this.dateActionPerformed = dateActionPerformed;
        this.durationTime = durationTime;
        this.dateFinished = dateFinished;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public RealmList<Screen> getScreenRealmList() {
        return screenRealmList;
    }

    public void setScreenRealmList(RealmList<Screen> screenRealmList) {
        this.screenRealmList = screenRealmList;
    }

    public Date getDateActionPerformed() {
        return dateActionPerformed;
    }

    public void setDateActionPerformed(Date dateActionPerformed) {
        this.dateActionPerformed = dateActionPerformed;
    }

    public long getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(long durationTime) {
        this.durationTime = durationTime;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
