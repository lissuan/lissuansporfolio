package hybrid.touchrate.com.touchcount.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by lissuan on 3/7/2016.
 */
public class ActionReport extends RealmObject{

    private String actionReportType;
    private String actionValue;
    private String customValue;
    private Date dateActionPerformed;
    private long durationTime=0;
    private Date dateFinished;

    public ActionReport() {
    }

    public ActionReport(String actionReportType, String actionValue, Date dateActionPerformed) {
        this.actionReportType = actionReportType;
        this.actionValue = actionValue;
        this.dateActionPerformed = dateActionPerformed;
    }

    public String getActionReportType() {
        return actionReportType;
    }

    public void setActionReportType(String actionReportType) {
        this.actionReportType = actionReportType;
    }

    public String getActionValue() {
        return actionValue;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    public Date getDateActionPerformed() {
        return dateActionPerformed;
    }

    public void setDateActionPerformed(Date dateActionPerformed) {
        this.dateActionPerformed = dateActionPerformed;
    }

    public long getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(long durationTime) {
        this.durationTime = durationTime;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public String getCustomValue() {
        return customValue;
    }

    public void setCustomValue(String customValue) {
        this.customValue = customValue;
    }
}
