package hybrid.touchrate.com.touchcount.comunication;

import android.util.Log;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import hybrid.touchrate.com.touchcount.managers.CurrentManager;
import hybrid.touchrate.com.touchcount.managers.ReportController;
import hybrid.touchrate.com.touchcount.model.ActionReport;
import hybrid.touchrate.com.touchcount.model.Screen;
import hybrid.touchrate.com.touchcount.model.Session;
import hybrid.touchrate.com.touchcount.model.UserInteraction;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by lissuan on 3/14/2016.
 */
public class UpdateManager {

    private static UpdateManager instance;

    public static UpdateManager getInstance() {
        if (instance==null){
            instance = new UpdateManager();
        }
        return instance;
    }

    public String initUpdate(){
        return showString();
    }

    private void clearAllData(){
        Realm realm = Realm.getInstance(ReportController.getInstance().getMyConfig());
        realm.beginTransaction();
        realm.clear(ActionReport.class);
        realm.clear(Screen.class);
        realm.clear(Session.class);
        realm.clear(UserInteraction.class);
        realm.commitTransaction();
        realm.close();
    }

    private String showString(){
        Realm realm = Realm.getInstance(ReportController.getInstance().getMyConfig());
        RealmResults<UserInteraction> interactions = realm.where(UserInteraction.class).findAll();
        StringBuilder builder = new StringBuilder();
        SendData sendData = new SendData();
        sendData.init();
        try {
            boolean b = sendData.sendData(getXmlDocument(), true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        System.out.println(builder);
        clearAllData();
        return builder.toString();
    }

    /**
     * Here contruct the Xml report.
     * */
    public String getXmlDocument() throws ParserConfigurationException, TransformerException {

       /**
         * For the reports
         **/
        Realm realm = Realm.getInstance(ReportController.getInstance().getMyConfig());
        RealmResults<UserInteraction> interactions = realm.where(UserInteraction.class).findAll();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();

        Document doc = impl.createDocument(null, null, null);
        Element e1 = doc.createElement("xml");

        doc.appendChild(e1);

       Element config = doc.createElement("config");
        e1.appendChild(config);
        config.setAttribute("dev_id", CurrentManager.getInstance().getCurrentUserInteraction().getUuid());
        config.setAttribute("db_version", "0");
        config.setAttribute("apk_version", "1.0");

        for (UserInteraction interaction: interactions){

            Element interactionParameter = doc.createElement("UIT");
            e1.appendChild(interactionParameter);
            interactionParameter.setAttribute("id", String.valueOf(interaction.getId()));
            //interactionParameter.setAttribute("uuid", String.valueOf(interaction.getUuid()));
            interactionParameter.setAttribute("uuid", "1asdas");
            //interactionParameter.setAttribute("deviceId", interaction.getDeviceId());
            interactionParameter.setAttribute("deviceId", "eeee");
            if (interaction.getDateActionPerformed()!=null)
            interactionParameter.setAttribute("dateActionPerformed", UtilsComucation.getStringFromDate(interaction.getDateActionPerformed()));
            interactionParameter.setAttribute("durationTime",String.valueOf(interaction.getDurationTime()));
            if (interaction.getDateFinished()!=null)
            interactionParameter.setAttribute("dateFinished", UtilsComucation.getStringFromDate(interaction.getDateFinished()));

            for (Session session: interaction.getSessionRealmList()){
                Element sessionElement = doc.createElement("SESSION");
                interactionParameter.appendChild(sessionElement);

                sessionElement.setAttribute("id", String.valueOf(session.getId()));
                sessionElement.setAttribute("sessionName", String.valueOf(session.getSessionName()));
                if (session.getDateActionPerformed()!=null)
                sessionElement.setAttribute("dateActionPerformed",UtilsComucation.getStringFromDate(session.getDateActionPerformed()));
                sessionElement.setAttribute("durationTime",String.valueOf(session.getDurationTime()));
                if (session.getDateFinished()!=null)
                sessionElement.setAttribute("dateFinished",UtilsComucation.getStringFromDate(session.getDateFinished()));

              for (Screen screen : session.getScreenRealmList()
                        ) {
                    Element screenElement = doc.createElement("SCREEN");
                    sessionElement.appendChild(screenElement);
                    screenElement.setAttribute("id", String.valueOf(screen.getId()));
                    screenElement.setAttribute("screenName", screen.getScreenName());
                    screenElement.setAttribute("screenValue", String.valueOf(screen.getScreenValue()));
                    if (screen.getDateActionPerformed()!=null)
                    screenElement.setAttribute("dateActionPerformed", UtilsComucation.getStringFromDate(screen.getDateActionPerformed()));
                    if (screen.getDateFinished()!=null)
                        screenElement.setAttribute("dateFinished", UtilsComucation.getStringFromDate(screen.getDateFinished()));
                    screenElement.setAttribute("durationTime", String.valueOf(screen.getId()));
                    for (ActionReport action: screen.getActionReportRealmList()
                         ) {
                        Element actionReportElement = doc.createElement("ACTIONREPORT");
                        screenElement.appendChild(actionReportElement);

                        actionReportElement.setAttribute("actionReportType", action.getActionReportType());
                        if (action.getActionValue()!=null)
                        actionReportElement.setAttribute("actionValue", action.getActionValue());
                        if (action.getCustomValue()!=null)
                        actionReportElement.setAttribute("customValue", action.getCustomValue());
                        if (action.getDateActionPerformed()!=null)
                        actionReportElement.setAttribute("dateActionPerformed", UtilsComucation.getStringFromDate(action.getDateActionPerformed()));
                        if (action.getDateFinished()!=null)
                            actionReportElement.setAttribute("dateFinished", UtilsComucation.getStringFromDate(action.getDateFinished()));
                        actionReportElement.setAttribute("durationTime",String.valueOf(action.getDurationTime()));
                    }
                }
            }
        }
        DOMSource domSource = new DOMSource(doc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        java.io.StringWriter sw = new java.io.StringWriter();
        StreamResult sr = new StreamResult(sw);

        transformer.transform(domSource, sr);
        String xml = sw.toString();
        Log.d("XML to upload", xml);
        return xml;
    }
}
