package hybrid.touchrate.com.touchcount.model;

import io.realm.RealmObject;

/**
 * Created by lissuan on 3/7/2016.
 */
public class ActionReportType {
    public final static String BTNACTIONREPORT = "BUTTONACTIONREPORT";
    public final static String TOUCHACTIONREPORT = "TOUCHACTIONREPORT";
    public final static String LINKACTIONREPORT = "LINKACTIONREPORT";
    public final static String TOUCHDISPLAYEVENT = "TOUCHDISPLAYEVENT";
}
