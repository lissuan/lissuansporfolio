package org.uriana.java.framework.core.resources.eventhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class EventAssociationManager {
	// eventos > eventName > artifactType > artifactName.
	protected HashMap<String, HashMap<String, ArrayList<String>>> events;
	
	public EventAssociationManager(){
		events = new HashMap<String, HashMap<String, ArrayList<String>>>();
	}
	
	public void createEventAssociation(String eventName, String artifactName, String artifactType){
		HashMap<String, ArrayList<String>> artifactTypes;
		ArrayList<String> artifactNames;
		
		if(!events.containsKey(eventName)){ 
			events.put(eventName, new HashMap<String, ArrayList<String>>());
		}
		
		artifactTypes = events.get(eventName);
		
		if(!artifactTypes.containsKey(artifactType)){ 
			artifactTypes.put(artifactType, new ArrayList<String>());
		}
		
		artifactNames = artifactTypes.get(artifactType);
		
		if(!artifactNames.contains(artifactName)){
			artifactNames.add(artifactName);
		}
	}
	
	
	public void removeEventAssociation(String eventName, String artifactType, String artifactName){
		HashMap<String, ArrayList<String>> artifactTypes;
		ArrayList<String> artifactNames;
		
		if(events.containsKey(eventName)){
			artifactTypes = events.get(eventName);
			
			if(artifactTypes.containsKey(artifactType)){
				artifactNames = artifactTypes.get(artifactType);
				
				if(artifactNames.contains(artifactName)){
					artifactNames.remove(artifactName);
					
					if(artifactNames.size() == 0) artifactTypes.remove(artifactType);
					if(artifactTypes.size() == 0) events.remove(eventName);
				}
			}
		}		
	}
	
	public void removeAll(){
		Iterator<String> itEventKeys;
		String currentEventKey;
		
		Iterator<String> itArtifactKeys;
		String currentArtifactKey;
		
		HashMap<String, ArrayList<String>> artifactTypes;
				
		itEventKeys = events.keySet().iterator();
		
		while(itEventKeys.hasNext()){
			currentEventKey = itEventKeys.next();
			artifactTypes = events.get(currentEventKey);
			
			itArtifactKeys = artifactTypes.keySet().iterator();
			
			while(itArtifactKeys.hasNext()){
				currentArtifactKey = itArtifactKeys.next();
				artifactTypes.get(currentArtifactKey).clear();
			}
			
			artifactTypes.clear();
		}
		events.clear();
	}
	
	public ArrayList<String> getAssociations(String eventName, String artifactName){
		HashMap<String, ArrayList<String>> eventArtifacts;
		ArrayList<String> result;
		
		if(events.containsKey(eventName)){
			eventArtifacts = events.get(eventName);
			
			if(eventArtifacts.size() <= 0) return null;
			
			if(eventArtifacts.containsKey(artifactName)){
				result = eventArtifacts.get(artifactName);
				if(result == null) return null;
				if(result.size() <= 0) return null;
				
				return result;
			}
		}
		return null;
	}

}
