package org.uriana.java.framework.core.resources;

import java.util.logging.Logger;

import org.uriana.java.framework.core.CoreConstants;
import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.interfaces.IController;
import org.uriana.java.framework.core.interfaces.IMediator;
import org.uriana.java.framework.core.resources.eventhandling.EventInterestMap;

/**
 * A Mediator wraps a resource to get a generic access point to its functionalities. So it acts as an interface 
 * application - resource and resource - application.
 * @author Carlos Velasco
 * 
 */	
public abstract class Mediator implements IMediator {
	protected String _name;
	protected String _type;
	protected IController _controller;
		
	protected EventInterestMap interests;
		
	protected Logger logger;
		
		
	/**
	 * Creates a Mediator instance.
	 * @param aName The unique name for this mediator
	 * 
	 */				
	public Mediator(String aName){
		_name = aName;
		_type = CoreConstants.MEDIATOR_GENERIC;
		
		interests = new EventInterestMap();
		
		declareListenedEvents();
	}
		
	/**
	 * Gets the mediator's name
	 * @return This mediator's name.
	 * 
	 */
	@Override
	public String getName(){return _name;}
	
	/**
	 * Gets the mediator's type in case it exists.
	 * @return A String representing this mediator's purpose.
	 * 
	 */
	@Override
	public String getType(){return _type;}
	
	/**
	 * Sets the controller which hosts the current mediator instance. This will be automatically called 
	 * when registering the mediator at the controller.
	 * @param aController The controller which hosts the current mediator.
	 * 
	 */
	@Override
	public void setController(IController aController){_controller = aController;}
	
	/**
	 * Retrieves the controller which is currently hosting this mediator.
	 * @return The controller instance.
	 * 
	 */
	@Override
	public IController getController(){return _controller;}
	
	/**
	 * The onRegister function is automatically called after registering the mediator's instance as child
	 * of a Controller and serves as the initialization point for this mediator.
	 * 
	 */
	@Override
	public void onRegister(){
		logger = _controller.getLogger(getClass().getCanonicalName());
	}
	
	/**
	 * The onRemove function is automatically called before removing a mediator from it's hoster controller
	 * it serves as a clean up function.
	 * 
	 */
	@Override
	public void onRemove(){
		logger = null;
		interests.removeAll();
		interests = null;			
		_controller = null;
	}
	
	/**
	 * Dispatches the event through the framework.
	 * @param event The event instance that will be dispatched.
	 * 
	 */
	@Override
	public void dispatchEvent(FrameworkEvent anEvent){
		_controller.onEvent(anEvent);		
	}
	
	/**
	 * Retrieves an event map this Mediator is interested to receive from the application.
	 * @return The event interest map.
	 * 
	 */
	@Override
	public EventInterestMap getEventInterests(){return interests;}
	
	
	/**
	 * Override declareListenedEvents function to define which events this Mediator would like to receive 
	 * from the application.
	 * Use as many interests.addEventInterest calls as needed.
	 * 
	 */		
	protected abstract void declareListenedEvents();		
}
