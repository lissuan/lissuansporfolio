package org.uriana.java.framework.core.resources.eventhandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.uriana.java.framework.core.interfaces.IFrameworkEventHandler;

public class EventInterestMap {

	private HashMap<String, ArrayList<IFrameworkEventHandler>> interests;

	public EventInterestMap(){
		interests= new HashMap<String, ArrayList<IFrameworkEventHandler>>();
	}
	
	public boolean addEventInterest(String eventName, IFrameworkEventHandler eventHandler){
		if(interests.containsKey(eventName)) 
			return interests.get(eventName).add(eventHandler);
		else{
			interests.put(eventName, new ArrayList<IFrameworkEventHandler>());
			return interests.get(eventName).add(eventHandler);
		}
	}
	
	public boolean removeEventInterest(String eventName, IFrameworkEventHandler eventHandler){
		boolean result;
		ArrayList<IFrameworkEventHandler> handlers;
		
		if(interests.containsKey(eventName)){
			handlers = interests.get(eventName);
			result = handlers.remove(eventHandler);
			
			if(handlers.size() == 0){
				interests.remove(eventName);
			}
			
			return result;
		}
		return false;
	}

	public void removeAll(){
		Set<String> keys;
		Iterator<String> itKeys;
		ArrayList<IFrameworkEventHandler> handlers;
		String currentKey;
		
		keys = interests.keySet();
		itKeys = keys.iterator();
		
		while(itKeys.hasNext()){
			currentKey = itKeys.next();
			handlers = interests.get(currentKey);
			handlers.clear();
			
		}		
		interests.clear();
	}
	
	public ArrayList<IFrameworkEventHandler> getEventHandlers(String eventName){
		return interests.get(eventName);
	}
	
	public String[] getEventNames(){return interests.keySet().toArray(new String[0]);}
}