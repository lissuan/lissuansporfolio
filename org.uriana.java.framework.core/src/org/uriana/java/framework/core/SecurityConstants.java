package org.uriana.java.framework.core;

public class SecurityConstants {
	//SECURITY LEVELS:
	public static final short SECURITY_ANONYMOUS_USER = 0;
	public static final short SECURITY_RESTRICTED_USER = 1;
	public static final short SECURITY_ADVANCED_USER = 2;
	public static final short SECURITY_ADMINISTRATOR_USER = 3;
}
