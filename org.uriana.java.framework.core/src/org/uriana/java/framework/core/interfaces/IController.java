package org.uriana.java.framework.core.interfaces;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.uriana.java.framework.core.control.events.FrameworkEvent;

public interface IController{

	public String getName();
	public String getContext();
	
	public void setParent(IController aController);
	public IController getParent();
	
	public void initialize();
	public void finalize();
	
	public void registerProcedure(IProcedure aProcedure);
	public void removeProcedure(String procedureName);
	
	public Logger getLogger(String className);
	
	public void registerMediator(IMediator aMediator);
	public IMediator retrieveMediator(String mediatorName);
	public void removeMediator(String mediatorName);	
	public boolean hostsMediator(String mediatorName);
	
	public void registerSubController(IController aController);
	public IController retrieveSubController(String controllerName);
	public void removeSubController(String controllerName);
	
	public void onEvent(FrameworkEvent event);
	public void onEvent(FrameworkEvent event, ArrayList<String> prevControllerNames);
	
	public void dispatchEvent(FrameworkEvent anEvent);	
}
