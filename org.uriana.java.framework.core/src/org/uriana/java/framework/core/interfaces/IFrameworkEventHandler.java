package org.uriana.java.framework.core.interfaces;

import org.uriana.java.framework.core.control.events.FrameworkEvent;

public interface IFrameworkEventHandler {
	public void handleEvent(FrameworkEvent event);
}
