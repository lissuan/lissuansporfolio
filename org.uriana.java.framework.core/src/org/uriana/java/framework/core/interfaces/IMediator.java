package org.uriana.java.framework.core.interfaces;

import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.resources.eventhandling.EventInterestMap;

public interface IMediator{

	public String getName();
	public String getType();
	
	public void setController(IController aController);
	public IController getController();
	
	public void onRegister();
	public void onRemove();
	
	public EventInterestMap getEventInterests();
	
	public void dispatchEvent(FrameworkEvent anEvent);
	
}
