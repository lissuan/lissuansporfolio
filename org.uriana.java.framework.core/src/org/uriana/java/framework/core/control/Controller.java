package org.uriana.java.framework.core.control;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.uriana.java.framework.core.CoreConstants;
import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.interfaces.IController;
import org.uriana.java.framework.core.interfaces.IFrameworkEventHandler;
import org.uriana.java.framework.core.interfaces.IMediator;
import org.uriana.java.framework.core.interfaces.IProcedure;
import org.uriana.java.framework.core.resources.eventhandling.EventAssociationManager;
import org.uriana.java.framework.core.resources.eventhandling.EventInterestMap;

public abstract class Controller implements IController{

	protected String _name;
	protected IController _parent;
	protected String _context;
	protected boolean _initialized;
		
	protected ArrayList<IController> subControllers;
	protected ArrayList<IProcedure> procedures;
	protected ArrayList<IMediator> mediators;

	protected EventAssociationManager eventManager;

	protected Logger _logger;
	
	/**
	 * Instantiates a new Controller instance.
	 * @param aName Controller name. It must be unique through the entire application.
	 * @param context String to set if the controller works on a web application (CoreConstants.APPLICATION_FLEX) or a desktop/mobile one (CoreConstants.APPLICAITON_AIR).
	 * 
	 */		
	public Controller(String aName, String context){
		_name = aName;
		_parent = null;
		_context = context;
		_initialized = false;
		
		subControllers = new ArrayList<IController>();
		procedures = new ArrayList<IProcedure>();
		mediators = new ArrayList<IMediator>();
		
		eventManager = new EventAssociationManager();
	}
	
	/**
	 * Get a logging resource to handle log actions.
	 * @return a LogManager instance for logging purposes.
	 * @see LogManager
	 * 
	 */
	@Override
	public Logger getLogger(String className){return Logger.getLogger(className);}
	
	/**
	 * Get the current controller's name.
	 * @return The controller's name. 
	 * 
	 */
	@Override
	public String getName(){ return _name;}
	
	/**
	 * Get the working context
	 * @return A String representing if the controller is on a web application (CoreConstants.APPLICATION_FLEX) or on a desktop/mobile application (CoreConstants.APPLICATION_AIR).
	 * @see CoreConstants.
	 */
	@Override
	public String getContext(){return _context;}
	
	/**
	 * Sets a parent controller for the current one.
	 * @param aController An IController instance to act as this controller's parent.
	 * @see IController
	 * 
	 */
	@Override
	public void setParent(IController aController){ _parent = aController;}
	
	/**
	 * Gets the current controller's parent
	 * @return The parent's controller or null in case it does not exist.
	 * 
	 */
	@Override
	public IController getParent(){ return _parent;}
	
	/**
	 * Initializes the Controller. This method is not supposed to be overriden.
	 * 
	 */
	@Override
	public void initialize(){
		if(!_initialized){
			_logger = Logger.getLogger(this.getClass().getCanonicalName());
			declareSubControllers();
			declareMediators();
			declareProcedures();
			postInitialize();
			_initialized = true;
		}
	}
	
	/**
	 * Override this function to place registerProcedure calls to link Procedures to the current controller.
	 * @see Procedure
	 * @see registerProcedure
	 */
	protected abstract void declareProcedures();
	
	/**
	 * Override this function to place registerMediator calls to link Mediators to the current controller.
	 * @see Mediator
	 * @see registerProcedure
	 */
	protected abstract void declareMediators();
	
	/**
	 * Override this function to place registerSubController calls to create a hierarchical set of Controllers.
	 * The subcontroller's parent property will be automatically set.
	 * 
	 */
	protected abstract void declareSubControllers();
	
	/**
	 * Override to place any custom post initialization code.
	 * 
	 */
	protected abstract void postInitialize();
	
	/**
	 * Finalizes the controller's lifecycle and also cleans up any hosted resource.
	 * This function is not supposed to be overriden.
	 * 
	 */
	@Override
	public void finalize(){
		Iterator<IController> itSubControllers;
		Iterator<IMediator> itMediators;
		Iterator<IProcedure> itProcedures;
		ArrayList<String> artifactNames;
		Iterator<String> itArtifactNames;
				
		if(_parent != null){
			IController con;
			con = _parent;
			_parent = null;
			con.removeSubController(_name);
			con=null;
			return;
		}
		
		_logger.info("Removing subcontrollers...");
		artifactNames = new ArrayList<String>();
		
		itSubControllers = subControllers.iterator();
		while(itSubControllers.hasNext()){
			artifactNames.add(itSubControllers.next().getName());
		}
		itSubControllers = null;
		
		itArtifactNames = artifactNames.iterator();
		while(itArtifactNames.hasNext()){
			removeSubController(itArtifactNames.next());
		}
		itArtifactNames = null;
		
		subControllers.clear();
		subControllers = null;
		_logger.fine("SUBCONTROLLERS were succesfully removed.");
				
		_logger.info("Removing mediators...");
		artifactNames = new ArrayList<String>();
		
		itMediators = mediators.iterator();
		while(itMediators.hasNext()){
			artifactNames.add(itMediators.next().getName());
		}
		itMediators = null;
		
		itArtifactNames = artifactNames.iterator();
		while(itArtifactNames.hasNext()){
			removeMediator(itArtifactNames.next());
		}
		itArtifactNames = null;
		
		mediators.clear();
		mediators = null;
		_logger.fine("MEDIATORS were succesfully removed.");
		
		_logger.info("Removing procedures...");
		artifactNames = new ArrayList<String>();
		
		itProcedures = procedures.iterator();
		while(itProcedures.hasNext()){
			artifactNames.add(itProcedures.next().getName());
		}
		itProcedures = null;
		
		itArtifactNames = artifactNames.iterator();
		while(itArtifactNames.hasNext()){
			removeProcedure(itArtifactNames.next());
		}
		itArtifactNames = null;
				
		procedures.clear();
		procedures = null;
		_logger.fine("PROCEDURES were succesfully removed.");
		
		_logger.info("Cleaning up event handling map.");
		eventManager.removeAll();
		eventManager = null;
		_logger.fine("EVENT HANDLING MAP was succesfully cleant");
		_logger.fine("Finalized.");
					
		_logger = null;
		_initialized = false;
	}
	
	/**
	 * Adds a procedure to the hosted procedures list. After addition, the IProcedure.onRegister() function will be called.
	 * @param procedure An instance of IProcedure to be linked to the current controller.
	 * @see IProcedure
	 */
	@Override
	public void registerProcedure(IProcedure procedure){
		String[] eventNames;
		int i;
		
		if(procedures.contains(procedure)){
			_logger.severe(procedure.getName() + " procedure is already registered and won't be overwritten.");
			return;
		}
		
		procedure.setController(this);
		
		procedures.add(procedure);
		
		_logger.info(procedure.getName() + " procedure added to procedures list.");
		
		_logger.info("Mapping event interests for " + procedure.getName() + " procedure.");
		eventNames = procedure.getEventInterests().getEventNames();
		
		for(i=0; i<eventNames.length;i++){
			eventManager.createEventAssociation(eventNames[i], procedure.getName() ,CoreConstants.ARTIFACT_PROCEDURE);
		}
			
		_logger.info("Event interests successfully mapped for " + procedure.getName() + " procedure.");
		_logger.fine(procedure.getName() + " procedure succesfully registered...");
		
		procedure.onRegister();
	}
	
	/**
	 * Removes the procedure from the hosted procedures list and so cleans its resources. After removing, the IProcedure.onRemove()
	 * function will be automatically called.
	 * @param procedureName The name of the procedure to be removed.
	 * 
	 */
	@Override
	public void removeProcedure(String procedureName){
		IProcedure currentProcedure;
		Iterator<IProcedure> itProcedures;
		String[] eventNames;
		int i;
		
		_logger.fine("Removing " + procedureName + " procedure...");
		
		itProcedures = procedures.iterator();
		while(itProcedures.hasNext()){
			currentProcedure = itProcedures.next();
			
			if(procedureName.equals(currentProcedure.getName())){
				
				_logger.info("Removing event interests mapping for " + procedureName + " procedure.");
				
				eventNames = currentProcedure.getEventInterests().getEventNames();
				for(i=0; i< eventNames.length;i++){
					eventManager.removeEventAssociation(eventNames[i], CoreConstants.ARTIFACT_PROCEDURE, procedureName);
				}
				_logger.info("Event interests mapping successfully removed for " + procedureName + " procedure.");
								
				
				itProcedures.remove();
				_logger.fine(procedureName + " procedure removed successfully from procedures list.");
				
				currentProcedure.onRemove();
				currentProcedure = null;
				return;
			}			
		}
		_logger.warning(procedureName + " procedure was not hosted by this controller.");
	}
	
	/**
	 * Registers a Mediator with the current Controller. After addition the onRegister function of the Mediator will be called.
	 * @param mediator An instance of IMediator to be linked.
	 * @see IMediator
	 */
	@Override
	public void registerMediator(IMediator mediator){
		String[] eventNames;
		int i;
		
		_logger.fine("Registering " + mediator.getName() + " mediator...");
		
		if(mediators.contains(mediator)){
			_logger.severe(mediator.getName() + " mediator is already registered and won't be overwritten.");
			return;
		}
		
		mediator.setController(this);
		mediators.add(mediator);
		_logger.info(mediator.getName() + " mediator added to mediators list.");
			
		_logger.info("Mapping event interests for " + mediator.getName() + " mediator.");
		eventNames = mediator.getEventInterests().getEventNames();	
		
		for(i=0; i< eventNames.length; i++){
			eventManager.createEventAssociation(eventNames[i], mediator.getName(), CoreConstants.ARTIFACT_MEDIATOR);
		}
		_logger.info("Event interests successfully mapped for " + mediator.getName() + " mediator.");
		_logger.fine(mediator.getName() + " mediator succesfully registered...");
		
		mediator.onRegister();
	}
	
	/**
	 * Finds the mediator through the application that responds to the given name. In case this controller
	 * does not directly host such a mediator, it will ask its subcontrollers and its parent.
	 * @param mediatorName The mediator's name.
	 * @return The mediator or null if it is not hosted.
	 * 
	 */
	@Override
	public IMediator retrieveMediator(String mediatorName){
		IMediator mediator;
		IController currentController;
		Iterator<IMediator> it;
		Iterator<IController>itControllers;
		
		_logger.fine("Retrieving " + mediatorName + " mediator.");
		
		it = mediators.iterator();
		while(it.hasNext()){
			mediator = it.next();			
			if(mediatorName.equals(mediator.getName())){
				_logger.fine(mediatorName + " mediator is hosted. Mediator returned");
				return mediator;
			}
		}
		// No est� alojado en el presente controlador.
		_logger.fine(mediatorName + " mediator is not directly hosted. Searching through the framework.");
		_logger.info("Trying to retrieve "+ mediatorName + " mediator from subcontrollers.");
		
		itControllers = subControllers.iterator();
		while(itControllers.hasNext()){
			currentController = itControllers.next();
			if(currentController.hostsMediator(mediatorName)){
				return currentController.retrieveMediator(mediatorName);
			}
		}
		
		_logger.info("Trying to retrieve "+ mediatorName + " mediator from parent.");
		if(_parent != null) return _parent.retrieveMediator(mediatorName);
		
		_logger.fine("The " + mediatorName + " mediator was not found through the framework.");
		return null;
	}
	
	/**
	 * Allows to consult if a mediator of the given name is hosted by the current controller instance.
	 * @param mediatorName The Mediator's name to find.
	 * @return True in case the mediator exists, otherwise false.
	 * 
	 */
	@Override
	public boolean hostsMediator(String mediatorName){
		Iterator<IMediator> it;
		
		it = mediators.iterator();
		while(it.hasNext()){
			if(mediatorName.equals(it.next().getName())) return true;
		}
		return false;
	}
	
	/**
	 * Removes the mediator of the given name in case it is hosted by the current controller instance.
	 * Before removing the mediator's onRemove function will be called.
	 * @param mediatorName The Mediator's name.
	 * 
	 */
	@Override
	public void removeMediator(String mediatorName){
		IMediator currentMediator;
		Iterator<IMediator> itMediators;
		String[] eventNames;
		int i;
		
		_logger.fine("Removing " + mediatorName + " mediator...");
		itMediators = mediators.iterator();
		while(itMediators.hasNext()){
			currentMediator = itMediators.next();
			if(mediatorName.equals(currentMediator.getName())){
				
				_logger.info("Removing event interests mapping for " + mediatorName + " mediator.");
				eventNames = currentMediator.getEventInterests().getEventNames();
				
				for(i=0; i<eventNames.length;i++){
					eventManager.removeEventAssociation(eventNames[i], CoreConstants.ARTIFACT_MEDIATOR, mediatorName);
				}
				_logger.info("Event interests mapping successfully removed for " + mediatorName + " mediator.");
				
				itMediators.remove();
				_logger.fine(mediatorName + " mediator removed successfully from mediators list.");
				
				currentMediator.onRemove();
				currentMediator = null;
				return;
			}
		}
		_logger.warning(mediatorName + " mediator was not hosted by this controller.");
	}
	
	/**
	 * Adds a subcontroller as a child of the current one. After addition, it will be called the IController.onRegister function.
	 * @param controller The IController instance to add as a child.
	 * 
	 */		
	public void registerSubController(IController controller){
		_logger.fine("Registering " + controller.getName() + " subcontroller...");			
		if(controller.getParent() != null){
			_logger.severe(controller.getName() + " controller is already registered with another controller.");				
			return;
		}
		
		controller.setParent(this);
		subControllers.add(controller);
		
		_logger.fine(controller.getName() + " controller added to subcontrollers list.");
		
		controller.initialize();
	}
	
	/**
	 * Retrieves the subcontroller's instance of a given name.
	 * @param controllerName The subcontroller's name.
	 * @return The hosted subcontroller instance or null in case it is not found.
	 * 
	 */		
	public IController retrieveSubController(String controllerName){
		Iterator<IController> it;
		IController currentController;
		it = subControllers.iterator();
		while(it.hasNext()){
			currentController = it.next();
			if(controllerName.equals(currentController.getName())) return currentController;
		}
		return null;
	}
	
	/**
	 * Removes the subcontroller of this controller's hierarchy.
	 * @param controllerName The hosted subcontroller's name.
	 * 
	 */		
	public void removeSubController(String controllerName){
		IController currentController;
		Iterator<IController> itControllers;
				
		_logger.fine("Removing " + controllerName + " controller...");
		itControllers = subControllers.iterator();
		
		while(itControllers.hasNext()){
			currentController = itControllers.next();
			
			if(controllerName.equals(currentController.getName())){
				
				itControllers.remove();
				_logger.fine(controllerName + " controller was successfully removed.");
			}
			currentController.finalize();
			currentController = null;
			return;
		}
		_logger.warning(controllerName + " controller was not hosted by this controller.");
	}
	
	
	/**
	 * Dispatches the event through the framework.
	 * @param event The event instance that will be dispatched.
	 * 
	 */
	@Override
	public void dispatchEvent(FrameworkEvent anEvent){onEvent(anEvent);}
	
	/**
	 * Propagates events between linked controllers and dispatches them to the hosted Procedures or Mediators when needed.
	 * This method is of internal use (between controllers) so it's not expected to be overriden.
	 * 
	 * @param ev The event instance to propagate.
	 * 
	 */
	@Override
	public void onEvent(FrameworkEvent ev){
		onEvent(ev, new ArrayList<String>());
	}
	
	
	/**
	 * Propagates events between linked controllers and dispatches them to the hosted Procedures or Mediators when needed.
	 * This method is of internal use (between controllers) so it's not expected to be overriden.
	 * 
	 * @param ev The event instance to propagate.
	 * @param prevControllerNames A list of controller names where this event has already been propagated.
	 * 
	 */
	@Override
	public void onEvent(FrameworkEvent ev, ArrayList<String> prevControllerNames){
		ArrayList<String> associations;
		Iterator<String> it;
		String artifactName;
		IController currentController;
		Iterator<IController> itControllers;
		
		try{
			if(prevControllerNames == null) prevControllerNames = new ArrayList<String>();
			prevControllerNames.add(_name);
			
			_logger.info("Event " + ev.getType() + " captured...");
			
			associations = eventManager.getAssociations(ev.getType(), CoreConstants.ARTIFACT_MEDIATOR);
			
			if(associations != null){
				it = associations.iterator();
				while(it.hasNext()){
					artifactName = it.next();
					_logger.info("Mediator " + artifactName + " is interested in event " + ev.getType());
					notifyMediator(ev, artifactName);
				}
			}
			associations = eventManager.getAssociations(ev.getType(), CoreConstants.ARTIFACT_PROCEDURE);
			if(associations != null){
				it = associations.iterator();
				while(it.hasNext()){
					artifactName = it.next();
					_logger.info("Procedure " + artifactName + " is interested in event " + ev.getType());
					notifyProcedure(ev, artifactName);
				}
			}	
			
			_logger.info("Broadcasting event [" + ev.getType() + "]");
			
			if(_parent != null){
				if(!prevControllerNames.contains(_parent.getName())){
					_parent.onEvent(ev, prevControllerNames);
				}
			}
			
			itControllers = subControllers.iterator();
			while(itControllers.hasNext()){
				currentController = itControllers.next();
				if(!prevControllerNames.contains(currentController.getName())){
					currentController.onEvent(ev, prevControllerNames);
				}
			}
		}catch(Exception e){
			//This Controller no longer exists.
		}			
	}
	
	
	/**
	 * Notifies an event to a Mediator.
	 * @param mediatorName The mediator's name.
	 * @param ev The event to be dispatched.
	 * 
	 */		
	private void notifyMediator(FrameworkEvent ev, String mediatorName){
		IMediator currentMediator;
		EventInterestMap eventInterests;
		Iterator<IFrameworkEventHandler> itHandlers;
		
		_logger.info("Calling handler methods from " + mediatorName + " mediator.");
		currentMediator = retrieveMediator(mediatorName);
		
		if(currentMediator == null) return;
		eventInterests = currentMediator.getEventInterests();
		if(eventInterests == null) return;
		
		itHandlers = eventInterests.getEventHandlers(ev.getType()).iterator();
		while(itHandlers.hasNext()){
			itHandlers.next().handleEvent(ev);
		}
	}
	
	/**
	 * Notifies an event to a Procedure (Causes the Procedure's execution).
	 * @param procedureName The procedure's name.
	 * @param ev The event to be dispatched.
	 * 
	 */		
	private void notifyProcedure(FrameworkEvent ev, String procedureName){
		IProcedure currentProcedure;
		Iterator<IProcedure> itProcedures;
		
		_logger.info("Executing " + procedureName + " procedure.");
		itProcedures = procedures.iterator();
		while(itProcedures.hasNext()){
			currentProcedure = itProcedures.next();
			if(procedureName.equals(currentProcedure.getName())){
				currentProcedure.execute(ev);
			}
		}
	}
}
