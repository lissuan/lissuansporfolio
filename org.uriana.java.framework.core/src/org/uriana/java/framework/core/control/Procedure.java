package org.uriana.java.framework.core.control;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

import org.uriana.java.framework.core.CoreConstants;
import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.interfaces.IController;
import org.uriana.java.framework.core.interfaces.IProcedure;
import org.uriana.java.framework.core.resources.eventhandling.EventInterestMap;

public abstract class Procedure implements IProcedure {

	protected String _name;
	protected String _type;
	protected IController _controller;

	protected String id;
	protected Logger logger;

	protected EventInterestMap interests;
	protected ArrayList<IProcedure> subProcedures;
	
	/**
	 * Creates an instance a procedure with the given name.
	 * @param aName The name to be used. Must be unique within the Application.
	 * 
	 */		
	public Procedure(String aName){
		_name = aName;
		_type = CoreConstants.PROCEDURE_SINGLE;
		
		interests = new EventInterestMap();
		subProcedures = new ArrayList<IProcedure>();
		
		declareListenedEvents();		
	}
	
	/**
	 * Gets the current procedure's name.
	 * @return The name.
	 * 
	 */
	@Override
	public String getName(){return _name;}
	
	/**
	 * Gets this procedure's type
	 * @return CoreConstants.PROCEDURE_BATCH if this procedure is within a procedures batch. Otherwise Coreconstants.PROCEDURE_SINGLE.
	 * 
	 */
	@Override
	public String getType(){return _type;}
	
	/**
	 * 
	 * @return True if the procedure belongs to a procedure's batch.
	 * 
	 */
	@Override
	public boolean isBatch(){return _type == CoreConstants.PROCEDURE_BATCH;}
	
	/**
	 * Sets the controller which is holding this procedure. This function is automatically called by the controller's 
	 * registerProcedure function.
	 * @param aController The controller that is hosting the procedure.
	 * 
	 */
	@Override
	public void setController(IController aController){_controller = aController;}
	
	/**
	 * Gets an instance of the controller that is hosting this procedure.
	 * @return The hoster controller.
	 * 
	 */
	@Override
	public IController getController(){return _controller;}		
			
	/**
	 * Retrieves a list of FrameworkEvents this procedure is interested in receiving.
	 * @return The events list.
	 * @see EventInterestMap
	 * 
	 */
	@Override
	public EventInterestMap getEventInterests(){return interests;}
	
	
	/**
	 * This function is automatically called when registering this procedure to be hosted by a Controller.
	 * 
	 */
	@Override
	public void onRegister(){
		IProcedure procedure;
		Iterator<IProcedure> it;
		
		logger = _controller.getLogger(this.getClass().getCanonicalName());
		
		createSubProcedures();
		
		logger.info("Registering subprocedures...");
		it = subProcedures.iterator();
		while(it.hasNext()){
			procedure = it.next();
			procedure.setController(_controller);
			procedure.onRegister();
		}
	}
	
	/**
	 * This function is automatically called when removing the procedure from the controller.
	 * 
	 */
	@Override
	public void onRemove(){
		Iterator<IProcedure> it;
		
		logger.info("Removing subprocedures...");
		it = subProcedures.iterator();
		while(it.hasNext()){
			it.next().onRemove();
		}
		subProcedures.clear();
		subProcedures = null;
		
		logger.info("Removing event interests.");
		interests.removeAll();
		interests = null;
		
		logger.fine("Procedure was successfully removed.");
		logger = null;
		
		_controller = null;
	}
	
	/**
	 * Registers a procedure to be child of the current one, so creating a procedure's batch.
	 * @param procedure The child procedure.
	 * 
	 */
	@Override
	public void addSubProcedure(IProcedure procedure){
		logger.info("Adding subprocedure " + procedure.getName());
		if(subProcedures.contains(procedure)){
			logger.severe(procedure.getName() + " subprocedure is already registered.");
			return;
		}
		_type = CoreConstants.PROCEDURE_BATCH;
		subProcedures.add(procedure);
		logger.fine(procedure.getName() + "subprocedure was successfully added.");			
	}
	
	/**
	 * Removes a procedure of the given name from this procedure's batch.
	 * @param procedureName The procedure's name.
	 * 
	 */
	@Override
	public void removeSubProcedure(String procedureName){
		Iterator<IProcedure> it;
		IProcedure currentProcedure;
		logger.info("Removing subprocedure " + procedureName);
		
		it = subProcedures.iterator();
		while(it.hasNext()){
			currentProcedure = it.next();
			if(procedureName.equals(currentProcedure.getName())){
				it.remove();
			}
			currentProcedure = null;
		}
		
		logger.fine(procedureName + " subprocedure was succesfully removed.");			
		if(subProcedures.size() == 0) _type = CoreConstants.PROCEDURE_SINGLE;
	}
	
	/**
	 * Override this function to code de logics this procedure is handling.
	 * @param event The event instance that launched this procedure's execution.
	 * 
	 */
	@Override
	public void execute(FrameworkEvent event){
		Iterator<IProcedure> it;
		IProcedure currentProcedure;
		
		id = event.getID();
		it = subProcedures.iterator();
		while(it.hasNext()){
			currentProcedure = it.next();
			logger.fine("Executing " + currentProcedure.getName() + " subprocedure.");
			currentProcedure.execute(event);
		}
		currentProcedure = null;
	}
	
	
	/**
	 * Dispatches the event through the framework.
	 * @param event The event instance that will be dispatched.
	 * 
	 */
	@Override
	public void dispatchEvent(FrameworkEvent anEvent){
		_controller.onEvent(anEvent);		
	}
	
	/**
	 * Override this function to declare which events will cause this procedure's execution.
	 * Do it by calling interests.addEventInterest as many times as required.
	 * 
	 */
	protected abstract void declareListenedEvents();
		
	/**
	 * Override this function to declare which procedures will be executed when the current one is executed.
	 * The subprocedures will be executed in order of declaration.
	 * 
	 */
	protected abstract void createSubProcedures();
}
