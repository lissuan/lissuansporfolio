package org.uriana.java.framework.core;

public class CoreConstants {
	//CORE MODE CONSTANTS:
	public static final String APPLICATION_J2EE = "J2EE";
	public static final String APPLICATION_GWT = "GWT";
			
	//ARTIFACT SUPPORTED TYPES:
	public static final String ARTIFACT_MEDIATOR = "ARTIFACT_MEDIATOR";
	public static final String ARTIFACT_PROCEDURE = "ARTIFACT_PROCEDURE";
	public static final String ARTIFACT_CONTROLLER = "ARTIFACT_CONTROLLER";
					
	//MEDIATOR SUPPORTED TYPES:
	public static final String MEDIATOR_GENERIC = "MEDIATOR_GENERIC";
			
	//PROCEDURE SUPPORTED TYPES:
	public static final String PROCEDURE_SINGLE = "PROCEDURE_SINGLE";
	public static final String PROCEDURE_BATCH = "PROCEDURE_BATCH";
}
