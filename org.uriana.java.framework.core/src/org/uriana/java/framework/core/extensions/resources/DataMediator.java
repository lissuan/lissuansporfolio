package org.uriana.java.framework.core.extensions.resources;

import org.uriana.java.framework.core.extensions.ExtensionConstants;
import org.uriana.java.framework.core.interfaces.IMediator;
import org.uriana.java.framework.core.resources.Mediator;

public abstract class DataMediator extends Mediator implements IMediator {

	protected Object _data;

	public DataMediator(String aName, Object data){
		super(aName);
		_data = data;
		_type = ExtensionConstants.MEDIATOR_MODEL;
		initializeData();
	}

	protected abstract void initializeData();
	protected abstract void finalizeData();

	@Override
	public void onRemove(){
		finalizeData();
		_data = null;
		super.onRemove();			
	}

	public Object getData(){return _data;}

}
