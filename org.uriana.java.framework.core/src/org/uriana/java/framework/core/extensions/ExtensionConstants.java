package org.uriana.java.framework.core.extensions;

public class ExtensionConstants {
	//DEDICATED MEDIATORS:
	public static final String MEDIATOR_MODEL = "MEDIATOR_MODEL";
	public static final String MEDIATOR_VIEW = "MEDIATOR_VIEW";
}
