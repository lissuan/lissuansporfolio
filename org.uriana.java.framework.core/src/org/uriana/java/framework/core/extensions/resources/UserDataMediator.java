package org.uriana.java.framework.core.extensions.resources;

import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.extensions.control.events.UserDataEvent;
import org.uriana.java.framework.core.extensions.resources.userdata.UserInfo;
import org.uriana.java.framework.core.interfaces.IFrameworkEventHandler;
import org.uriana.java.framework.core.interfaces.IMediator;

public class UserDataMediator extends DataMediator implements IMediator {

	public UserDataMediator(String aName){
		super(aName, new UserInfo());
	}
	
	@Override
	protected void declareListenedEvents(){
		interests.addEventInterest(UserDataEvent.USER_DATA_LOAD, new IFrameworkEventHandler() {
			
			@Override
			public void handleEvent(FrameworkEvent event) {
				UserDataMediator.this.dispatchEvent(
						new UserDataEvent(UserDataEvent.USER_DATA_LOADED, event.getID(), 
								UserDataMediator.this.getUser()));
			}
		});
		interests.addEventInterest(UserDataEvent.USER_DATA_STORE, new IFrameworkEventHandler() {
			
			@Override
			public void handleEvent(FrameworkEvent event) {
				UserDataMediator.this.setUser((UserInfo)event.getData());
				UserDataMediator.this.dispatchEvent(
						new UserDataEvent(UserDataEvent.USER_DATA_STORED, event.getID(), 
								UserDataMediator.this.getUser()));
			}
		});
	}
	
	public void setUser(UserInfo userInfo){_data = userInfo;}
	public UserInfo getUser(){return (UserInfo)_data;}

	@Override
	protected void initializeData() {}

	@Override
	protected void finalizeData() { _data = null;}
}