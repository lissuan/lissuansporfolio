package org.uriana.java.framework.core.extensions.control.events;

import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.extensions.resources.views.vo.ViewLoadInfo;

public class ViewLoadEvent extends FrameworkEvent {
	
	public static final String VIEW_LOAD = "VIEW_LOAD";
	
	public ViewLoadEvent(String type, ViewLoadInfo data) {
		super(type, data);
	}
	
	
	public ViewLoadEvent(String type, String id, ViewLoadInfo data) {
		super(type, id, data);
	}
	
	
	public ViewLoadEvent(String type, String id, ViewLoadInfo data, boolean error,
			String errorCode, String errorMessage) {
		super(type, id, data, error, errorCode, errorMessage);
	}
	
	public ViewLoadInfo getViewInfo(){return (ViewLoadInfo)data;}
}
