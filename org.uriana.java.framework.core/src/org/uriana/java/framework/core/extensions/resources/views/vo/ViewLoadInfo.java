package org.uriana.java.framework.core.extensions.resources.views.vo;

import org.uriana.java.framework.core.extensions.interfaces.IView;

public class ViewLoadInfo {

	protected Class<IView> viewClass;
	protected String divName;
	protected Object loadingData;

	public ViewLoadInfo(){
		viewClass = null;
		divName = null;
		loadingData = null;
	}
			
	public void setViewClass(Class<IView> viewClass){this.viewClass = viewClass;}
	public Class<IView> getViewClass(){return viewClass;}
	
	public void setDivName(String divName){this.divName = divName;}
	public String getDivName(){return divName;}
	
	public void setLoadingData(Object loadingData){this.loadingData = loadingData;}
	public Object getLoadingData(){return loadingData;}
	
}
