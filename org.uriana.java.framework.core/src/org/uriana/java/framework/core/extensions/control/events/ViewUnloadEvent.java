package org.uriana.java.framework.core.extensions.control.events;

import org.uriana.java.framework.core.control.events.FrameworkEvent;
import org.uriana.java.framework.core.extensions.resources.views.vo.ViewUnloadInfo;


public class ViewUnloadEvent extends FrameworkEvent {
	public static final String VIEW_UNLOAD = "VIEW_UNLOAD";
	public static final String VIEW_UNLOAD_ALL = "VIEW_UNLOAD_ALL";
	
	public ViewUnloadEvent(String type, ViewUnloadInfo data) {
		super(type, data);
	}
	
	
	public ViewUnloadEvent(String type, String id, ViewUnloadInfo data) {
		super(type, id, data);
	}
	
	
	public ViewUnloadEvent(String type, String id, ViewUnloadInfo data, boolean error,
			String errorCode, String errorMessage) {
		super(type, id, data, error, errorCode, errorMessage);
	}
	
	public ViewUnloadInfo getViewInfo(){return (ViewUnloadInfo)data;}
}
