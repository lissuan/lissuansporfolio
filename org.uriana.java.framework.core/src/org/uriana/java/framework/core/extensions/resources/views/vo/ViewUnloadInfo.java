package org.uriana.java.framework.core.extensions.resources.views.vo;

public class ViewUnloadInfo {
	private String viewName;
	
	public ViewUnloadInfo(){
		setViewName(null);
	}

	public String getViewName() {return viewName;}

	public void setViewName(String viewName) {this.viewName = viewName;}
	
}
