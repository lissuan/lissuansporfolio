package org.uriana.java.framework.core.extensions.interfaces;

import org.uriana.java.framework.core.control.Controller;
import org.uriana.java.framework.core.resources.Mediator;

public interface IView {

	public void dispose();
	
	public String getViewName();
	public String getSectionName();
	public short getSecurityLevel();
	
	public Class<? extends Mediator> getMediatorClass();
	public Class<? extends Controller> getSectionControllerClass();
}
