package org.uriana.java.framework.core.extensions.resources;

import org.uriana.java.framework.core.extensions.ExtensionConstants;
import org.uriana.java.framework.core.extensions.interfaces.IView;
import org.uriana.java.framework.core.interfaces.IMediator;
import org.uriana.java.framework.core.resources.Mediator;

public abstract class ViewMediator extends Mediator implements IMediator {

	protected IView _view;
	protected String _mode;
	
	
	public ViewMediator(String aName, IView aView, String aMode) {
		super(aName);
		_view = aView;
		_mode = aMode;
		_type = ExtensionConstants.MEDIATOR_VIEW;		
	}

	@Override
	public void onRegister(){
		super.onRegister();
		declareViewListenedEvents();
	}
	
	@Override
	public void onRemove(){
		removeViewListenedEvents();
		_view = null;
		super.onRemove();			
	}
	
					
	public IView getView(){return _view;}
	
	
	protected abstract void declareViewListenedEvents();
	
	protected abstract void removeViewListenedEvents();

}
