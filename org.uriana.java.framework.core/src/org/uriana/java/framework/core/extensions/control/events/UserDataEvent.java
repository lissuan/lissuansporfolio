package org.uriana.java.framework.core.extensions.control.events;

import org.uriana.java.framework.core.control.events.FrameworkEvent;

public class UserDataEvent extends FrameworkEvent {
	
	public static final String USER_DATA_LOAD = "USER_DATA_LOAD";
	public static final String USER_DATA_STORE = "USER_DATA_STORE";
	
	public static final String USER_DATA_LOADED = "USER_DATA_LOADED";
	public static final String USER_DATA_STORED = "USER_DATA_STORED";
		
	public UserDataEvent(String type, Object data){
		super(type,data);
	}
	
	public UserDataEvent(String type, String id, Object data){
		super(type,id,data);
	}
	
	public UserDataEvent(String type, String id, Object data, boolean error, String errorCode, String errorMessage) {
		super(type, id, data, error, errorCode, errorMessage);
	}
}
