package org.uriana.java.framework.core.extensions.resources.userdata;

import org.uriana.java.framework.core.SecurityConstants;

public class UserInfo {

	protected String domain;
	protected String userName;
	protected String userPass;
	
	protected String name;
	protected String surname;
	protected String email;
	
	protected int securityLevel;
	
	protected String sessionID;
	
	protected String code;
	

	public UserInfo(){
		domain ="";
		userName = "Anonymous";
		userPass ="";
		
		name = "";
		surname = "";
		email = "";
		
		securityLevel = SecurityConstants.SECURITY_ANONYMOUS_USER;
		sessionID = "";
		code = "";
	}

	public String getCanonicalName(){return domain + "\\" + userName;}
	
	public void setDomain(String domain){this.domain = domain;}
	public String getDomain(){return domain;}
	
	public void setUserName(String userName){this.userName = userName;}
	public String getUserName(){return userName;}
	
	public void setUserPass(String userPass){this.userPass = userPass;}
	public String getUserPass(){return userPass;}
	
	public void setName(String name){this.name = name;}
	public String getName(){return name;}
	
	public void setSurname(String surname){this.surname = surname;}
	public String getSurname(){return surname;}
	
	public void setEmail(String email){this.email = email;}
	public String getEmail(){return email;}
	
	public void setSecurityLevel(int securityLevel){this.securityLevel = securityLevel;}
	public int getSecurityLevel(){return securityLevel;}
	
	public void setSessionID(String sessionID){this.sessionID = sessionID;}
	public String getSessionID(){return sessionID;}
	
	public void setCode(String code){this.code = code;}
	public String getCode(){return code;}
}
